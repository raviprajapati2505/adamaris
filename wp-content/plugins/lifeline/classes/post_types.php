<?php

class SH_Post_types {

	function __construct() {
		// Hook into the 'init' action
		add_action( 'init', array( $this, 'bistro_slider' ), 0 );
	}

	function labels( $names = '', $labels = array() ) {
		$default = array(
			'name' => _x( 'Slides', 'Slides', 'lifeline' ),
			'singular_name' => _x( 'Slide', 'Slide', 'lifeline' ),
			'menu_name' => __( 'Slidr', 'lifeline' ),
			'parent_item_colon' => __( 'Parent Slide:', 'lifeline' ),
			'all_items' => __( 'All Slides', 'lifeline' ),
			'view_item' => __( 'View Slide', 'lifeline' ),
			'add_new_item' => __( 'Add New Slide', 'lifeline' ),
			'add_new' => __( 'New Slide', 'lifeline' ),
			'edit_item' => __( 'Edit Slide', 'lifeline' ),
			'update_item' => __( 'Update Slide', 'lifeline' ),
			'search_items' => __( 'Search Slides', 'lifeline' ),
			'not_found' => __( 'No Slides found', 'lifeline' ),
			'not_found_in_trash' => __( 'No Slides found in Trash', 'lifeline' ),
		);

		foreach ( $default as $k => $v ) {
			$default[$k] = str_replace( array( 'Slide', 'Slides' ), $names, $v );
		}
		$labels = wp_parse_args( $labels, $default );

		return $labels;
	}

	function args( $args = array() ) {
		$default = array(
			'label' => __( 'bistro_slider', 'lifeline' ),
			'labels' => array(),
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => false,
			'menu_position' => 5,
			'menu_icon' => '',
			'can_export' => true,
			'has_archive' => false,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => array(),
			'capability_type' => 'post',
		);
		$args = wp_parse_args( $args, $default );
		return $args;
	}

	// Register Custom Post Type
	function bistro_slider() {
        
		$settings = include( SH_FRW_DIR . 'resource/post_types.php');

		foreach ( $options as $k => $v ) {
            
			$labels = $this->labels( sh_set( $v, 'labels' ), sh_set( $v, 'label_args' ) );

			$rewrite = array(
				'slug' => sh_set( $v, 'slug' ),
				'with_front' => true,
				'pages' => true,
				'feeds' => false,
			);

			$args = $this->args( array( 'labels' => $labels, 'supports' => sh_set( $v, 'supports' ), 'rewrite' => $rewrite, 'label' => sh_set( $v, 'label' ) ) );
			$args = wp_parse_args( sh_set( $v, 'args' ), $args );
			
			//if ( $k == 'dict_testimonials' ) printr($args);

			$register = register_post_type( $k, $args );
			if ( is_wp_error( $register ) )
				echo $register->get_error_message();
		}
	}

}
new SH_Post_types();