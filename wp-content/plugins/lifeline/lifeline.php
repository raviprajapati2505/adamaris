<?php
/*
  Plugin Name: Lifeline
  Plugin URI: http://www.webinane.com
  Description: A utitlity plugin for Lifeline Wordpress Theme
  Author: webinane
  Author URI: http://www.webinane.com
  Version: 2.1
  Text Domain: wp_lifeline
  License: GPLv2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */
define('PLUGIN_URI', plugins_url('lifeline') . '/');
define('CPATH', plugin_dir_path(__FILE__));
define('CURL', plugin_dir_url(__FILE__));

load_plugin_textdomain( 'wp_lifeline', false, CPATH.'/languages'); 

/*
 * Includes classes
 */
//require_once(plugin_dir_path(__FILE__) . '/classes/post_types.php');
require_once(plugin_dir_path(__FILE__) . '/classes/taxonomies.php');
require_once(plugin_dir_path(__FILE__) . '/classes/newsletter.php');
require_once(plugin_dir_path(__FILE__) . '/classes/instagram.php');

/*
 * Includes third party libraries
 */
require_once('includes/functions.php');
require_once('includes/codebird.php');
require_once('includes/recaptchalib.php');
if(!class_exists('SH_Grab')) {
	require_once( 'includes/grabber/grab.php' );
}
add_action('plugins_loaded', array('SH_Newsletter', 'lifeline_web_newsletter_table'));

function lifeline_register_widgets( $widget ) {
  return register_widget( $widget );
}
function lifeline_print_output($output) {
  return $output;

}

remove_filter('nav_menu_description', 'strip_tags');

function sh_setup_nav_menu_item($menu_item) {
    if (isset($menu_item->post_type)) {
        if ('nav_menu_item' == $menu_item->post_type) {
            $menu_item->description = apply_filters('nav_menu_description', $menu_item->post_content);
        }
    }

    return $menu_item;
}

add_filter('wp_setup_nav_menu_item', 'sh_setup_nav_menu_item');


if ( function_exists( 'vc_map' ) ) {
  function sh_vc_disable_update() {
    if ( function_exists( 'vc_license' ) && function_exists( 'vc_updater' )
         && ! vc_license()->isActivated()
    ) {

      remove_filter( 'upgrader_pre_download',
        [ vc_updater(), 'preUpgradeFilter' ], 10 );
      remove_filter( 'pre_set_site_transient_update_plugins', [
        vc_updater()->updateManager(),
        'check_update'
      ] );

    }
  }

  add_action( 'admin_init', 'sh_vc_disable_update', 9 );
}

function lifeline_mail_function( $contact_to, $string, $message, $headers ) {
  return wp_mail($contact_to, $string, $message, $headers);
}
function lifeline_return_server_info($string) {
  return $_SERVER[$string];
}
function lifeline_custom_post_types( $k, $args ) {
  return register_post_type( $k, $args );
}

  require_once( 'classes/meta_boxes.php' );
  new SH_Meta_boxes();