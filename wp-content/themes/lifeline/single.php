<?php
sh_custom_header();
$Settings = get_option('lifeline');

$PageSettings = get_post_meta(get_the_ID(), '_post_settings', true);
$PostSettings = get_post_meta(get_the_ID(), '_' . sh_set($post, 'post_type') . '_settings', true);
$videos = sh_set($Settings, 'videos');
$attachments = get_posts(array('post_type' => 'attachment', 'post_parent' => get_the_ID(), 'showposts' => -1));
//print_r($attachments); exit('aaa');
$sidebar = sh_set($PageSettings, 'sidebar') ? sh_set($PageSettings, 'sidebar') : 'default-sidebar';
$col_class = sh_set($PageSettings, 'sidebar') ? 'col-md-9' : 'left-content col-md-12';
$show_blog_comment = sh_set($Settings, 'show_blog_comment');
$sidebar_position = sh_set($PageSettings, 'sidebar_pos');
wp_enqueue_script('flexslider');
//printr($sidebar_position);
?>
<?php if (sh_set($PageSettings, 'top_image')): ?>
    <div class="top-image"> <img src="<?php echo sh_set($PageSettings, 'top_image'); ?>" /></div>
<?php endif; ?>


<!-- Page Top Image -->
<section class="inner-page <?php echo ( sh_set( $PageSettings, 'sidebar_pos' ) == 'left' ) ? ' switch' : ''; ?>">
    <div class="container">
        <div class="row">

	        <?php if ( $sidebar_position == 'left' && is_active_sidebar($sidebar) ): ?>
                <div class="sidebar col-md-3 pull-left"><?php dynamic_sidebar( $sidebar ); ?></div>
	        <?php endif; ?>

            <div class=" <?php echo esc_attr( $col_class ); ?>">
                <div id="post-<?php the_ID(); ?>" <?php post_class("post"); ?>>
                    <?php
                    while (have_posts()): the_post();

                        if (!post_password_required()) {
                            ?>
                            <?php if (sh_set($PostSettings, 'format') == 'image'): ?>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('1170x455'); ?>
                                <?php endif; ?>
                            <?php elseif (sh_set($PostSettings, 'format') == 'slider'): ?>
                                <div class="post-slider">
                                    <div class="single_carousel" >
                                        <ul class="slides"">
                                            <?php foreach ($attachments as $attachment):
                                                ?>
                                                <li> 
                                                    <?php echo wp_get_attachment_image($attachment->ID, '1170x455'); ?> 
                                                </li>
                                                <!-- Slide -->
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                         <script>
                        jQuery(document).ready(function ($) {
                            if ($(".single_carousel").length > 0) {
                                $('.single_carousel').flexslider({
                                    animation: "slide",
                                    animationLoop: false,
                                    controlNav: false,
                                    maxItems: 1,
                                    pausePlay: false,
                                    mousewheel: false,
                                    prevText :'<i class="icon-angle-left"></i>',
                                    nextText : '<i class="icon-angle-right"></i>',
                                    start: function (slider) {
                                        $('body').removeClass('loading');
                                    }
                                });
                            }
                        });
                    </script> 
                                <?php
                                
                            elseif (sh_set($PostSettings, 'format') == 'video'):
                                $video_link = sh_set(sh_set($PostSettings, 'videos'), 0);
                                $video_data = sh_grab_video($video_link, $PostSettings);
                                $thumb_size = (sh_set($PostSettings, 'sidebar')) ? 'style="width:870px; height:374px;"' : 'style="width:1170px; height:374px;"';
                                ?>
                                <div class="video-post"> <img src="<?php echo sh_set($video_data, 'thumb'); ?>"  alt="<?php echo sh_set($video_data, 'title'); ?>" /> <a class="html5lightbox" href="<?php echo esc_url( $video_link ); ?>"  ><i class="icon-play"></i></a> </div>
                            <?php endif; ?>
                            <span class="category">
                                <?php _e('In', 'lifeline'); ?>
                                <?php the_category(',', ''); ?>
                            </span><!-- Categories -->
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                            <ul class="post-meta">
                                <?php if(!sh_set($PageSettings, 'hide_post_date')):?>
                                
                                <li><a href="<?php echo esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d'))); ?>"  ><i class="icon-calendar-empty"></i><span><?php echo get_the_date('F'); ?></span> <?php echo get_the_date('d, Y'); ?></a></li>
                                <?php
                                
                                endif;
                                
                                $Author = get_the_author();
                                if (!empty($Author) && !sh_set($PageSettings, 'hide_post_author')) :
                                    ?>
                                    <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"  ><i class="icon-user"></i><?php echo __('By', 'lifeline'); ?> <?php echo get_the_author(); ?></a></li>
                                <?php endif; ?>
                                <?php if (sh_set($PostSettings, 'location')) : ?>
                                    <li><i class="icon-map-marker"></i><?php echo __('In', 'lifeline') . ' ' . sh_set($PostSettings, 'location'); ?></li>
                                <?php endif; ?>
                            </ul>
                            <div class="post-desc">
                                <?php the_content(); ?>
                                <?php wp_link_pages(); ?>
                            </div>
                            <?php
                        }
                        else {
                            ?>
                            <div class="post-desc">
                                <?php the_content(); ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php if (get_tags()): ?>
                            <div class="cloud-tags">
                                <?php the_tags('<h3 class="sub-head">' . __('Tags Clouds', 'lifeline') . '</h3>', ''); ?>
                            </div>
                        <?php endif; ?>
                        <!-- Tags -->
                        <?php if($show_blog_comment == 'false') : ?>
                            <?php if (is_single() && comments_open()) comments_template(); ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>

	        <?php if ( $sidebar_position == 'right' && is_active_sidebar($sidebar) ): ?>
                <div class="sidebar col-md-3 pull-right"><?php dynamic_sidebar( $sidebar ); ?></div>
	        <?php endif; ?>

        </div>
    </div>
</section>
<?php


get_footer(); ?>
