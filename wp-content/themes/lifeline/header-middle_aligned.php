<?php $settings = get_option( 'lifeline' ); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
		<?php echo ( sh_set( $settings, 'site_favicon' ) ) ? '<link rel="icon" type="image/png" href="' . sh_set( $settings, 'site_favicon' ) . '">' : ''; ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>

    </head>
	<?php
	$res_settings = sh_header_settings( $settings );
	$custom_sticky_header_class = (sh_set( $settings, 'sh_custom_stickey_menu' ) == 'true') ? 'sticky' : '';
	$header_enable_overlap = (sh_set( $settings, 'sh_enable_overlap' ) == 'true') ? 'headeroverlap' : '';
	?>
    <body <?php body_class( sh_set( $res_settings, 'pattern' ) ); ?>  style=" <?php echo sh_set( $res_settings, 'pattern_image' ); ?>">
	<div class="site-loading"></div>
        <?php echo donation_box(); ?>
        <div class="theme-layout <?php
		if ( sh_set( $settings, 'boxed_layout_status' ) == 'true' ): echo 'boxed';
		endif;
		?>" style=" <?php echo sh_set( $res_settings, 'width' ); ?>">
             <?php $headerbg_color=sh_set($settings, 'header_color_scheme' ); ?>
            <header class="middle-logo <?php echo esc_attr($custom_sticky_header_class); ?><?php echo esc_attr($header_enable_overlap); ?>" <?php echo esc_attr($headerbg_color)? 'style="background-color:'.$headerbg_color.' !important;"':'';?>>
                <div class="container">
                    <div class="logo"> 
						<?php
						if ( isset( $settings['logo_text_status'] ) && $settings['logo_text_status'] === 'true' ) {
							$LogoStyle = sh_get_font_settings( array( 'logo_text_font_size' => 'font-size', 'logo_text_font_family' => 'font-family', 'logo_text_font_style' => 'font-style', 'logo_text_color' => 'color' ), ' style="', '"' );
							$Logo = $settings['logo_text'];
						} else {
							$LogoStyle = '';
							$LogoImageStyle = ( sh_set( $settings, 'logo_width' ) || sh_set( $settings, 'logo_height' ) ) ? ' style="' : '';
							$LogoImageStyle .= ( sh_set( $settings, 'logo_width' ) ) ? ' width:' . sh_set( $settings, 'logo_width' ) . 'px;' : '';
							$LogoImageStyle .= ( sh_set( $settings, 'logo_height' ) ) ? ' height:' . sh_set( $settings, 'logo_height' ) . 'px;' : '';
							$LogoImageStyle .= ( sh_set( $settings, 'logo_width' ) || sh_set( $settings, 'logo_height' ) ) ? '"' : '';
							$Logo = '<img src="' . $settings['logo_image'] . '" alt="'.esc_attr__( 'Lifeline', 'lifeline' ).'"' . $LogoImageStyle . ' />';
						}
						?>
                        <a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>"<?php echo esc_attr($LogoStyle); ?>>
							<?php if ( sh_set( $settings, 'logo_text_status' ) === 'true' )  ?> <h1 <?php echo esc_attr($LogoStyle); ?>>
								<?php echo wp_kses_post($Logo); ?>
								<?php if ( sh_set( $settings, 'logo_text_status' ) === 'true' )  ?> </h1>
                        </a>
						<?php
						if ( sh_set( $settings, 'logo_text_status' ) === 'true' && sh_set( $settings, 'site_salogan' ) ) {
							$SaloganStyle = sh_get_font_settings( array( 'salogan_font_size' => 'font-size', 'salogan_font_family' => 'font-family', 'salogan_font_style' => 'font-style' ), ' style="', '"' );
							echo '<p' . $SaloganStyle . '>' . $settings['site_salogan'] . '</p>';
						}
						?>
                    </div>
                    <!-- Logo -->
                    <nav class="menu">
						<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_class' => '', 'container' => null, 'menu_id' => 'menu-navigation', 'fallback_cb' => false, 'walker' => new SH_Megamenu_walker ) ); ?>
                    </nav>
                </div>
            </header>
			<?php sh_responsive_menu() ?> 
