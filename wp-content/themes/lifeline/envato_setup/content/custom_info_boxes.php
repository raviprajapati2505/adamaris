<?php

if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );



class lifeline2_custom_info_boxes_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;



    public static function lifeline2_custom_info_boxes( $atts = null ) {

        if ( $atts == 'lifeline2_Shortcodes_Map' ) {

            $return = array(

                "name"                    => esc_html__( "Custom Info Boxes", 'lifeline' ),

                "base"                    => "lifeline2_custom_info_boxes_output",

                "icon"                    => VC . 'about_blog.png',

                "category"                => esc_html__( 'Webinane', 'lifeline' ),

                "as_parent"               => array( 'only' => 'lifeline2_fact_output' ),

                "show_settings_on_create" => true,

                "params"                  => array(

                     array(

                        'type' => 'param_group',

                        'value' => '',

                        'param_name' => 'custom_boxes',

                        "heading" => esc_html__("Add Custom Box", 'lifeline'),

                        "show_settings_on_create" => true,

                        'params' => array(

                            array(

                                "type"        => "textfield",

                                "class"       => "",

                                "heading"     => esc_html__( "Title", 'lifeline' ),

                                "param_name"  => "title",

                                "description" => esc_html__( "Enter the title to show on this section", 'lifeline' )

                            ),

                            array(

                                "type"        => "textfield",

                                "class"       => "",

                                "heading"     => esc_html__( "Button Link", 'lifeline' ),

                                "param_name"  => "btn_link",

                                "description" => esc_html__( "Enter URL to link with title", 'lifeline' )

                            ),

                            

                            array(

                                "type"        => "attach_image",

                                "class"       => "",

                                "heading"     => esc_html__( "Background Image", 'lifeline' ),

                                "param_name"  => "image",

                                "description" => esc_html__( "Upload bg image to show in this section", 'lifeline' ),

                            ),

                    ),

                        ),

                )

            );



            return $return;

        }

    }



    public static function lifeline2_custom_info_boxes_output( $atts = null, $content = null ) {



        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        ob_start();

        echo do_shortcode( $content );

        $output     = ob_get_contents();

        ob_clean();

        $custom_boxes = (json_decode(urldecode($custom_boxes)));

        if ( $custom_boxes ) :



               if(class_exists('lifeline2_Resizer'))

            $img_obj = new lifeline2_Resizer();

        ?>

        <div class="row">

            <div class="col-md-12">

           <div class="custom-info-boxes">

            <?php foreach ( $custom_boxes as $box_info ) : ?>

                <div class="box-item">

                    <div class="box-img">

                       <figure> 

                            <?php 

                            echo wp_kses_post($img_obj->lifeline2_resize(wp_get_attachment_url(lifeline2_set( $box_info, 'image' ), 'full'), 230, 130, true));

                            ?>

                        </figure>



                        <a href="<?php echo esc_url( lifeline2_set( $box_info, 'btn_link' ) ); ?>"  ><?php echo wp_kses_post( lifeline2_set( $box_info, 'title' ) ); ?></a>

                    </div>

                </div>

            <?php endforeach; ?>

           </div>

       </div>

        <?php

    endif; 

        $output     = ob_get_contents();

        ob_clean();

        return $output;

    }

}