<?php
/**
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     3.5.5
 */
global $product;
?>
<li>
	<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
		<?php echo wp_kses_post($product->get_image()); ?>
		<?php echo wp_kses_post($product->get_title()); ?>
	</a>
	<?php if ( !empty( $show_rating ) ) echo wp_kses_post( $product->wc_get_rating_html()); ?>
	<?php echo wp_kses_post($product->get_price_html()); ?>
</li>
