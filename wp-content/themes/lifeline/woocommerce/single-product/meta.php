<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if ( !defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly
global $post, $product;
$cat_count = ( get_the_terms( $post->ID, 'product_tag' ) ) ? sizeof( get_the_terms( $post->ID, 'product_cat' ) ) : '';
$tag_count = ( get_the_terms( $post->ID, 'product_tag' ) ) ? sizeof( get_the_terms( $post->ID, 'product_tag' ) ) : '';
?>
<div class="product_meta">
	<?php do_action( 'woocommerce_product_meta_start' ); ?>
	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
		<?php $sku = $product->get_sku(); ?>
		<span class="sku_wrapper"><?php _e( 'SKU:', 'lifeline' ); ?> <span class="sku" itemprop="sku"><?php echo esc_attr( $sku ) ? $sku : __( 'n/a', 'lifeline' ); ?></span>.</span>
	<?php endif; ?>
	<?php echo wc_get_product_category_list( $product->get_id(), '', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'lifeline' ) . ' ', '.</span>' ); ?>
	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'lifeline' ) . ' ', '.</span>' ); ?>
	<?php do_action( 'woocommerce_product_meta_end' ); ?>
</div>
