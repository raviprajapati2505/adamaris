<?php
sh_custom_header();
$ThemeSettings = get_option( 'lifeline' );
$PageSettings = get_post_meta( get_the_ID(), '_page_settings', true );
$sidebar = sh_set( $ThemeSettings, 'author_page_sidebar' );
$sidepos = sh_set( $ThemeSettings, 'author_page_sidebar_pos' );

?>
<?php if ( sh_set( $ThemeSettings, 'author_page_image' ) ) : ?>
	<div class="top-image">
		<img src="<?php echo sh_set( $ThemeSettings, 'author_page_image' ); ?>" alt="<?php esc_attr_e( 'Image Not Found', 'lifeline' ); ?>" />
	</div>
<?php else: ?>
	<div class="no-top-image"></div>
<?php endif; ?>
<!-- Page Top Image -->
<section class="inner-page">
    <div class="container">

        <div class="page-title">
			<?php
			$BlogHeadingFont = sh_get_font_settings( array( 'blog_heading_font_size' => 'font-size', 'blog_heading_font_family' => 'font-family', 'blog_heading_font_style' => 'font-style', 'blog_heading_color' => 'color' ), ' style="', '"' );
			$BlogSubHeadingFont = sh_get_font_settings( array( 'blog_sub_heading_font_size' => 'font-size', 'blog_sub_heading_font_family' => 'font-family', 'blog_sub_heading_font_style' => 'font-style', 'blog_sub_heading_color' => 'color' ), ' style="', '"' );
			
			 if (sh_set($theme_options, 'author_page_heading'))
                    $Heading = sh_set($theme_options, 'author_page_heading');
                else
                    $Heading = (!$queried_object) ? esc_html__('Author', 'lifeline') : the_title();
                if (sh_set($theme_options, 'author_page_sub_heading'))
                    $SubHeading = sh_set($theme_options, 'author_page_sub_heading');
                else
                    $SubHeading = '';
			?>
			
			 <h1<?php echo esc_attr( $BlogHeadingFont ); ?>><?php echo esc_html($Heading); ?><span<?php echo esc_attr( $BlogSubHeadingFont ); ?>><?php echo esc_html( $SubHeading ); ?></span></h1>
        </div>

		<?php if ( $sidebar != '' && $sidepos == 'left' ) : ?>
			<div class="col-md-3">
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
		<?php endif; ?>

		<?php if ( $sidebar != '' ) : ?>
			<div class="col-md-9">
			<?php else: ?>
				<div class="left-content col-md-12">
				<?php endif; ?>  

				<?php while ( have_posts() ): the_post(); ?>

					<div class="blog-post">

						<h2><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

						<?php $PostForamt = get_post_meta( get_the_ID(), 'format', true ); ?>
							<?php if ( has_post_thumbnail() ): ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( '1170x325' ); ?></a>
							<?php endif; ?>
						<div class="blog-post-details">

							<ul class="post-meta">

								<li><a href=""  ><i class="icon-calendar-empty"></i><span><?php echo get_the_date( 'F' ); ?></span> <?php echo get_the_date( 'd,Y' ); ?></a></li>

								<?php
								$Author = get_the_author();
								if ( !empty( $Author ) ) :
									?>

									<li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"  ><i class="icon-user"></i><?php echo __( 'By', 'lifeline' ); ?> <?php echo get_the_author(); ?></a></li>

								<?php endif; ?>

								<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="icon-share-alt"></i> <?php the_category( ',', '' ); ?></a></li>

								<?php if ( sh_set( $PageSettings, 'location' ) ) : ?>

									<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="icon-map-marker"></i><?php echo __( 'In', 'lifeline' ) . ' ' . sh_set( $PageSettings, 'location' ); ?></a></li>

								<?php endif; ?>

							</ul>

							<div class="post-desc"><p><?php the_excerpt(); ?></p></div>

						</div>

					</div>                 
				<?php endwhile; ?>

				<div class="pagination-area"><?php _the_pagination(); ?></div>
			</div>
			<?php if ( $sidebar != '' && $sidepos == 'right' ) : ?>
				<div class="col-md-3">
					<?php dynamic_sidebar( $sidebar ); ?>
				</div>
			<?php endif; ?>

			<?php if ( sh_set( $ThemeSettings, 'page_comments_status' ) == 'true' ): ?> 

				<div class="comments"><?php comments_template(); ?></div>

			<?php endif; ?>
		</div>

</section>
<?php get_footer(); ?>
