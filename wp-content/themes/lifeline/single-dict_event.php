<?php
global $post_type;
sh_custom_header();
$ThemeSettings = get_option( 'lifeline' );
$settings = get_post_meta( get_the_ID(), '_dict_event_settings', true );
$sidebar = sh_set( $settings, 'sidebar' );
$col_class = ($sidebar) ? 'col-md-9' : 'col-md-12';
$sidebar_position = sh_set( $settings, 'sidebar_pos' );
//printr($sidebar_position);
?>
<?php if ( sh_set( $settings, 'top_image' ) ) : ?>
<div class="top-image"> <img src="<?php echo sh_set( $settings, 'top_image' ); ?>" /> </div>
<?php endif; ?>
<!-- Page Top Image -->

<section class="inner-page <?php echo ( sh_set( $settings, 'sidebar_pos' ) == 'left' ) ? ' switch' : ''; ?>">

    <div class="container">
		<div class="row">
			<?php if ( $sidebar_position == 'left' && is_active_sidebar($sidebar) ): ?>
                <div class="sidebar col-md-3 pull-left"><?php dynamic_sidebar( $sidebar ); ?></div>
			<?php endif; ?>
			<div class="<?php echo esc_attr($col_class); ?>">

				<?php
				while ( have_posts() ): the_post();
					$EventSettings = get_post_meta( get_the_ID(), '_dict_event_settings', true );
					?>

					<div id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>

						<?php if ( has_post_thumbnail() ): ?>
							<?php the_post_thumbnail( '1170x455' ); ?>  
						<?php endif; ?>
						<h1><?php the_title(); ?></h1>
						<div class="event-detail">
							<h2><?php _e( "EVENT DETAILS :", 'lifeline' ); ?></h2>

							<ul>
								<li><strong><?php _e( "Start Date:", 'lifeline' ); ?></strong><span><?php echo sh_set( $settings, 'start_date' ); ?></span></li>
								<li><strong><?php _e( "End Date:", 'lifeline' ); ?></strong><span><?php echo sh_set( $settings, 'end_date' ); ?></span></li>
								<li><strong><?php _e( "Start Time:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'start_time' ); ?></span></li>
								<li><strong><?php _e( "End Time:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'end_time' ); ?></span></li>
								<li><strong><?php _e( "Location:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'location' ); ?></span></li>
							</ul>
						</div>
						<?php if( sh_set($settings, 'show_organization_section') == 'true' ) : ?>
							<div class="event-detail">
								<h2><?php _e( "ORGANIZER :", 'lifeline' ); ?></h2>
								<ul>
									<li><strong><?php _e( "Organized by:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'organizer' ); ?></span></li>
									<li><strong><?php _e( "Mobile:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'contact' ); ?></span></li>
									<li><strong><?php _e( "Email:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'email' ); ?></span></li>
									<li><strong><?php _e( "Website:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'website' ); ?></span></li>
									<li><strong><?php _e( "Address:", 'lifeline' ); ?></strong><span><?php echo sh_set( $EventSettings, 'address' ); ?></span></li>
								</ul>
							</div>
						<?php endif; ?>
						<?php the_content(); ?>
					</div>
				<?php endwhile; ?>

				<?php if ( sh_set( $ThemeSettings, 'page_comments_status' ) == 'true' && (comments_open(get_the_id()))): ?> 

					<div class="comments"><?php comments_template(); ?></div>

				<?php endif; ?>
			</div>

			<?php if ( $sidebar_position == 'right' && is_active_sidebar($sidebar) ): ?>
				<div class="sidebar col-md-3 pull-right"><?php dynamic_sidebar( $sidebar ); ?></div>
			<?php endif; ?>

		</div>
    </div>

</section>

<?php get_footer(); ?>
