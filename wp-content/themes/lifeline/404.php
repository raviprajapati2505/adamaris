<?php
sh_custom_header();
$Settings = get_option('lifeline');
?>


<div class="top-image">
    <?php if (sh_set($Settings, '404_page_image')) : ?>
        <img src="<?php echo sh_set($Settings, '404_page_image'); ?>" alt="<?php esc_attr_e( 'Image Not Found', 'lifeline' ); ?>" />
    <?php else:?>
    <?php endif; ?>
</div>


<!--Page Top Image-->
<section class="inner-page">
    <div class="container">
        <div class="page-title">
            <?php if (sh_set($Settings, '404_page_heading')): ?>
                <h1><?php echo sh_set($Settings, '404_page_heading'); ?> <span><?php echo sh_set($Settings, '404_page_sub_heading'); ?></span></h1>
            <?php else: ?>
                <h1><?php esc_html_e('Error', 'lifeline'); ?> <span><?php esc_html_e('404', 'lifeline'); ?></span></h1>
            <?php endif; ?>
        </div>
        <!--Page Title-->
        <div class="error-page">
            <h2><?php echo sh_set($Settings, '404_page_main_title_colored') ? sh_set($Settings, '404_page_main_title_colored') : 404; ?></h2>
            <?php if(sh_set($Settings, '404_page_main_title_grey') || sh_set($Settings, '404_page_sub_title')):?>
                <p><?php echo sh_set($Settings, '404_page_main_title_grey'); ?> <span><?php echo sh_set($Settings, '404_page_sub_title'); ?></span></p>
            <?php else:?>
                <p><?php esc_html_e('Uh-oh! nothing found', 'lifeline');?></p>
            <?php endif;?>
        </div>
    </div>
    <div class="error-page-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="search-bar">
                                <form id="search-form" method="get">
                                    <input name="s" value="<?php echo get_search_query(); ?>" type="text" placeholder="<?php _e('Search', 'lifeline'); ?>" class="search">
                                    <input type="submit" value="" class="search-button">            
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php if (sh_set($Settings, '404_page_contents_heading')) : ?>
                        <h3><?php echo sh_set($Settings, '404_page_contents_heading'); ?></h3>
                    <?php endif; ?>
                    <?php if (sh_set($Settings, '404_page_content')) : ?>
                        <p><?php echo sh_set($Settings, '404_page_content'); ?></p>
                    <?php endif; ?>
                    <a href="<?php echo esc_url(home_url('/'));?>"  ><?php _e("Go To Home:", 'lifeline'); ?> </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
