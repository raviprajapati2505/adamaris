<?php
if (!class_exists('SH_Options')) {
    get_template_part('framework/theme_options/options');
}

function add_another_section($sections) {

    $sections[] = array(
        'title' => __('A Section added by hook', 'lifeline'),
        'desc' => __('<p class="description">This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.</p>', 'lifeline'),
        'icon' => trailingslashit(get_template_directory_uri()) . 'options/img/glyphicons/glyphicons_062_attach.png',
        'fields' => array()
    );

    return $sections;
}
//function
function change_framework_args($args) {
    return $args;
}

//function

function setup_framework_options() {
    $args = array();
//Set it to dev mode to view the class settings/info in the form - default is false
    $args['dev_mode'] = true;
//google api key MUST BE DEFINED IF YOU WANT TO USE GOOGLE WEBFONTS
//Remove the default stylesheet? make sure you enqueue another one all the page will look whack!
//Add HTML before the form
    $args['intro_text'] = __('<p>This is the HTML which can be displayed before the form, it isnt required, but more info is always better. Anything goes in terms of markup here, any HTML.</p>', 'lifeline');
//Setup custom links in the footer for share icons
    $args['share_icons']['twitter'] = array(
        'link' => 'http://twitter.com/lee__mason',
        'title' => 'Folow me on Twitter',
        'img' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_322_twitter.png'
    );
    $args['share_icons']['linked_in'] = array(
        'link' => 'http://uk.linkedin.com/pub/lee-mason/38/618/bab',
        'title' => 'Find me on LinkedIn',
        'img' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_337_linked_in.png'
    );
    $args['opt_name'] = 'lifeline';
    $args['menu_title'] = __('Theme Options', 'lifeline');
//Custom Page 4Title for options page - default is "Options"
    $args['page_title'] = __('Lifeline Theme Options', 'lifeline');
//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "nhp_theme_options"
    $args['page_slug'] = 'sh_theme_options';
    $args['page_type'] = 'submenu';
    $args['page_parent'] = 'themes.php';
//custom page location - default 100 - must be unique or will override other items
    $args['page_position'] = 27;
//Custom page icon class (used to override the page icon next to heading)
    $args['page_icon'] = 'icon-themes';

//Set ANY custom page help tabs - displayed using the new help tab API, show in order of definition
    $args['help_tabs'][] = array(
        'id' => 'sh-opts-1',
        'title' => __('Theme Information 1', 'lifeline'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'lifeline')
    );
    $args['help_tabs'][] = array(
        'id' => 'nhp-opts-2',
        'title' => __('Theme Information 2', 'lifeline'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'lifeline')
    );
//Set the Help Sidebar for the options page - no sidebar by default
    $args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'lifeline');
    if (count($_POST)) {
        if ($opt_name = sh_set($_POST, $args['opt_name']))
            update_option($args['opt_name'], $opt_name);
    }
    
    $sections = array();
    $sections[] = array(
        'title' => __('General Settings', 'lifeline'),
        'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
        'id' => 'general_settings',
        'children' => array(
            array(
                'title' => __('General Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'theme_general_settings',
                'fields' => array(
                    array(
                        'id' => 'dep_radio', //must be unique
                        'type' => 'radio', //builtin fields include:
                        'title' => __('Theme Color Scheme', 'lifeline'),
                        'desc' => __('Pick the color to apply for theme', 'lifeline'),
                        'options' => array('opt1' => 'General Color Scheme', 'opt2' => 'Predefined Color Scheme'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => 'opt1',
                    ),
                    array(
                        'id' => 'theme_general_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Theme Color Scheme', 'lifeline'),
                        'desc' => __('Pick the color to apply for theme', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '#4FC0AA',
                        'dependent' => 'opt1',
                        ''
                    ),
                    array(
                        'type' => 'select', //builtin fields include:
                        'id' => 'theme_color_scheme',
                        'title' => __('Predefined Color Schemes', 'lifeline'),
                        'options' => array(
                            'brown' => 'Brown',
                            'bright-red' => 'Bright Red',
                            'yellow' => 'Yellow',
                            'green' => 'Green',
                            'hunter-green' => 'Hunter Green',
                            'light-pink' => 'Light Pink',
                            'orange' => 'Orange',
                            'pink' => 'Pink',
                            'red' => 'Red',
                            'sea-green' => 'Sea Green',
                            'bourbon' => 'Bourbon',
                            'como' => 'Como',
                            'deep-pink' => 'Deep Pink',
                            'drove-gray' => 'Drove Gray',
                            'pacific-blue' => 'Pacific Blue',
                        ),
                        'desc' => __('Choose One of Our Predefined Color Schemes', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                        'dependent' => 'opt2',),
                    array(
                        'id' => 'sh_rtl', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('RTL(Right to Left)', 'lifeline'),
                        'desc' => __('Turn RTL On or Off', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                    ),
                    
                    array(
                        'id' => 'sh_seo_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('SEO Status', 'lifeline'),
                        'desc' => __('Turn This Option On to enable SEO', 'lifeline'),
                    ),
                   
                    array(
                        'id' => 'sh_post_types_seo', //must be unique
                        'type' => 'multi_select', //builtin fields include:
                        'title' => __('Post Types', 'lifeline'),
                        'desc' => __('Select Post Types for SEO.', 'lifeline'),
                        'options' => sh_get_post_types(),
                    ),
                    array(
                        'id' => 'homepage_meta_title', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Homepage Meta Title', 'lifeline'),
                        'desc' => __('Enter Homepage Meta Title', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'homepage_meta_desc', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Homepage Meta Description', 'lifeline'),
                        'desc' => __('Enter Homepage Meta Description.', 'lifeline'),
                    ),
                    array(
                        'id' => 'homepage_meta_keywords', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Homepage Meta Keywords', 'lifeline'),
                        'desc' => __('Enter Homepage Meta Keywords.', 'lifeline'),
                    ),
                    array(
                        'id' => 'custom_css', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Custom Style', 'lifeline'),
                        'desc' => __('Please Input custom css Enclosed with Style Tag.', 'lifeline'),
                    ),
                ),
                
            ),
            array(
                'title' => __('Archive Pages Meta Title Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains top bar settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'archive_pages_settings',
                'fields' => array(
                    array(
                        'id' => 'seperator', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Seperator', 'lifeline'),
                        'desc' => __('Enter Seperator Character', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'title_setting', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('After Seperator', 'lifeline'),
                        'desc' => __('Choose what should be displayed after seperator.', 'lifeline'),
                        'options' => array('' => 'No Description', 'name' => 'Site Title', 'description' => 'Site Description')
                    ),
                ),
            ),
            array(
                'title' => __('Top Bar Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains top bar settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'top_bar_settings',
                'fields' => array(
                    array(
                        'id' => 'sh_topbar_custom_setting', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Topbar', 'lifeline'),
                        'desc' => __('Turn This Option On to show Topbar" , Stikcey.', 'lifeline'),
                    ),
                    array(
                        'id' => 'topbar_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Topbar Color Scheme', 'lifeline'),
                        'desc' => __('Pick the color to apply for top bar', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '#111111',
                    ),
                    array(
                        'id' => 'topbar_font_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Topbar Font Color Scheme', 'lifeline'),
                        'desc' => __('Pick the color to apply for top bar font', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '#9c9191',
                    ),
                    array(
                        'id' => 'topbar_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Topbar Font Size', 'lifeline'),
                        'desc' => __('Enter font size of topbar with PX e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '12px',
                    ),
                     array(
                        'id' => 'topbar_text_font_family', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Font Family', 'lifeline'),
                        'desc' => '',
                        'options' => sh_set(sh_google_fonts(), 'family')
                    ),
                   
                    array(
                        'id' => 'header_address', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Address', 'lifeline'),
                        'desc' => __('Enter address to be displayed in top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'header_phone_number', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Phone Number', 'lifeline'),
                        'desc' => __('Enter phone number to be displayed in top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'header_phone_number_link', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Phone Hyperlink', 'lifeline'),
                        'desc' => __('Enter phone number Hyperlink', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'header_email_address', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Email Address', 'lifeline'),
                        'desc' => __('Enter email address to be displayed in top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'header_email_link', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Email Hyperlink', 'lifeline'),
                        'desc' => __('Enter email Hyperlink', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                ),
               
            ),
            array(
                'title' => __('Choose Header Style', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_general_settings',
                'fields' => array(
                    array(
                        'type' => 'image_boxes',
                        'id' => 'custom_header',
                        'title' => '',
                        'attributes' => array('style' => 'width:40%'),
                        'options' => array(
                            'dafault' => array('label' => '<ul><li>Left Side Logo</li><li>Right Side Menu</li></ul>', 'img' => '/images/Sticky-Header.jpg'),
                            'toggle' => array('label' => '<ul><li>Header Toggle</li><li>Right Side Menu</li></ul>', 'img' => '/images/toggle-Header.jpg'),
                            'middle_aligned' => array('label' => '<ul><li>Middle Logo</li><li>Down Menu</li></ul>', 'img' => '/images/with-logo-in-the-mid.jpg'),
                            'middle_aligned' => array('label' => '<ul><li>Middle Logo</li><li>Down Menu</li></ul>', 'img' => '/images/with-logo-in-the-mid.jpg'),
                            'social-icon' => array('label' => '<ul><li>Header With Social icons</li></ul>', 'img' => '/images/header-social.jpg'),
                            'counter' => array('label' => '<ul><li>Header With Counter</li></ul>', 'img' => '/images/header-count.jpg'),
                        ),
                        'desc' => __('Sort the modules through drag & drop.', 'lifeline'),
                        'std' => 'dafault',
                        'settings' => array('hide_title' => true)
                    ),
                    
                    array(
                        'id' => 'sh_custom_stickey_menu', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Sticky Header', 'lifeline'),
                        'desc' => __('Turn This Option On to Make any "Non Sticky Header" , Stikcey.', 'lifeline'),
                    ),
                    array(
	                    'id' => '', //must be unique
	                    'type' => 'heading', //builtin fields include:
	                    'heading' => __('Settings for Header with Middle Logo', 'lifeline')
                    ),
                    array(
	                    'id' => 'sh_enable_overlap', //must be unique
	                    'type' => 'button_set', //builtin fields include:
	                    'title' => __('Header Overlap', 'lifeline'),
	                    'desc' => __('Enable or Disable overlap in this header style', 'lifeline'),
                    ),
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Header Settings for Header with Social Icons', 'lifeline')
                    ),
                    array(
                        'id' => 'sh_show_soical_icons', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Social Icons', 'lifeline'),
                        'desc' => __('Show or hide social icons in this header style', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_show_donate_btn', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Donate Button', 'lifeline'),
                        'desc' => __('Show or hide Donate Button in this header style.', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_show_donate_btn_txt', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Button Text', 'lifeline'),
                        'desc' => __('Enter the text for donation button.', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_donate_btn_link', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Button link', 'lifeline'),
                        'desc' => __('Enter the cutom link for donation button. "Note:" if you will not enter the link donation popup will work', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_serch_box', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Search Box', 'lifeline'),
                        'desc' => __('Turn This Option On to show Search Box', 'lifeline'),
                    ),
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Header Setting for Header with Counter', 'lifeline')
                    ),
                    array(
                        'id' => 'sh_show_event_counter', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Event', 'lifeline'),
                        'desc' => __('Show or hide Event Countdown in this header style.', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_counter_post', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Select Event', 'lifeline'),
                        'desc' => '',
                        'options' => sh_get_posts_array('dict_event')
                    ),
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Logo Settings', 'lifeline')
                    ),
                    array(
                        'id' => 'site_favicon', //must be unique
                        'type' => 'upload', //builtin fields include:
                        'title' => __('Upload your Favicon From Here', 'lifeline'),
                        'title' => __('Favicon', 'lifeline'),
                        'desc' => __('The Favicon size shold be 16x16 px.', 'lifeline'),
                        'std' => SH_URL.'images/favicon.png'
                    ),
                    array(
                        'id' => 'header_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Select the Header Backound Color', 'lifeline'),
                        'desc' => __('Pick the color to apply in header', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'logo_text_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Use Logo Text', 'lifeline'),
                        'desc' => __('Use Text Instead of Logo Click the Button to On of Off the Text in the header instead of Logo', 'lifeline'),
                    ),
                    array(
                        'id' => 'logo_text', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Logo Text', 'lifeline'),
                        'desc' => __('Enter the Text here, you want to as Logo', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'logo_text_color', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Logo Color', 'lifeline'),
                        'desc' => __('Pick color for Logo text', 'lifeline'),
                    ),
                    array(
                        'id' => 'logo_font', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Logo Font Settings', 'lifeline'),
                        'desc' => __('Set Logo Font Settings', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'logo_text_font_size', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Size', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(2, 40, 2), range(2, 40, 2))
                            ),
                            array(
                                'id' => 'logo_text_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                            array(
                                'id' => 'logo_text_font_style', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Style', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'style')
                            ),
                        )
                    ),
                    array(
                        'id' => 'site_salogan', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Site Slogan', 'lifeline'),
                        'desc' => __('Enter the Sub-heading or Tag Line to show below Logo Upload your site Logo here', 'lifeline'),
                    ),
                    array(
                        'id' => 'slogan_settings', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Slogan Settings', 'lifeline'),
                        'desc' => __(' Choose the salogan settings here', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'salogan_font_size', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Size', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(2, 40, 2), range(2, 40, 2))
                            ),
                            array(
                                'id' => 'salogan_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                            array(
                                'id' => 'salogan_font_style', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Style', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'style')
                            ),
                        )
                    ),
                    array(
                        'id' => 'logo_image', //must be unique
                        'type' => 'upload', //builtin fields include:
                        'title' => __('Logo Image', 'lifeline'),
                        'desc' => __('Upload Logo here but be sure that the Logo Text button should be Off', 'lifeline'),
                    ),
                    array(
                        'id' => 'logo_size', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Logo Size', 'lifeline'),
                        'desc' => __('Select the Height and Width of the Logo image to show in the header', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'logo_width', //must be unique
                                'type' => 'text', //builtin fields include:
                                'title' => __('Logo Width', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(6, 100, 2), range(6, 100, 2))
                            ),
                            array(
                                'id' => 'logo_height', //must be unique
                                'type' => 'text', //builtin fields include:
                                'title' => __('Logo Height', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(6, 36, 2), range(6, 36, 2))
                            ),
                        )
                    ),
                ),
            ),
            array(
                'title' => __('Responsive Header Style', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme responsive header</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'responsive_header_settings',
                'fields' => array( 
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Responsive Header Top Bar Settings', 'lifeline')
                    ),
                    array(
                        'id' => 'sh_responsive_header_top_bar', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Responsive Header Top Bar', 'lifeline'),
                        'desc' => __('Enabel to show top bar in responsive header', 'lifeline'),
                    ),
                    array(
                        'id' => 'responsive_header_address', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Address', 'lifeline'),
                        'desc' => __('Enter address to be displayed in responsive top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'responsive_header_phone_number', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Phone Number', 'lifeline'),
                        'desc' => __('Enter phone number to be displayed in top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'responsive_header_email_address', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Email Address', 'lifeline'),
                        'desc' => __('Enter email address to be displayed in top bar', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Header Settings for Header with Social Icons', 'lifeline')
                    ),
                    array(
                        'id' => 'sh_show_responsive_soical_icons', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Social Icons', 'lifeline'),
                        'desc' => __('Show or hide social icons in this header style', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_show_responsive_donate_btn', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Donate Button', 'lifeline'),
                        'desc' => __('Show or hide Donate Button in this header style.', 'lifeline'),
                    ),
                    array(
                        'id' => 'sh_show_responsive_donate_btn_txt', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Button Text', 'lifeline'),
                        'desc' => __('Enter the text for donation button.', 'lifeline'),
                    ),
                    
                    array(
                        'id' => '', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Logo Settings', 'lifeline')
                    ),
                    
                    array(
                        'id' => 'responsive_logo_text_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Use Logo Text', 'lifeline'),
                        'desc' => __('Use Text Instead of Logo Click the Button to On of Off the Text in the header instead of Logo', 'lifeline'),
                    ),
                    array(
                        'id' => 'responsive_logo_text', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Logo Text', 'lifeline'),
                        'desc' => __('Enter the Text here, you want to as Logo', 'lifeline'),
                        'attributes' => array('class' => 'input-field')
                    ),
                    array(
                        'id' => 'responsive_logo_text_color', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Logo Color', 'lifeline'),
                        'desc' => __('Pick color for Logo text', 'lifeline'),
                    ),
                    array(
                        'id' => 'responsive_logo_font', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Logo Font Settings', 'lifeline'),
                        'desc' => __('Set Logo Font Settings', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'responsive_logo_text_font_size', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Size', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(2, 40, 2), range(2, 40, 2))
                            ),
                            array(
                                'id' => 'responsive_logo_text_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                            array(
                                'id' => 'responsive_logo_text_font_style', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Style', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'style')
                            ),
                        )
                    ),
                    array(
                        'id' => 'responsive_site_salogan', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Site Slogan', 'lifeline'),
                        'desc' => __('Enter the Sub-heading or Tag Line to show below Logo Upload your site Logo here', 'lifeline'),
                    ),
                    array(
                        'id' => 'responsive_slogan_settings', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Slogan Settings', 'lifeline'),
                        'desc' => __(' Choose the salogan settings here', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'responsive_salogan_font_size', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Size', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(2, 40, 2), range(2, 40, 2))
                            ),
                            array(
                                'id' => 'responsive_salogan_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                            array(
                                'id' => 'responsive_salogan_font_style', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Style', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'style')
                            ),
                        )
                    ),
                    array(
                        'id' => 'responsive_logo_image', //must be unique
                        'type' => 'upload', //builtin fields include:
                        'title' => __('Logo Image', 'lifeline'),
                        'desc' => __('Upload Logo here but be sure that the Logo Text button should be Off', 'lifeline'),
                    ),
                    array(
                        'id' => 'responsive_logo_size', //must be unique
                        'type' => 'multi_fields', //builtin fields include:
                        'title' => __('Logo Size', 'lifeline'),
                        'desc' => __('Select the Height and Width of the Logo image to show in the header', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'responsive_logo_width', //must be unique
                                'type' => 'text', //builtin fields include:
                                'title' => __('Logo Width', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(6, 100, 2), range(6, 100, 2))
                            ),
                            array(
                                'id' => 'responsive_logo_height', //must be unique
                                'type' => 'text', //builtin fields include:
                                'title' => __('Logo Height', 'lifeline'),
                                'desc' => '',
                                'options' => array_combine(range(6, 36, 2), range(6, 36, 2))
                            ),
                        )
                    ),
                ),
            ),
            array(
                'title' => __('Footer Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains footer options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_footer_settings',
                'fields' => array(
                    array(
                        'id' => 'show_footer', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Footer', 'lifeline'),
                        'desc' => __('Enable / Disable to show widget area section for footer', 'lifeline'),
                        'options' => array('1' => 'Enable', '0' => 'Disable')
                    ),
                    array(
	                    'id' => 'sticky_footer', //must be unique
	                    'type' => 'button_set', //builtin fields include:
	                    'title' => __('Sticky Footer', 'lifeline'),
	                    'desc' => __('Enable / Disable for sticky footer', 'lifeline'),
	                    'options' => array('1' => 'Enable', '0' => 'Disable')
                    ),
                    array(
                        'id' => 'column_type', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Select Widgets Columns', 'lifeline'),
                        'desc' => __('Select how many columns you want to show in footer widget area.', 'lifeline'),
                        'options' => array('col-md-3' => '4 Columns', 'col-md-4' => '3 Columns', 'col-md-6' => '2 Columns')
                    ),
                    array(
                        'id' => 'footer_light', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Light Footer', 'lifeline'),
                        'desc' => __('Enable / Disable to show footer in light scheme', 'lifeline'),
                        'options' => array('1' => 'Enable', '0' => 'Disable')
                    ),
                    array(
                        'id' => 'footer_bg', //must be unique
                        'type' => 'upload', //builtin fields include:
                        'title' => __('Footer Background Image', 'lifeline'),
                        'desc' => __('Upload Image to change Footer Background', 'lifeline'),
                        'std' => SH_URL.'/images/footer-bg.png'
                    ),
                    array(
                        'id' => 'footer_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('Footer Background Color', 'lifeline'),
                        'desc' => __('Pick the color to apply in footer background', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    
                    array(
                        'id' => 'footer_copyright', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Footer Copyright Text', 'lifeline'),
                        'desc' => __('Enter the Copyrights Text here to show in the Footer You can use HTML tags in this area as well', 'lifeline'),
                    ),
                    array(
                        'id' => 'footer_analytics', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Footer Analytics / Scripts', 'lifeline'),
                        'desc' => __('In this area you can put Google Analytics Code or any other Script that you want to be included in the footer before the Body tag. Note: do not use script tags.', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('APIs Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains apis configuration settings.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_apis_settings',
                'fields' => array(
                    array(
                        'id' => 'youtube_section', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Youtube Option', 'lifeline')
                    ),
                    array(
                        'id' => 'youtube_api_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Youtube API Key', 'lifeline'),
                        'desc' => __('Enter the youtube api key for youtube videos data', 'lifeline'),
                    ),
                    array(
                        'id' => 'mailchimp_section', //must be unique
                        'type' => 'heading', //builtin fields include:
                        'heading' => __('Mailchimp Option', 'lifeline')
                    ),
                    array(
                        'id' => 'mailchimp_list_id', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Mailchimp List ID:', 'lifeline'),
                        'desc' => __('Enter the mailchimp list id for mailchimp newsletter subscriptions', 'lifeline'),
                    ),
                    array(
                        'id' => 'mailchimp_api_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Mailchimp API Key:', 'lifeline'),
                        'desc' => __('Enter the mailchimp API key for mailchimp newsletter subscriptions', 'lifeline'),
                    ),
                ),
            ),
        )
    );
    /** Contact Section */
    $sections[] = array(
        'title' => __('Contact Page Options', 'lifeline'),
        'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
        'id' => 'contact_page_options',
        'children' => array(
            array(
                'title' => __('Contact Page Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains contact options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_contact_settings',
                'fields' => array(
                    array(
                        'id' => 'contact_email', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Contact Email', 'lifeline'),
                        'desc' => __('Enter the email address where do you want to recieve emails sent through contact form', 'lifeline'),
                        'std' => get_bloginfo('admin_email')
                    ),
                    array(
                        'id' => 'success_message', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Success Message', 'lifeline'),
                        'desc' => __('Enter the Success Code to show once the email is sent through contact form', 'lifeline'),
                    ),
                    array(
                        'id' => 'captcha_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Captcha Status', 'lifeline'),
                        'desc' => __('Enable / disable google recaptcha on contact form', 'lifeline'),
                    ),
                    array(
                        'id' => 'captcha_api', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Captcha API Key', 'lifeline'),
                        'desc' => __('Enter the captcha API key of your Google account.', 'lifeline'),
                    ),
                    array(
                        'id' => 'captcha_secret_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Captcha Secret', 'lifeline'),
                        'desc' => __('Enter the secret key of your Google account.', 'lifeline'),
                    ),
                    array(
                        'id' => 'google_map_code', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Google Map Code', 'lifeline'),
                        'desc' => __('Enter the Google Map Code of your company or office to be shown on Contact Us page.', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('Contact Information', 'lifeline'),
                'desc' => __('<p class="description">This section contains contact options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'contact_information',
                'fields' => array(
                     
                    array(
                        'id' => 'contact_page_address', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('ADDRESS', 'lifeline'),
                        'desc' => __('Enter the Address you want to show on Contact Page', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_phone', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Phone', 'lifeline'),
                        'desc' => __('Enter the Phone Number you want to show on Contact Page', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_phone_link', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Phone Hyperlink', 'lifeline'),
                        'desc' => __('Enter Phone Hyperlink if you want.', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
	                    'id' => 'contact_page_fax', //must be unique
	                    'type' => 'text', //builtin fields include:
	                    'title' => __('Fax Number', 'lifeline'),
	                    'desc' => __('Enter the Fax Number you want to show on Contact Page', 'lifeline'),
	                    'attributes' => array('style' => 'width:40%'),
	                    'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_email', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Email', 'lifeline'),
                        'desc' => __('Enter the Email Address you want to show on Contact Page', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_email_link', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Email Hyperlink', 'lifeline'),
                        'desc' => __('Enter the Email Hyperlink if you want.', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_website', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Website', 'lifeline'),
                        'desc' => __('Enter the Website URL you want to show on Contact Page', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'id' => 'contact_page_text', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Text', 'lifeline'),
                        'desc' => __('Enter the Text you want to show on Contact Page', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                ),
            ),
            array(
                'title' => __('Social Media', 'lifeline'),
                'desc' => __('<p class="description">This section contains contact options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'contact_social_media',
                'fields' => array(
                    array(
                        'id' => 'contact_text_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Social Media Area in contact page.', 'lifeline'),
                        'desc' => __('Enable to show social media area in contact page', 'lifeline'),
                    ),
                    array(
                        'id' => 'social_section_title', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Title', 'lifeline'),
                        'desc' => __('Enter the Title you want to show in Social Networking Section', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'contact_rss', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('RSS Link', 'lifeline'),
                        'desc' => __('Enter the RSS link', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'contact_facebook', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Facebook Link', 'lifeline'),
                        'desc' => __('Enter the Facebook Link', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_twitter', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Twitter Link', 'lifeline'),
                        'desc' => __('Enter the Twitter Link', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_linkedin', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Linkedin Link', 'lifeline'),
                        'desc' => __('Enter Linkedin Link', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_gplus', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Google Plus', 'lifeline'),
                        'desc' => __('Enter the Google Plus Link.', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_pintrest', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Pintrest Link', 'lifeline'),
                        'desc' => __('Enter the Pinterest Link.', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_instagram', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Instagram Link', 'lifeline'),
                        'desc' => __('Enter the Instagram Link.', 'lifeline'),
                    ),
                    array(
                        'id' => 'contact_youtube', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Youtube Link', 'lifeline'),
                        'desc' => __('Enter the Youtube Link.', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('Countries Slider', 'lifeline'),
                'desc' => __('<p class="description">This section contains contact options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'contact_countries',
                'fields' => array(
                    array(
                        'id' => 'contact_text_counter', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Show Countries Slider Area', 'lifeline'),
                        'desc' => __('Enable to show countries slider area in contact page', 'lifeline'),
                    ),
                    array(
                        'id' => 'country_section_title', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Title', 'lifeline'),
                        'desc' => __('Enter the Title you want to show in countries Slider', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'country_section_text', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Text', 'lifeline'),
                        'desc' => __('Enter the Text you want to show in countries Slider', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'contact_countries', //must be unique
                        'type' => 'multi_group', //builtin fields include:
                        'title' => __('Add Countries', 'lifeline'),
                        'desc' => __('Add Countries here ', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                        'field' => array(
                            array(
                                'id' => 'contact_country_img', //must be unique
                                'type' => 'upload', //builtin fields include:
                                'title' => __('Country Image ', 'lifeline'),
                                'desc' => __('Upload Country Image', 'lifeline'),
                                'attributes' => array('style' => 'width:40%'),
                                'std' => ''
                            ),
                        )
                    ),
                ),
            ),
        )
    );
	/** Template Section */
	$sections[] = array(
		'title' => __('Template Options', 'lifeline'),
		'desc' => __('<p class="description">This section contains general options about the Template Settings.</p>', 'lifeline'),
		'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
		'id' => 'template_page_options',
		'children' => array(
			array(
				'title' => __('Event Template Settings', 'lifeline'),
				'desc' => __('<p class="description">This section contains Event Template options.</p>', 'lifeline'),
				'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
				'id' => 'sub_event_settings',
				'fields' => array(
					array(
						'id' => 'event_order', //must be unique
						'type' => 'select', //builtin fields include:
						'title' => __('Event Order', 'lifeline'),
						'desc' => __('You can select Event Order Ascending or Descending', 'lifeline'),
						'options' => array('' => 'No Order', 'ASC' => 'Ascending', 'DESC' => 'Descending')
					),
					array(
						'id' => 'event_order_by', //must be unique
						'type' => 'select', //builtin fields include:
						'title' => __('Event Order By', 'lifeline'),
						'desc' => __('You can select Event Order By Title or Publish Date', 'lifeline'),
						'options' => array('' => 'No Order By', 'title' => 'Event Title', 'date' => 'Event Publish Date')
					),
				),
			),
		)
	);
    /** Font Settings */
    $sections[] = array(
        'title' => __('Font Options', 'lifeline'),
        'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
        'id' => 'font_options',
        'children' => array(
            array(
                'title' => __('Heading Fonts', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_heading_fonts',
                'fields' => array(
                    array(
                        'id' => 'sh_use_custom_fonts', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable Custom Fonts settings', 'lifeline'),
                        'desc' => __('enable this to use custom heading fonts settings', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                    ),
                    array(
                        'id' => 'h1_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H1 Color', 'lifeline'),
                        'desc' => __('Pick the color for h1', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',

                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h1_typography',
                        'title' => __('H1 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H1 ', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h1_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                    array(
                        'id' => 'h1_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H1 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h1 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '30px',
                    ),
                    array(
                        'id' => 'h2_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H2 Color', 'lifeline'),
                        'desc' => __('Pick the color for h2', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h2_typography',
                        'title' => __('H2 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H2', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h2_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                     array(
                        'id' => 'h2_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H2 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h2 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '26px',
                    ),
                     array(
                        'id' => 'h3_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H3 Color', 'lifeline'),
                        'desc' => __('Pick the color for h3', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h3_typography',
                        'title' => __('H3 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H3', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h3_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                     array(
                        'id' => 'h3_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H3 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h3 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '20px',
                    ),
                     array(
                        'id' => 'h4_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H4 Color', 'lifeline'),
                        'desc' => __('Pick the color for h4', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h4_typography',
                        'title' => __('H4 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H4', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h4_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                     array(
                        'id' => 'h4_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H4 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h4 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '19px',
                    ),
                     array(
                        'id' => 'h5_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H5 Color', 'lifeline'),
                        'desc' => __('Pick the color for h5', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h5_typography',
                        'title' => __('H5 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H5', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h5_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                     array(
                        'id' => 'h5_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H5 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h5 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '17px',
                    ),
                     array(
                        'id' => 'h6_color_scheme', //must be unique
                        'type' => 'color', //builtin fields include:
                        'title' => __('H6 Color', 'lifeline'),
                        'desc' => __('Pick the color for h6', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                        'std' => '',
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'h6_typography',
                        'title' => __('H6 Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of H6', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'h6_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                     array(
                        'id' => 'h6_font_size', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('H6 Font Size', 'lifeline'),
                        'desc' => __('Enter font size of h6 in px e.g 12px', 'lifeline'),
                        'attributes' => array('class' => 'input-field'),
                        'std' => '15px',
                    ),
                ),
            ),
            array(
                'title' => __('Body Font', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_body_font',
                'fields' => array(
                    array(
                        'type' => 'multi_fields',
                        'id' => 'body_typography',
                        'title' => __('Body Font Options', 'lifeline'),
                        'desc' => __('Change the Typography Settings of Body tag', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'body_color_scheme', //must be unique
                                'type' => 'color', //builtin fields include:
                                'title' => __('Select the color for theme font', 'lifeline'),
                                'desc' => __('Pick the color to apply in body font', 'lifeline'),
                                'attributes' => array('style' => 'width:40%'),
                                'std' => '',
                            ),
                            array(
                                'id' => 'body_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                            array(
                                'id' => 'body_font_size', //must be unique
                                'type' => 'text', //builtin fields include:
                                'title' => __('Body Font Size', 'lifeline'),
                                'desc' => __('Enter font size of body in px e.g 12px', 'lifeline'),
                                'attributes' => array('class' => 'input-field'),
                                'std' => '12px',
                            ),
                        )
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'grey_area_typography',
                        'title' => __('Grey Area Typography', 'lifeline'),
                        'desc' => __('Change the Typography Settings of Grey Area.', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'grey_area_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                    array(
                        'type' => 'multi_fields',
                        'id' => 'footer_typography',
                        'title' => __('Footer Options', 'lifeline'),
                        'desc' => __('Change the Typography Settings of Footer area', 'lifeline'),
                        'fields' => array(
                            array(
                                'id' => 'footer_font_family', //must be unique
                                'type' => 'select', //builtin fields include:
                                'title' => __('Font Family', 'lifeline'),
                                'desc' => '',
                                'options' => sh_set(sh_google_fonts(), 'family')
                            ),
                        )
                    ),
                ),
            ),
        )
    );
    $sections[] = array(
        'title' => __('Donation Settings', 'lifeline'),
        'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
        'id' => 'donation_settings',
        'children' => array(
            array(
                'title' => __('Donation Settings', 'lifeline'),
                'desc' => __('<p class="description">This section contains general options about the theme.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_donation_settings',
                'fields' => array(
                    array(
                        'id' => 'enable_paypal', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable PayPal', 'lifeline'),
                        'desc' => __('Enable to show PayPal options.', 'lifeline')
                    ),
                    array(
                        'id' => 'paypal_type', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Paypal Type', 'lifeline'),
                        'desc' => __('Select Which Paypal Version you want to use - Live or Sandbox', 'lifeline'),
                        'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
                    ),
                    array(
                        'id' => 'donate_method', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Donate Popup', 'lifeline'),
                        'desc' => __('Enable to show popup box for donation', 'lifeline')
                    ),
                    array(
                        'id' => 'donation_bar', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable to show donation bar in donation Popup', 'lifeline'),
                       
                    ),
                    array(
                        'id' => 'paypal_title', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Title', 'lifeline'),
                        'desc' => __('Enter the title show on header paypal donation section', 'lifeline'),
                    ),
                    array(
                        'type' => 'select', //builtin fields include:
                        'id' => 'currency_code',
                        'title' => __('Currency Code', 'lifeline'),
                        'options' => sh_get_currencies(),
                        'desc' => __('Select Currency Symbol', 'lifeline'),
                        'attributes' => array('style' => 'width:40%'),
                    ),
                    array(
                        'id' => 'paypal_currency', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Currency Symbol', 'lifeline'),
                        'std' => '$'
                    ),
                    array(
                        'id' => 'paypal_raised', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Raised', 'lifeline'),
                        'std' => '0'
                    ),
                    array(
                        'id' => 'paypal_target', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Target', 'lifeline'),
                        'std' => '25000'
                    ),
                    array(
                        'id' => 'paypal_contact', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Contact Number', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'paypal_username', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Paypal Username', 'lifeline'),
                        'desc' => __('Enter the paypal username. To get API username <a href="http://developer.paypal.com">visit</a>', 'lifeline'),
                        'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
                    ),
                    array(
                        'id' => 'paypal_api_username', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Paypal API Username', 'lifeline'),
                        'desc' => __('Enter the paypal API username', 'lifeline'),
                    ),
                    array(
                        'id' => 'paypal_api_password', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Paypal API Password', 'lifeline'),
                        'desc' => __('Enter the paypal api password', 'lifeline'),
                    ),
                    array(
                        'id' => 'paypal_api_signature', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Paypal API Signature', 'lifeline'),
                        'desc' => __('Enter the paypal api signature', 'lifeline'),
                    ),
                    array(
                        'id' => 'paypal_note', //must be unique
                        'type' => 'textarea', //builtin fields include:
                        'title' => __('Note', 'lifeline'),
                        'desc' => __('Enter the note to show on donation section in header', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('Credit Card', 'lifeline'),
                'desc' => __('<p class="description">This section contains Credit Card options.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'credit_card',
                'fields' => array(
                    array(
                        'id' => 'enable_stripe', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable Stripe', 'lifeline'),
                        'desc' => __('Enable to show stripe credit card options.', 'lifeline')
                    ),
                    array(
                        'id' => 'credit_card_secret_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Stripe Secret Key', 'lifeline'),
                        'desc' => __('Enter stripe secret key', 'lifeline'),
                    ),
                    array(
                        'id' => 'credit_card_publish_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Stripe Publishable Key', 'lifeline'),
                        'desc' => __('Enter stripe Publishable key', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('2Checkout', 'lifeline'),
                'desc' => __('<p class="description">This section contains 2checkout options.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'checkout2',
                'fields' => array(
                    array(
                        'id' => 'enable_checkout2', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable 2Checkout', 'lifeline'),
                        'desc' => __('Enable to show 2checkout options.', 'lifeline')
                    ),
                    array(
                        'id' => 'checkout2_mode', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Type', 'lifeline'),
                        'options' => array('false' => 'Live', 'true' => 'Sandbox')
                    ),
                    array(
                        'id' => 'checkout2_account_number', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Account Number', 'lifeline'),
                        'desc' => __('Enter your 2Checkout Account Number', 'lifeline'),
                    ),
                    array(
                        'id' => 'checkout2_publish_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('2Checkout Publishable Key', 'lifeline'),
                        'desc' => __('Enter 2Checkout Publishable key', 'lifeline'),
                    ),
                    array(
                        'id' => 'checkout2_private_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('2Checkout Private Key', 'lifeline'),
                        'desc' => __('Enter 2Checkout Private key', 'lifeline'),
                    ),
                ),
            ),
            array(
                'title' => __('Braintree', 'lifeline'),
                'desc' => __('<p class="description">This section contains braintree options.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'braintree',
                'fields' => array(
                    array(
                        'id' => 'enable_braintree', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Enable Braintree', 'lifeline'),
                        'desc' => __('Enable to show braintree options.', 'lifeline')
                    ),
                    array(
                        'id' => 'braintree_mode', //must be unique
                        'type' => 'select', //builtin fields include:
                        'title' => __('Type', 'lifeline'),
                        'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
                    ),
                    array(
                        'id' => 'braintree_merchant_id', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Merchant ID', 'lifeline'),
                        'desc' => __('Enter your Merchant Account Number', 'lifeline'),
                    ),
                    array(
                        'id' => 'braintree_publish_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Braintree Publishable Key', 'lifeline'),
                        'desc' => __('Enter braintree Publishable key', 'lifeline'),
                    ),
                    array(
                        'id' => 'braintree_private_key', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Braintree Private Key', 'lifeline'),
                        'desc' => __('Enter Braintree Private key', 'lifeline'),
                    ),
                ),
            ),
            array(
	            'title' => __('PayUMoney', 'lifeline'),
	            'desc' => __('<p class="description">This section contains PayUmoney options.</p>', 'lifeline'),
	            'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
	            'id' => 'payumoney',
	            'fields' => array(
		            array(
			            'id' => 'enable_payumoney', //must be unique
			            'type' => 'button_set', //builtin fields include:
			            'title' => __('Enable PayUMoney', 'lifeline'),
			            'desc' => __('Enable to show payumoney options.', 'lifeline')
		            ),
		            array(
			            'id' => 'payumoney_mode', //must be unique
			            'type' => 'select', //builtin fields include:
			            'title' => __('Type', 'lifeline'),
			            'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
		            ),
		            array(
			            'id' => 'payumoney_key', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('PayUMoney Key', 'lifeline'),
			            'desc' => __('Enter PayUMoney Account Key', 'lifeline'),
		            ),
		            array(
			            'id' => 'payumoney_salt', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('PayUMoney Salt', 'lifeline'),
			            'desc' => __('Enter PayUMoney Account Salt', 'lifeline'),
		            ),
	            ),
            ),
            array(
	            'title' => __('QuickPay', 'lifeline'),
	            'desc' => __('<p class="description">This section contains QuickPay options.</p>', 'lifeline'),
	            'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
	            'id' => 'quickpay',
	            'fields' => array(
		            array(
			            'id' => 'enable_quickpay', //must be unique
			            'type' => 'button_set', //builtin fields include:
			            'title' => __('Enable QuickPay', 'lifeline'),
			            'desc' => __('Enable to show quickpay options.', 'lifeline')
		            ),
		            array(
		                'title' => __('You need to change your currency to Danish Krone', 'lifeline'),
                        ),
		            array(
			            'id' => 'quickpay_mode', //must be unique
			            'type' => 'select', //builtin fields include:
			            'title' => __('Type', 'lifeline'),
			            'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
		            ),
		            array(
			            'id' => 'quickpay_merchant_id', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay Merchant ID', 'lifeline'),
			            'desc' => __('Enter QuickPay Merchant ID', 'lifeline'),
		            ),
		            array(
			            'id' => 'quickpay_agreement', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay Agreement ID', 'lifeline'),
			            'desc' => __('Enter QuickPay Payment Window Agreement ID', 'lifeline'),
		            ),
		            array(
			            'id' => 'quickpay_api', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay API Key', 'lifeline'),
			            'desc' => __('Enter QuickPay Payment Window API Key', 'lifeline'),
		            ),
		            array(
			            'id' => 'quickpay_continueurl', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay Continue URL', 'lifeline'),
			            'desc' => __('Enter QuickPay Continue URL', 'lifeline'),
		            ),
		            array(
			            'id' => 'quickpay_cancelurl', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay Cancel URL', 'lifeline'),
			            'desc' => __('Enter QuickPay Cancel URL', 'lifeline'),
		            ),
		            array(
			            'id' => 'quickpay_callbackurl', //must be unique
			            'type' => 'text', //builtin fields include:
			            'title' => __('QuickPay Callback URL', 'lifeline'),
			            'desc' => __('Enter QuickPay Callback URL', 'lifeline'),
		            ),
	            ),
            ),
            array(
                'title' => __('Donation Transactions', 'lifeline'),
                'desc' => __('<p class="description">This section contains general dontation transactions record.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_donation_transactions',
                'fields' => array(
                    array(
                        'id' => 'transactions_detail', //must be unique
                        'type' => 'transactions', //builtin fields include:
                        'title' => __('Donations Trasactions', 'lifeline'),
                        'desc' => __('Review transactions history', 'lifeline'),
                        'options' => array('live' => 'Live', 'sandbox' => 'Sandbox')
                    ),
                ),
            ),
            array(
                'title' => __('Time & Price', 'lifeline'),
                'desc' => __('<p class="description">This section contains time & price change options.</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_023_cogwheels.png',
                'id' => 'sub_donation_time_price',
                'fields' => array(
                    array(
                        'id' => 'transactions_detail', //must be unique
                        'type' => 'multi_select', //builtin fields include:
                        'title' => __('Donations Trasactions', 'lifeline'),
                        'desc' => __('Select Which Value you want to show.', 'lifeline'),
                        'options' => array('One Time' => __('One Time', 'lifeline'), 'daily' => __('Daily', 'lifeline'), 'weekly' => __('Weekly', 'lifeline'), 'fortnightly' => __('Fortnightly', 'lifeline'), 'monthly' => __('Monthly', 'lifeline'), 'quarterly' => __('Quarterly', 'lifeline'), 'half_year' => __('Half Year', 'lifeline'), 'yearly' => __('Yearly', 'lifeline'))
                    ),
                    array(
                        'id' => 'pop_up_1st_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 1st Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_2nd_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 2nd Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_3rd_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 3rd Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_4th_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 4th Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_5th_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 5th Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_6th_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 6th Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                    array(
                        'id' => 'pop_up_7th_value', //must be unique
                        'type' => 'text', //builtin fields include:
                        'title' => __('Change 7th Value', 'lifeline'),
                        'desc' => __('Enter the paypal value, Enter Only Numbers', 'lifeline'),
                    ),
                ),
            ),
        )
    );
    $sections[] = array(
        'title' => __('Sidebar Options', 'lifeline'),
        'desc' => __('<p class="description">You can create as many sidebars as you required.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'sidebar_creator',
        'fields' => array(
            array(
                'id' => '', //must be unique
                'type' => 'heading', //builtin fields include:
                'heading' => __('Sidebar Position', 'lifeline')
            ),
            array(
                'id' => '', //must be unique
                'type' => 'heading', //builtin fields include:
                'heading' => __('Sidebar Creator', 'lifeline')
            ),
            array(
                'id' => 'dynamic_sidebars', //must be unique
                'type' => 'multi_text', //builtin fields include:
                'title' => __('Sidebar Name', 'lifeline'),
                'desc' => __('Enter the sidebar name', 'lifeline'),
            ),
        )
    );
    /** Page settings */
    $sections[] = array(
        'title' => __('Page Settings', 'lifeline'),
        'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'page_settings',
        'children' =>
        array(
                array(
                'title' => __('Blog Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'blog_page_section',
                'fields' => array(
                    array(
                    'id' => 'show_blog_title', //must be unique
                    'type' => 'button_set', //builtin fields include:
                    'title' => __('Hide Blog Title and Subtitle', 'lifeline'),
                    'desc' => __('Turn On or Off to hide blog title and subtitle ', 'lifeline'),
                       
                    ),
                    array(
                        'id' => 'blog_page_heading',
                        'type' => 'text',
                        'title' => __('Blog Page Title', 'lifeline'),
                        'std' => 'blog'
                    ),
                    array(
                        'id' => 'blog_page_sub_heading',
                        'type' => 'text',
                        'title' => __('Blog Page Subtitle', 'lifeline'),
                    ),
                    array(
                        'id' => 'show_blog_comment', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Hide Comment Form on Blog Detail Page', 'lifeline'),
                        'desc' => __('Turn On to hide comment form on blog detail page. ', 'lifeline'),
                    ),
                    array(
	                    'id' => 'show_blog_comment_tags', //must be unique
	                    'type' => 'button_set', //builtin fields include:
	                    'title' => __('Hide Comment tags section on Blog Detail Page', 'lifeline'),
	                    'desc' => __('Turn Off to hide comment form tag section on blog detail page. ', 'lifeline'),
                    ),

                )
            ),

                array(
                'title' => __('Shop Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'shop_page_section',
                'fields' => array(
                    array(
                    'id' => 'show_shop_title', //must be unique
                    'type' => 'button_set', //builtin fields include:
                    'title' => __('Hide Shop Title', 'lifeline'),
                    'desc' => __('Turn On or Off to hide shop title', 'lifeline'),
                       
                    ),
                    array(
                        'id' => 'shop_page_heading',
                        'type' => 'text',
                        'title' => __('Shop Page Title', 'lifeline'),
                        'std' => 'Shop'
                    ),

                ),
            ),
            array(
                'title' => __('404 Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => '404_page_section',
                'fields' => array(
                    array(
                        'id' => '404_page_image',
                        'type' => 'upload',
                        'title' => __('404 Page Image', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => '404_page_heading',
                        'type' => 'text',
                        'title' => __('404 Page Heading', 'lifeline'),
                        'std' => '404'
                    ),
                    array(
                        'id' => '404_page_sub_heading',
                        'type' => 'text',
                        'title' => __('404 Page Sub Heading', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => '404_page_main_title_colored',
                        'type' => 'textarea',
                        'title' => __('404 Page Colored Main Title', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => '404_page_sub_title',
                        'type' => 'textarea',
                        'title' => __('404 Page Sub Title', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => '404_page_contents_heading',
                        'type' => 'textarea',
                        'title' => __('404 Page Contents Heading', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => '404_page_content',
                        'type' => 'textarea',
                        'title' => __('404 Page Content', 'lifeline'),
                        'std' => ''
                    ),
                    array(
                        'id' => 'page_comments_status', //must be unique
                        'type' => 'button_set', //builtin fields include:
                        'title' => __('Page Comments Status', 'lifeline'),
                        'desc' => __('Set Page Comments Status', 'lifeline'),
                    ),
                )
            ),
            array(
                'title' => __('Search Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'search_page_section',
                'fields' => array(
                    array(
                        'id' => 'search_page_image',
                        'type' => 'upload',
                        'title' => __('Search 404 Page Image', 'lifeline'),
                    ),
                    array(
                        'id' => 'search_page_heading',
                        'type' => 'text',
                        'title' => __('Search Page Heading', 'lifeline'),
                        'std' => 'Search Results'
                    ),
                    array(
                        'id' => 'search_page_sidebar_pos',
                        'type' => 'radio',
                        'title' => __('Search Page Sidebar Position', 'lifeline'),
                        'std' => 'left',
                        'options' => array('right' => 'Right', 'left' => 'Left'),
                    ),
                    array(
                        'id' => 'search_page_sidebar',
                        'type' => 'select',
                        'title' => __('Search Page Sidebar', 'lifeline'),
                        'std' => 'Default Sidebar',
                        'options' => sh_get_sidebars(),
                    ),
                )
            ),
            array(
                'title' => __('Category Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'category_page_section',
                'fields' => array(
                    array(
                        'id' => 'category_page_image',
                        'type' => 'upload',
                        'title' => __('Category Page Image', 'lifeline'),
                    ),
                    array(
                        'id' => 'category_page_heading',
                        'type' => 'text',
                        'title' => __('Category Page Heading', 'lifeline'),
                    ),
                    array(
                        'id' => 'category_page_sidebar_pos',
                        'type' => 'radio',
                        'title' => __('Category Page Sidebar Position', 'lifeline'),
                        'std' => 'left',
                        'options' => array('right' => 'Right', 'left' => 'Left'),
                    ),
                    array(
                        'id' => 'category_page_sidebar',
                        'type' => 'select',
                        'title' => __('Category Page Sidebar', 'lifeline'),
                        'std' => 'Default Sidebar',
                        'options' => sh_get_sidebars(),
                    ),
                )
            ),
             array(
                'title' => __('Tag Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'tag_page_section',
                'fields' => array(
                    array(
                        'id' => 'tag_page_image',
                        'type' => 'upload',
                        'title' => __('Tag Page Image', 'lifeline'),
                    ),
                      array(
                        'id' => 'tag_page_heading',
                        'type' => 'text',
                        'title' => __('Tag Page Title', 'lifeline'),
                        'std' => 'Tag'
                    ),
                    array(
                        'id' => 'tag_page_sub_heading',
                        'type' => 'text',
                        'title' => __('Tag Page Subtitle', 'lifeline'),
                    ),
                    array(
                        'id' => 'tag_page_sidebar_pos',
                        'type' => 'radio',
                        'title' => __('Tag Page Sidebar Position', 'lifeline'),
                        'std' => 'left',
                        'options' => array('right' => 'Right', 'left' => 'Left'),
                    ),
                    array(
                        'id' => 'tag_page_sidebar',
                        'type' => 'select',
                        'title' => __('Tag Page Sidebar', 'lifeline'),
                        'std' => 'Default Sidebar',
                        'options' => sh_get_sidebars(),
                    ),
                )
            ),
            
             array(
                'title' => __('Author Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'author_page_section',
                'fields' => array(
                    array(
                        'id' => 'author_page_image',
                        'type' => 'upload',
                        'title' => __('Author Page Image', 'lifeline'),
                    ),
                      array(
                        'id' => 'author_page_heading',
                        'type' => 'text',
                        'title' => __('Author Page Title', 'lifeline'),
                        'std' => 'Author'
                    ),
                    array(
                        'id' => 'author_page_sub_heading',
                        'type' => 'text',
                        'title' => __('Author Page Subtitle', 'lifeline'),
                    ),
                    array(
                        'id' => 'author_page_sidebar_pos',
                        'type' => 'radio',
                        'title' => __('Author Page Sidebar Position', 'lifeline'),
                        'std' => 'left',
                        'options' => array('right' => 'Right', 'left' => 'Left'),
                    ),
                    array(
                        'id' => 'author_page_sidebar',
                        'type' => 'select',
                        'title' => __('Tag Page Sidebar', 'lifeline'),
                        'std' => 'Default Sidebar',
                        'options' => sh_get_sidebars(),
                    ),
                )
            ),
            array(
                'title' => __('Archive Page Settings', 'lifeline'),
                'desc' => __('<p class="description">Set the page settings</p>', 'lifeline'),
                'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
                'id' => 'archive_page_section',
                'fields' => array(
                    array(
                        'id' => 'archive_page_image',
                        'type' => 'upload',
                        'title' => __('Archive Page Image', 'lifeline'),
                    ),
                    array(
                        'id' => 'archive_page_sidebar_pos',
                        'type' => 'radio',
                        'title' => __('Archive Page Sidebar Position', 'lifeline'),
                        'std' => 'left',
                        'options' => array('right' => 'Right', 'left' => 'Left'),
                    ),
                    array(
                        'id' => 'archive_page_sidebar',
                        'type' => 'select',
                        'title' => __('Archive Page Sidebar', 'lifeline'),
                        'std' => 'Default Sidebar',
                        'options' => sh_get_sidebars(),
                    ),
                )
            ),
        )
    );
    /** Layout Settings */
    $sections[] = array(
        'title' => __('Layout Settings', 'lifeline'),
        'desc' => __('<p class="description">Set the Layout settings</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'layout_settings',
        'fields' => array(
            array(
                'type' => 'button_set', //builtin fields include:
                'id' => 'layout_responsive_options',
                'title' => __('Responsive Options', 'lifeline'),
                'desc' => __('Choose the Responsive Options', 'lifeline'),
            ),
            array(
                'id' => 'boxed_layout_status', //must be unique
                'type' => 'button_set', //builtin fields include:
                'title' => __('Use Boxed Layout', 'lifeline'),
                'desc' => __('Use Boxed Layout', 'lifeline'),
            ),
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'layout_responsive_width',
                'title' => __('Theme Layout Width', 'lifeline'),
                'options' => array('' => 'Default', '1040' => '1040px', '960' => '960px'),
                'desc' => __('Choose the width for Theme Layout', 'lifeline'),
                'attributes' => array('style' => 'width:40%'),
                'std' => '',
            ),
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'layout_sidebar_patron',
                'title' => __('Predefined Patterns', 'lifeline'),
                'options' => array('bg-body1' => 'Background 1', 'bg-body2' => 'Background 2', 'bg-body3' => 'Background 3', 'bg-body4' => 'Background 4'),
                'attributes' => array('style' => 'width:40%'),
                'std' => '',
            //'dependent'=> 'patrn_opt1' ,
            ),
            array(
                'id' => 'layout_patron_image', //must be unique
                'type' => 'upload', //builtin fields include:
                'title' => __('Patterns Image', 'lifeline'),
            //'dependent'=> 'patrn_opt2' ,
            ),
        )
    );

    $sections[] = array(
        'title' => __('Home Page Settings', 'lifeline'),
        'desc' => __('<p class="description">Set the Home settings</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'homepage_settings',
        'fields' => array(
            array(
                'id' => 'top_image', //must be unique
                'type' => 'upload', //builtin fields include:
                'title' => __('Upload Top Image', 'lifeline'),
                'desc' => __('Use Boxed Layout', 'lifeline'),
            ),
             
        )
    );

    $sections[] = array(
        'title' => __('Join Our Team', 'lifeline'),
        'desc' => __('<p class="description">Set the Home settings</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'team_section',
        'fields' => array(
            array(
                'id' => 'team_title', //must be unique
                'type' => 'text', //builtin fields include:
                'title' => __('Title', 'lifeline'),
                'desc' => __('Enter Title for Join Our Team Section.', 'lifeline'),
            ),
            array(
                'id' => 'team_text', //must be unique
                'type' => 'textarea', //builtin fields include:
                'title' => __('Text', 'lifeline'),
                'desc' => __('Enter Text for Join Our Team Section.', 'lifeline'),
            ),
            array(
                'id' => 'team_link', //must be unique
                'type' => 'text', //builtin fields include:
                'title' => __('Link', 'lifeline'),
                'desc' => __('Enter link for Join Our Team Section.', 'lifeline'),
            ),
        )
    );
    $sections[] = array(
        'title' => __('Quotes Section', 'lifeline'),
        'desc' => __('<p class="description">You can create as many sidebars as you required.</p>', 'lifeline'),
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_062_attach.png',
        'id' => 'qoutation_section',
        'fields' => array(
            array(
                'id' => '', //must be unique
                'type' => 'heading', //builtin fields include:
                'heading' => __('Add Quotes Here', 'lifeline')
            ),
            array(
                'id' => 'qoutation_text', //must be unique
                'type' => 'multi_text', //builtin fields include:
                'title' => __('Quote', 'lifeline'),
                'desc' => __('Enter the Quote', 'lifeline'),
            ),
        )
    );
   
    apply_filters('sh-opts-sections-theme', $sections);

    $tabs = array();

    if (function_exists('wp_get_theme')) {
        $theme_data = wp_get_theme();
        $theme_uri = $theme_data->get('ThemeURI');
        $description = $theme_data->get('Description');
        $author = $theme_data->get('Author');
        $version = $theme_data->get('Version');
        $tags = $theme_data->get('Tags');
    } else {
        $theme_data = wp_get_theme(trailingslashit(get_stylesheet_directory()) . 'style.css');
        $theme_uri = $theme_data['URI'];
        $description = $theme_data['Description'];
        $author = $theme_data['Author'];
        $version = $theme_data['Version'];
        $tags = $theme_data['Tags'];
    }
    $theme_info = '<div class="nhp-opts-section-desc">';
    $theme_info .= '<p class="nhp-opts-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'lifeline') . '<a href="' . $theme_uri . '" target="_blank">' . $theme_uri . '</a></p>';
    $theme_info .= '<p class="nhp-opts-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'lifeline') . $author . '</p>';
    $theme_info .= '<p class="nhp-opts-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'lifeline') . $version . '</p>';
    $theme_info .= '<p class="nhp-opts-theme-data description theme-description">' . $description . '</p>';
    $theme_info .= '<p class="nhp-opts-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'lifeline') . implode(', ', $tags) . '</p>';
    $theme_info .= '</div>';
    $tabs['theme_info'] = array(
        'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_195_circle_info.png',
        'title' => __('Theme Information', 'lifeline'),
        'content' => $theme_info
    );

    if (file_exists(trailingslashit(get_stylesheet_directory()) . 'README.html')) {
        $tabs['theme_docs'] = array(
            'icon' => get_template_directory() . '/framework/theme_options/img/glyphicons/glyphicons_071_book.png',
            'title' => __('Documentation', 'lifeline'),
            'content' => nl2br(sh_set(wp_remote_get(trailingslashit(get_stylesheet_directory_uri()) . '/README.html'), 'body'))
        );
    }//if
    global $NHP_Options;
    $NHP_Options = new SH_Options($sections, $args, $tabs);
}

//function
add_action('init', 'setup_framework_options', 0);
/*
 *
 * Custom function for the callback referenced above
 *
 */

function my_custom_field($field, $value) {
    print_r($field);
    print_r($value);
}

//function
/*
 *
 * Custom function for the callback validation referenced above
 *
 */

function validate_callback_function($field, $value, $existing_value) {

    $error = false;
    $value = 'just testing';
    $return['value'] = $value;
    if ($error == true) {
        $return['error'] = $field;
    }
    return $return;
}

//function
?>
