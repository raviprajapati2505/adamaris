<?php

$options = array();

global $current_user;
$user_roles = $current_user->roles;
$user_role = array_shift( $user_roles );

if ( $user_role && $user_role == 'causes_author' ) {
	$options['dict_causes'] = array(
		'labels' => array( __( 'Cause', 'lifeline' ), __( 'Cause', 'lifeline' ) ),
		'slug' => 'cause',
		'label_args' => array( 'menu_name' => __( 'Cause', 'lifeline' ) ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'author' ),
		'label' => __( 'Cause', 'lifeline' ),
		'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Causes.png', 'map_meta_cap' => true, 'capabilities' => array(
				'edit_post' => 'edit_dict_causes',
				'edit_posts' => 'edit_dict_causess',
				'edit_others_posts' => 'edit_other_dict_causess',
				'publish_posts' => 'publish_dict_causess',
				'edit_publish_posts' => 'edit_publish_dict_causess',
				'read_post' => 'read_lessons',
				'read_private_posts' => 'read_private_dict_causess',
				'delete_post' => 'delete_dict_causes',
			),
			'capability_type' => array( 'dict_causess', 'dict_causesss' ),
		),
	);
} else {
	$options['dict_causes'] = array(
		'labels' => array( __( 'Cause', 'lifeline' ), __( 'Cause', 'lifeline' ) ),
		'slug' => 'cause',
		'label_args' => array( 'menu_name' => __( 'Cause', 'lifeline' ) ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'label' => __( 'Cause', 'lifeline' ),
		'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Causes.png' ),
	);
}
$options['dict_testimonials'] = array(
	'labels' => array( __( 'Testimonial', 'lifeline' ), __( 'Testimonial', 'lifeline' ) ),
	'slug' => 'testimonail',
	'label_args' => array( 'menu_name' => __( 'Testimonial', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'label' => __( 'Testimonial', 'lifeline' ),
	'args' => array('public'=>false, 'menu_icon' => get_template_directory_uri() . '/images/COmments.png', 'publicly_queryable' => false, 'exclude_from_search' => true,  )
);
$options['dict_project'] = array(
	'labels' => array( __( 'Project', 'lifeline' ), __( 'Projects', 'lifeline' ) ),
	'slug' => 'project',
	'label_args' => array( 'menu_name' => __( 'Projects', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'label' => __( 'Projects', 'lifeline' ),
	'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Projects.png' )
);
$options['dict_event'] = array(
	'labels' => array( __( 'Event', 'lifeline' ), __( 'Events', 'lifeline' ) ),
	'slug' => 'event',
	'label_args' => array( 'menu_name' => __( 'Events', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail','comments' ),
	'label' => __( 'Events', 'lifeline' ),
	'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Events.png' )
);
$options['dict_portfolio'] = array(
	'labels' => array( __( 'Portfolio', 'lifeline' ), __( 'Portfolios', 'lifeline' ) ),
	'slug' => 'portfolio',
	'label_args' => array( 'menu_name' => __( 'Portfolios', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'label' => __( 'Portfolios', 'lifeline' ),
	'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/POrtfolio.png' )
);
$options['dict_gallery'] = array(
	'labels' => array( __( 'Gallery Image', 'lifeline' ), __( 'Galleries', 'lifeline' ) ),
	'slug' => 'galleries',
	'label_args' => array( 'menu_name' => __( 'Galleries', 'lifeline' ) ),
	'supports' => array( 'title', 'thumbnail' ),
	'label' => __( 'Galleries', 'lifeline' ),
	'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Galleries.png' )
);
$options['dict_team'] = array(
	'labels' => array( __( 'Team', 'lifeline' ), __( 'Teams', 'lifeline' ) ),
	'slug' => 'team',
	'label_args' => array( 'menu_name' => __( 'Teams', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'label' => __( 'Teams', 'lifeline' ),
	'args' => array('menu_icon' => get_template_directory_uri() . '/images/team.png')
);
$options['dict_services'] = array(
	'labels' => array( __( 'Service', 'lifeline' ), __( 'Service', 'lifeline' ) ),
	'slug' => 'dict_services',
	'label_args' => array( 'menu_name' => __( 'Services', 'lifeline' ) ),
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'label' => __( 'Service', 'lifeline' ),
	'args' => array( 'menu_icon' => get_template_directory_uri() . '/images/Services.png' )
);
