<?php
$maps[] = array(
	"name" => __("Our Causes With Donation", 'lifeline'),
	"base" => "sh_our_causes",
	"class" => "",
	"icon" => 'our-casuses-with-donation',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Grid Columns", 'lifeline'),
			"param_name" => "columns_grid",
			"value" => array(__('3 Colums', 'lifeline') => '4', __('4 Columns', 'lifeline') => '3'),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('post_type' => 'dict_causes', 'taxonomy' => 'causes_category'), true)),
			"description" => __("Choose any one category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Donation Section", 'lifeline'),
			"param_name" => "donate_sec",
			"value" => array_flip(array('true' => 'True', 'fasle' => 'False')),
			"description" => __("Choose either you want to Display Donation Section in this Area or Not.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title", 'lifeline'),
			"param_name" => "don_sect_title",
			"value" => '',
			"description" => __("Donation Section Title", 'lifeline'),
			'dependency' => array(
				'element' => 'donate_sec',
				'value' => array('true')
			),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Needed Label", 'lifeline'),
			"param_name" => "needed_label",
			"value" => '',
			"description" => __("Donation Needed Label", 'lifeline'),
			'dependency' => array(
				'element' => 'donate_sec',
				'value' => array('true')
			),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Collected Label", 'lifeline'),
			"param_name" => "collected_label",
			"value" => '',
			"description" => __("Donation Collected Label", 'lifeline'),
			'dependency' => array(
				'element' => 'donate_sec',
				'value' => array('true')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array("name" => __("Causes Parallax Slider", 'lifeline'),
	"base" => "sh_our_causes_2",
	"class" => "",
	"icon" => 'our-casuses-with-donation',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Background Layer", 'lifeline'),
			"param_name" => "bg_layer",
			"value" => array_flip(array('true' => __('True', 'lifeline'), 'false' => __('False', 'lifeline'))),
			"description" => __("Enable Background Layer.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category'), true)),
			"description" => __("Choose any one category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array(
	"name" => __("Causes Tabber Slider", 'lifeline'),
	"base" => "sh_our_causes_3",
	"icon" => 'our-casuses-with-donation',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category'), true)),
			"description" => __("Choose any one category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array(
	"name" => __("Causes Portfolio", 'lifeline'),
	"base" => "sh_our_causes_4",
	"icon" => 'our-casuses-with-donation',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category'), true)),
			"description" => __("Choose any one Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array(
	"name" => __("Donation", 'lifeline'),
	"base" => "sh_donation",
	"icon" => 'sh_donation',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Margins", 'lifeline'),
			"param_name" => "margins",
			"value" => array_flip(array('top' => 'Top Margine', 'bottom' => 'Bottom Margine')),
			"description" => __("Choose either you want to Display Donation Section in this Area or Not.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter Title for this Section.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Heading Style", 'lifeline'),
			"param_name" => "heading_style",
			"value" => array_flip(array('simple' => 'Simple', 'underline' => 'Underlined Heading', 'modern' => 'Modern Heading')),
			"description" => __("Choose the Heading style You want to choose.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title", 'lifeline'),
			"param_name" => "don_sect_title",
			"value" => '',
			"description" => __("Donation Section Title", 'lifeline'),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Needed Label", 'lifeline'),
			"param_name" => "needed_label",
			"value" => '',
			"description" => __("Donation Needed Label", 'lifeline'),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Collected Label", 'lifeline'),
			"param_name" => "collected_label",
			"value" => '',
			"description" => __("Donation Collected Label", 'lifeline'),
		),
	)
);
$maps[] = array(
	"name" => __("Donation Sidebox", 'lifeline'),
	"base" => "sh_donation_2",
	"icon" => 'sh_donation',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
    //"show_settings_on_create" => false,
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Needed Label", 'lifeline'),
			"param_name" => "needed_label",
			"value" => '',
			"description" => __("Donation Needed Label", 'lifeline'),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Collected Label", 'lifeline'),
			"param_name" => "collected_label",
			"value" => '',
			"description" => __("Donation Collected Label", 'lifeline'),
		),
	),
);
$maps[] = array(
	"name" => __("Donation Wide Box", 'lifeline'),
	"icon" => 'sh_donation',
	"base" => "sh_donation_3",
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"show_settings_on_create" => false,
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Label", 'lifeline'),
			"param_name" => "btn_label",
			"value" => '',
			"description" => __("Enter button label to show.", 'lifeline')
		),


	),
);
$maps[] = array(
	"name" => __("Start Regular Donation", 'lifeline'),
	"base" => "sh_start_regular_donation",
	"icon" => 'regular-donation',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter main title.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Sub Title", 'lifeline'),
			"param_name" => "sub_title",
			"value" => '',
			"description" => __("Enter sub title.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Currency", 'lifeline'),
			"param_name" => "currency",
			"value" => '',
			"description" => __("Enter donation currency symbol.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Donation Needed", 'lifeline'),
			"param_name" => "donation_needed",
			"value" => '',
			"description" => __("Enter needed donation amount.", 'lifeline')
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Image", 'lifeline'),
			"param_name" => "image",
			"value" => '',
			"description" => __("Upload Image.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Caption", 'lifeline'),
			"param_name" => "link_caption",
			"value" => '',
			"description" => __("Enter Button Caption.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button URL", 'lifeline'),
			"param_name" => "button_cap_url",
			"value" => '',
			"description" => __("Enter url for button.", 'lifeline')
		),
		array(
			"type" => "textarea", "class" => "",
			"heading" => __("Text", 'lifeline'),
			"param_name" => "text",
			"value" => '',
			"description" => __("Enter the text.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Testimonials", 'lifeline'),
	"base" => "sh_ceo_message",
	"icon" => 'sh_testimonials',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number Of Messages", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Number Of Messages", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Overlap", 'lifeline'),
			"param_name" => "overlap",
			"value" => array(__('True', 'lifeline') => 'true', __('False', 'lifeline') => 'false'),
			"description" => __("Make this section overlap.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'testimonial_category', 'hide_empty' => false), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'rand' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('No Sorting Order', 'lifeline') => '', __('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Recent News", 'lifeline'),
	"icon" => 'sh_recent_news',
	"base" => "sh_recent_news",
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('post_type' => 'post', 'taxonomy' => 'category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title Character Limit:", 'lifeline'),
			"param_name" => "title_charcacter_limit",
			"value" => "",
			"description" => __("Enter the number of showing post title characters.", 'lifeline'),
			
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array(
	"name" => __("Recent Events", 'lifeline'),
	"base" => "sh_recent_events",
	"class" => "",
	"icon" => 'sh_recent-event',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title Limit", 'lifeline'),
			"param_name" => "t_limit",
			"value" => '',
			"description" => __("Enter limit for title", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'event_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => 'Date', 'title' => 'Title', 'name' => 'Name', 'author' => 'Author', 'comment_count' => 'Comment Count', 'random' => 'Random')),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array('Ascending Order' => 'ASC', 'Descending Order' => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Upcoming Events Slider", 'lifeline'),
	"base" => "sh_recent_events_2",
	"icon" => 'upcoming-events',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'event_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array("name" => __("Upcoming Event", 'lifeline'),
	"base" => "sh_recent_events_3",
	"class" => "",
	"icon" => 'upcoming-events',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'event_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Successful Stories", 'lifeline'),
	"base" => "sh_successful_stories",
	"icon" => 'successful-stories',
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Content Options:", 'lifeline'),
			"param_name" => "c_opt",
			"value" => array(
				__('Content Excerpt', 'lifeline') => 'excerpt',
				__('Full Content', 'lifeline') => 'full',
				__('Custom Limit', 'lifeline') => 'limit',
			),
			"description" => __("Select option of showing post content.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Character Limit:", 'lifeline'),
			"param_name" => "c_limit",
			"value" => "",
			"description" => __("Enter the number of showing post characters.", 'lifeline'),
			'dependency' => array(
				'element' => 'c_opt',
				'value' => array('limit')
			),
		),
	)
);
$maps[] = array(
	"name" => __("Welfare Projects 2 Column", 'lifeline'),
	"base" => "sh_welfare_projects",
	"class" => "",
	"icon" => 'welfare-project',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Donate Now", 'lifeline'),
			"param_name" => "donate_now",
			"value" => array_flip(array('true' => 'Donate Now')),
			"description" => __("Enable this to show donate now button on hover.", 'lifeline')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Needed Amount", 'lifeline'),
			"param_name" => "needed_amount",
			"value" => array_flip(array('true' => 'Enable Nedded Amount')),
			"description" => __("Enable this to show needed amount in this section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Needed Text", 'lifeline'),
			"param_name" => "needed_text",
			"value" => '',
			"description" => __("Enter needed text to show", 'lifeline'),
			'dependency' => array(
				'element' => 'needed_amount',
				'value' => array('true')
			),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title Limit", 'lifeline'),
			"param_name" => "title_limit",
			"value" => '',
			"description" => __("Enter the number of characters to show in title", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Description Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '',
			"description" => __("Enter the number of characters to show in causes description", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Welfare Projects 3 Columns", 'lifeline'),
	"base" => "sh_welfare_projects_2",
	"class" => "",
	"icon" => 'welfare-project',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Text Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '50',
			"description" => __("Enter the text limit of this section in integer", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Leave A Message", 'lifeline'),
	"base" => "sh_leave_message",
	"class" => "",
	"icon" => 'leave-a-message',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textarea", "class" => "",
			"heading" => __("Text", 'lifeline'),
			"param_name" => "text",
			"value" => '',
			"description" => __("Enter text.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Block Quotes", 'lifeline'),
	"base" => "sh_block_quotes",
	"class" => "",
	"icon" => 'sh_block-quotes',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Block Quotes", 'lifeline'),
			"param_name" => "blockquotes",
			"value" => '',
			"description" => __("Enter Block Quotes.", 'lifeline')
		),
	)
);
$maps[] = array("name" => __("Boxed Block Quotes", 'lifeline'),
	"base" => "sh_boxed_block_quotes",
	"class" => "",
	"icon" => 'sh_block-quotes',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Text Before Block Quotes", 'lifeline'),
			"param_name" => "text1",
			"value" => '',
			"description" => __("Enter Text Before Block Quotes", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Text After Block Quotes", 'lifeline'),
			"param_name" => "text2",
			"value" => '',
			"description" => __("Enter Text After Block Quotes", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Block Quotes", 'lifeline'),
			"param_name" => "blockquotes",
			"value" => '',
			"description" => __("Enter Block Quotes.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Button", 'lifeline'),
	"base" => "sh_button",
	"class" => "",
	"icon" => 'sh_vc_button',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Text", 'lifeline'),
			"param_name" => "text",
			"value" => '',
			"description" => __("Enter button text", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Link", 'lifeline'),
			"param_name" => "Link",
			"value" => '',
			"description" => __("Enter Button Link", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Size", 'lifeline'), "param_name" => "size",
			"value" => array('small' => 'Small', 'medium' => 'Medium', 'large' => 'Large'),
			"description" => __("Choose button size.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Color", 'lifeline'),
			"param_name" => "color",
			"value" => array('skyblue' => 'Sky Blue', 'green' => 'Green', 'dodgerblue' => 'Dodger Blue', 'blue' => 'Blue', 'limegreen' => 'Lime Green', 'silver' => 'Silver'),
			"description" => __("Choose button color.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("List Item", 'lifeline'),
	"base" => "sh_list_item",
	"class" => "",
	"icon" => 'faqs',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Text", 'lifeline'),
			"param_name" => "text",
			"value" => '',
			"description" => __("Enter list contents", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("List Style", 'lifeline'),
			"param_name" => "style",
			"value" => array('icon-check' => 'Check',
				'icon-plus' => 'Plus',
				'icon-minus' => 'Minus',
				'icon-star' => 'Star',
				'icon-angle-right' => 'Angle Right',
				'icon-caret-right' => 'Caret Right',
				'icon-sign-blank' => 'Square',
				'icon-circle' => 'Circle',
				'icon-remove' => 'Cross',
				'icon-random' => 'Random',
				'icon-thumbs-up-alt' => 'Thumbs Up',
				'icon-thumbs-down-alt' => 'Thumbs Down',
				'icon-undo' => 'Undo',
				'icon-sort' => 'Sort',
				'icon-angle-right' => 'Double Angle Right', 'icon-angle-left' => 'Double Angle Left',
				'icon-quote-left' => 'Quotes',
				'icon-spinner' => 'Spinner',
			),
			"description" => __("Choose list style.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("List Item Style 2", 'lifeline'),
	"base" => "sh_list_item",
	"class" => "",
	"icon" => 'sh_list-item',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter title", 'lifeline')
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Message", 'lifeline'),
			"param_name" => "message",
			"value" => '',
			"description" => __("Enter message.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Type", 'lifeline'),
			"param_name" => "type",
			"value" => array_flip(array('warning' => 'Warning', 'cancel' => 'Cancel', 'attention' => 'Attention', 'done' => 'Done')),
			"description" => __("Choose Alert Mesage Type.", 'lifeline')
		),
	)
);
$maps[] = array("name" => __("Social Media Icon", 'lifeline'),
	"base" => "sh_social_media_icon",
	"class" => "",
	"icon" => 'sh_social-media',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Social Media", 'lifeline'),
			"param_name" => "media",
			"value" => array_flip(array('social_facebook' => 'facebook',
				'social_flickr' => 'flickr',
				'social_forst' => 'forst',
				'social_google-plus' => 'google-plus',
				'social_blogger' => 'blogger',
				'social_lastfm' => 'lastfm',
				'social_linkedin' => 'linkedin',
				'social_wordpress' => 'wordpress',
				'social_twitter' => 'twitter',
				'social_tumbler' => 'tumbler',
				'social_digg' => 'digg',
				'social_dribble' => 'dribble',
				'social_behance' => 'behance',
				'social_addthis' => 'addthis',
				'social_sharethis' => 'sharethis',
				'social_rss' => 'rss',
				'social_skype' => 'skype',
				'social_deliciou' => 'deliciou',
				'social_stumble' => 'stumble',
				'social_vimeo' => 'vimeo',
				'social_virb' => 'virb',
				'social_mail' => 'mail',
				'social_grooveshark' => 'grooveshark',
				'social_infinte' => 'infinte',
				'social_instagram' => 'instagram',
				'social_evernote' => 'evernote',
				'social_path' => 'path',
				'social_myspace' => 'myspace',
				'social-gray_play' => 'Gray Play',
				'social-gray_google-plus' => 'Gray Google Plus',
				'social-gray_facebook' => 'Gray Facebook',
				'social-gray_tumbler' => 'Gray Tumbler',
				'social-gray_twitter' => 'Gray Twitter',
				'social-gray_sharethis' => 'Gray Sharethis',
				'social-gray_msn' => 'Gary MSN',
				'social-gray_flickr' => 'Gray Flicker',
				'social-gray_linkedin' => 'Gray Linkedin',
				'social-gray_vimeo' => 'Gary Vimeo',
				'social-gray_gtalk' => 'Gray Gtalk',
				'social-gray_skype' => 'Gray Skype',
				'social-gray_found' => 'Gray Found',
				'social-gray_rss' => 'Gray RSS',
				'social-gray_buzz' => 'Gray Buzz',
				'social-gray_yahoomessanger' => 'Gray Yahoo Messanger',
				'social-gray_yahoo' => 'Gray Yahoo',
				'social-gray_digg' => 'Gray Digg',
				'social-gray_deleciou' => 'Gray Dilicious',
				'social-gray_upcoming' => 'Gray Upcoming',
				'social-gray_aim' => 'Gray Aim',
				'social-gray_myspace' => 'Gray MySpace',
				'social-gray_wikipedia' => 'Gray Wikipedia',
				'social-gray_vcard' => 'Gray Vcard',
				'social-gray_picasa' => 'Gray Picasa',
				'social-gray_dribble' => 'Gray Dribble',
				'social-gray_netvibes' => 'Gray Netvibes',
				'social-gray_deviantart' => 'Gray Deviantart',
				'social-gray_fireeagle' => 'Gray Fireeagle',
				'social-gray_itunes' => 'Gray iTunes',
				'social-gray_lastfm' => 'Gray Lastfm',
				'social-gray_amazon' => 'Gray Amazon',
				'social-gray_reddit' => 'Gray Reddit',
				'social-gray_stumble' => 'Gray Stumble',
				'social-gray_digg2' => 'Gray Digg 2',
				'social-gray_orkut' => 'Gray Orkut')),
			"description" => __("Choose Alert Mesage Type.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Progressbar", 'lifeline'),
	"base" => "sh_progressbar",
	"class" => "",
	"icon" => 'sh_list-item',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter title", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Percentage Value", 'lifeline'),
			"param_name" => "percentage",
			"value" => '',
			"description" => __("Enter percentage value e.g. 10, 40, 75", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Position", 'lifeline'),
			"param_name" => "position",
			"value" => array_flip(array('top' => 'Top', 'within-progress-bar' => 'Within Progressbar')),
			"description" => __("Choose Percentage Value Position.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Price Table", 'lifeline'),
	"base" => "sh_price_table",
	"class" => "",
	"icon" => 'sh_price-table',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter title", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Sub Title", 'lifeline'),
			"param_name" => "sub_title",
			"value" => '',
			"description" => __("Enter Sub Title", 'lifeline')
		),
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Price", 'lifeline'),
			"param_name" => "price",
			"value" => '',
			"description" => __("Enter Price", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Currency Symbol", 'lifeline'),
			"param_name" => "currency",
			"value" => '',
			"description" => __("Enter Currency Symbol", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Option One Contents", 'lifeline'),
			"param_name" => "option1",
			"value" => '',
			"description" => __("Enter Option One Contents", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Option Two Contents", 'lifeline'),
			"param_name" => "option3",
			"value" => '',
			"description" => __("Enter Option Two Contents", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Option Three Contents", 'lifeline'),
			"param_name" => "option3",
			"value" => '',
			"description" => __("Enter Option Three Contents", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Redirect Link", 'lifeline'),
			"param_name" => "link",
			"value" => '',
			"description" => __("Enter redirect link", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Charity Video", 'lifeline'),
	"base" => "sh_charity_video",
	"class" => "",
	"icon" => 'sh_charity-video',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter main title of the section", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video Title", 'lifeline'),
			"param_name" => "video_title",
			"value" => '',
			"description" => __("Enter title for video", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Video Type", 'lifeline'),
			"param_name" => "video_type",
			"value" => array(
				'Vimeo Video' => 'vimeo', 
				'Youtube Video' => 'youtube', 
			),
			'std'         => 'vimeo',
			"description" => __("Select video type.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video ID", 'lifeline'),
			"param_name" => "video_link",
			"value" => '',
			"description" => __("Enter the Vimeo video id", 'lifeline'),
			'dependency' => array(
				'element' => 'video_type',
				'value' => array('vimeo')
			),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video URL (If you want to show Youtube video)", 'lifeline'),
			"param_name" => "video_link2",
			"value" => '',
			"description" => __("Enter the url of the youtube video", 'lifeline'),
			'dependency' => array(
				'element' => 'video_type',
				'value' => array('youtube')
			),
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Video Description", 'lifeline'),
			"param_name" => "description",
			"value" => '',
			"description" => __("Enter description.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Lifeline Video", 'lifeline'),
	"base" => "sh_lifeline_video",
	"class" => "",
	"icon" => 'sh_charity-video',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter the title to show on video thumb", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video Link", 'lifeline'),
			"param_name" => "video_link",
			"value" => '',
			"description" => __("Enter the URL for the video you want to play", 'lifeline')
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Video Thumb", 'lifeline'),
			"param_name" => "video_thumb",
			"value" => '',
			"description" => __("Insert the video thumb you want to show in this section", 'lifeline')
		),
	)
);
$maps[] = array("name" => __("Charity Video 2", 'lifeline'),
	"base" => "sh_charity_video2",
	"class" => "",
	"icon" => 'sh_charity-video',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => '',
			"description" => __("Enter main title of the section", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video Link", 'lifeline'),
			"param_name" => "video_link",
			"value" => '',
			"description" => __("Enter video link to add in this section. Note: This field will support youtube,dailymotion and vimeo videos", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Duration", 'lifeline'),
			"param_name" => "duration",
			"value" => '',
			"description" => __("Enter Duration", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Projects", 'lifeline'),
			"param_name" => "projects",
			"value" => '',
			"description" => __("Enter Projects", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Members", 'lifeline'),
			"param_name" => "members",
			"value" => '',
			"description" => __("Enter Members", 'lifeline')
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Video Description", 'lifeline'),
			"param_name" => "description",
			"value" => '',
			"description" => __("Enter description.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Team", 'lifeline'),
	"base" => "sh_team",
	"class" => "",
	"icon" => 'sh_vc_team',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "", "heading" => __("Number Of Team Members", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Number Of Team Members", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array('col-md-12' => '1 Column', 'col-md-6' => '2 Columns', 'col-md-4' => '3 Columns', 'col-md-3' => '4 Columns', 'col-md-2' => '6 Columns', 'col-md-1' => '12 Columns')),
			"description" => __("Choose Number of Columns for Team Members.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'team_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'rand' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Shop Online", 'lifeline'),
	"base" => "sh_shop_online",
	"class" => "",
	"icon" => 'sh_shop-online',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of Products", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);

$maps[] = array(
	"name" => __("Services", 'lifeline'),
	"base" => "sh_services",
	"class" => "",
	"icon" => 'sh_vc_services',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of Services", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "col",
			"value" => array(__('2 Columns', 'lifeline') => 6, __('3 Columns', 'lifeline') => 4, __('4 Columns', 'lifeline') => 3,__('6 Columns', 'lifeline') => 2 ),
			"description" => __("Choose number of columns", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by",
			"value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Linked to Deatiled Page ?", 'lifeline'),
			"param_name" => "linked",
			"value" => array('linked' => 'Linked'),
			"description" => __("Click to link services to the Detail page.", 'lifeline')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Overlap Services?", 'lifeline'),
			"param_name" => "overlap",
			"value" => array('overlap' => 'Overlap'),
			"description" => __("Click to Make this Section Overlap the Upper Section.", 'lifeline')
		)
	)
);
$maps[] = array(
	"name" => __("Gallery", 'lifeline'),
	"base" => "sh_Gallery", "class" => "",
	"icon" => 'sh_gallery',
	"category" => __('Lifeline', 'lifeline'), "params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "num",
			"description" => __("Enter Number of Galleries.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array(1 => '1 Column', 2 => '2 Columns', 3 => '3 Columns', 4 => '4 Columns')),
			"description" => __("Choose Number of Columns for Galleries.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Portfolio Wide", 'lifeline'),
	"base" => "sh_portfolio_without_sidebar", "class" => "",
	"icon" => 'sh_portfolio',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "num",
			"description" => __("Enter Number of Portfolios.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array(2 => '2 Columns', 3 => '3 Columns', 4 => '4 Column',)),
			"description" => __("Choose Number of Columns for Galleries.", 'lifeline')
		),
		array("type" => "checkbox",
			"class" => "",
			"heading" => __("Show Toggle", 'lifeline'),
			"param_name" => "show_toggle",
			"value" => '',
			"description" => __("Hide/Show Toggle", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Portfolio With Sidebar", 'lifeline'),
	"base" => "sh_portfolio_with_sidebar", "class" => "",
	"icon" => 'sh_portfolio',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "num",
			"description" => __("Enter Number of Portfolios.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "", "heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array(2 => '2 Columns', 3 => '3 Columns')),
			"description" => __("Choose Number of Columns for Galleries.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Video Gallery", 'lifeline'),
	"base" => "sh_video_gallery", "class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"icon" => 'sh_gallery',
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "", "heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array(6 => '2 Columns', 4 => '3 Columns')),
			"description" => __("Choose Number of Columns for Galleries.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Play Options", 'lifeline'), "param_name" => "play_options",
			"value" => array_flip(array('lightbox' => 'Play in Light Box', 'simple' => 'Simple Player')),
			"description" => __("Choose either video should play in Light Box or not .", 'lifeline')
		), array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Video Links", 'lifeline'),
			"param_name" => "links",
			"description" => __("Enter Comma Seperated Video Links (dailymotion, Vimeo, Youtube).", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Quotes Slider", 'lifeline'),
	"base" => "sh_qoutes_slider", "class" => "",
	"icon" => 'welfare-project',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Causes(Issues We Are Working On)", 'lifeline'),
	"base" => "sh_issues_we_work",
	"class" => "",
	"icon" => SH_URL . '/images/vc/Causes.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Category", 'lifeline'),
		"param_name" => "category",
		"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE), true)),
		"description" => __('Choose Category. ', 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Project Slider", 'lifeline'),
	"base" => "sh_projects_slider",
	"class" => "",
	"icon" => SH_URL . '/images/vc/project.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title", 'lifeline'),
			"param_name" => "section_title",
			"value" => '',
			"description" => __("Enter Title fot this Section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE), true)),
			"description" => __('Choose Category. ', 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Latest News Slider", 'lifeline'),
	"base" => "sh_latest_news_slider",
	"class" => "",
	"icon" => SH_URL . '/images/vc/news_new.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title", 'lifeline'),
			"param_name" => "section_title",
			"value" => '',
			"description" => __("Enter Title fot this Section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'category', 'hide_empty' => FALSE), true)),
			"description" => __('Choose Category. ', 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Welcome Box", 'lifeline'),
	"base" => "sh_welcome_box",
	"class" => "",
	"icon" => SH_URL . '/images/vc/welcome.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Heading Text", 'lifeline'),
			"param_name" => "h_txt",
			"value" => "",
			"description" => __("Enter the heading text of this section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Description", 'lifeline'),
			"param_name" => "h_desc",
			"value" => "",
			"description" => __("Enter the description of this section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'lifeline'),
			"param_name" => "button_txt",
			"value" => "",
			"description" => __("Enter the text of the button.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Url", 'lifeline'),
			"param_name" => "button_url",
			"value" => "",
			"description" => __("Enter the url of the button.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Full width Video", 'lifeline'),
	"base" => "sh_parallax_video",
	"class" => "",
	"icon" => SH_URL . '/images/vc/full-video.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Video ID", 'lifeline'),
			"param_name" => "video_id",
			"value" => "",
			"description" => __("Enter the vimeo video id like '10259948' ", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Causes with carousel", 'lifeline'),
	"base" => "sh_causes_with_carousel",
	"class" => "",
	"icon" => SH_URL . '/images/vc/qote-slider.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title", 'lifeline'),
			"param_name" => "section_title",
			"value" => '',
			"description" => __("Enter Title for this Section.", 'lifeline')
		),
		array("type" => "dropdown",
			"class" => "",
			"heading" => __("Use As", 'lifeline'),
			"param_name" => "use_as",
			"value" => array(__('Heading', 'lifeline') => 'Heading', __('slider', 'lifeline') => 'Slider'),
			"description" => __("Select the option of this section.", 'lifeline')
		),
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Section Description", 'lifeline'),
			"param_name" => "section_desc",
			"value" => '',
			"description" => __("Enter Description fot this Section.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE), true)),
			"description" => __('Choose Category. ', 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Services With Pictorial Icons", 'lifeline'),
	"base" => "sh_services_with_pictures",
	"class" => "",
	"icon" => SH_URL . '/images/vc/services_new.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Layout", 'lifeline'), "param_name" => "layout",
		"value" => array_flip(array('title_only' => __('Title Only', 'lifeline'), 'title_desc' => __('Title With Description', 'lifeline'))),
		"description" => __("Choose Display.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Display", 'lifeline'),
		"param_name" => "display",
		"value" => array_flip(array('icon' => __('Icons', 'lifeline'), 'picture' => __('Pictures', 'lifeline'))),
		"description" => __("Choose Display.", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Button Text", 'lifeline'),
		"param_name" => "btn_text",
		"value" => '',
		"description" => __("Enter the text of the button.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Description Limit", 'lifeline'),
		"param_name" => "limit",
		"value" => '',
		"description" => __("Enter the number of characters to show in causes description", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Charity Statics", 'lifeline'),
	"base" => "sh_charity_statics",
	"class" => "", "icon" => SH_URL . '/images/vc/chairty.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Heading", 'lifeline'),
		"param_name" => "heading",
		"value" => '',
		"description" => __("Enter the Heading", 'lifeline')
	),
	array(
		"type" => "textarea",
		"class" => "",
		"heading" => __("Description", 'lifeline'),
		"param_name" => "desc",
		"value" => '',
		"description" => __("Enter the Description", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("First Box Statics", 'lifeline'),
		"param_name" => "box1",
		"value" => '',
		"description" => __("Enter the First Box Statics Number", 'lifeline')
	),
	array(
		"type" => "textfield", "class" => "",
		"heading" => __("First Box Statics Text", 'lifeline'),
		"param_name" => "box1_txt",
		"value" => '',
		"description" => __("Enter the First Box Statics text", 'lifeline')
	),
	array(
		"type" => "attach_image", "class" => "",
		"heading" => __("First Box Background Image", 'lifeline'),
		"param_name" => "box1_bg",
		"value" => '',
		"description" => __("Select the First Box Background Image", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Secound Box Statics", 'lifeline'),
		"param_name" => "box2",
		"value" => '',
		"description" => __("Enter the Secound Box Statics Number", 'lifeline')
	),
	array(
		"type" => "textfield", "class" => "",
		"heading" => __("Secound Box Statics Text", 'lifeline'),
		"param_name" => "box2_txt",
		"value" => '',
		"description" => __("Enter the Secound Box Statics text", 'lifeline')
	),
	array(
		"type" => "attach_image", "class" => "",
		"heading" => __("Secound Box Background Image", 'lifeline'),
		"param_name" => "box2_bg",
		"value" => '',
		"description" => __("Select the Secound Box Background Image", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Third Box Statics", 'lifeline'),
		"param_name" => "box3",
		"value" => '',
		"description" => __("Enter the Third Box Statics Number", 'lifeline')
	),
	array(
		"type" => "textfield", "class" => "",
		"heading" => __("Third Box Statics Text", 'lifeline'),
		"param_name" => "box3_txt",
		"value" => '',
		"description" => __("Enter the Third Box Statics text", 'lifeline')
	),
	array(
		"type" => "attach_image", "class" => "",
		"heading" => __("Third Box Background Image", 'lifeline'),
		"param_name" => "box3_bg",
		"value" => '',
		"description" => __("Select the Third Box Background Image", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Fourth Box Statics", 'lifeline'),
		"param_name" => "box4",
		"value" => '',
		"description" => __("Enter the Fourth Box Statics Number", 'lifeline')
	),
	array(
		"type" => "textfield", "class" => "",
		"heading" => __("Fourth Box Statics Text", 'lifeline'),
		"param_name" => "box4_txt",
		"value" => '',
		"description" => __("Enter the Fourth Box Statics text", 'lifeline')
	),
	array(
		"type" => "attach_image", "class" => "",
		"heading" => __("Fourth Box Background Image", 'lifeline'),
		"param_name" => "box4_bg",
		"value" => '',
		"description" => __("Select the Fourth Box Background Image", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Our Mission Carousel", 'lifeline'),
	"base" => "sh_our_mission_carousel",
	"class" => "",
	"icon" => SH_URL . '/images/vc/qote-slider.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Post Type", 'lifeline'),
			"param_name" => "post_type",
			"value" => array(
				'Post' => 'post', 
				'Testimonial' => 'dict_testimonials', 
				'Causes' => 'dict_causes', 
				'Project' => 'dict_project', 
				'Event' => 'dict_event', 
				'Portfolio' => 'dict_portfolio', 
				'Team' => 'dict_team', 
				'Services' => 'dict_services'
			),
			"description" => __("Select the post type.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category_post",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('post')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category2",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'testimonial_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_testimonials')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category3",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'project_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_project')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category4",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'event_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_event')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category5",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'portfolio_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_portfolio')
			),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category6",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'team_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_team')
			),
		),
		
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category7",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline'),
			'dependency' => array(
				'element' => 'post_type',
				'value' => array('dict_causes')
			),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Sponsor", 'lifeline'),
	"base" => "sh_sponsor",
	"class" => "",
	"icon" => SH_URL . '/images/vc/qote-slider.png',
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array("type" => "textarea_raw_html",
			"class" => "",
			"heading" => __("Description", 'lifeline'),
			"param_name" => "desc",
			"value" => "",
			"description" => __("Enter Description.", 'lifeline')
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Select Image", 'lifeline'),
			"param_name" => "image",
			"value" => '',
			"description" => __("Select Images.", 'lifeline')
		),
		array("type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'lifeline'),
			"param_name" => "btn_text",
			"value" => '',
			"description" => __("Enter the button text.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Creative Recent News", 'lifeline'),
	"icon" => '',
	"base" => "sh_creative_recent_news",
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array('col-md-6' => '2 Column', 'col-md-4' => '3 Columns')),
			"description" => __("Choose Number of Columns for Posts.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Text Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '50',
			"description" => __("Enter the text limit of this section in integer", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('post_type' => 'post', 'taxonomy' => 'category', 'hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Featured Posts", 'lifeline'),
	"base" => "sh_featured_posts", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Post Type", 'lifeline'),
			"param_name" => "post_type",
			"value" => array('Post' => 'post', 'Testimonial' => 'dict_testimonials', 'Causes' => 'dict_causes', 'Project' => 'dict_project', 'Event' => 'dict_event', 'Portfolio' => 'dict_portfolio', 'Team' => 'dict_team', 'Services' => 'dict_services'),
			"description" => __("Select the post type.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Post IDs", 'lifeline'),
			"param_name" => "post_ids",
			"value" => '',
			"description" => __("Enter posts IDs with comma seperated", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),
		array("type" => "textfield",
			"class" => "",
			"heading" => __("Read More Text", 'lifeline'),
			"param_name" => "btn_text",
			"value" => '',
			"description" => __("Enter the read more text.", 'lifeline')
		),
		array("type" => "textfield",
			"class" => "",
			"heading" => __("Content Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '',
			"description" => __("Enter the limit for content.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Post Video Carousel", 'lifeline'),
	"icon" => '', "base" => "sh_post_carousel",
	"class" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of posts", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Category", 'lifeline'),
			"param_name" => "category",
			"value" => array_flip(sh_get_categories(array('hide_empty' => FALSE), true)),
			"description" => __("Choose Category.", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose Sorting by.", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "sorting_order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose Sorting Order.", 'lifeline')
		),

		array(
			"type" => "textfield", "class" => "",
			"heading" => __("Text Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '50',
			"description" => __("Enter the text limit of this section in integer", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Blockquote Carousel", 'lifeline'),
	"base" => "sh_blockquote_carousel", "class" => "",
	"content_element" => true,
	"as_parent" => array('only' => 'sh_blockquote_text'),
	"icon" => "",
	"content_element" => true,
	"show_settings_on_create" => false,
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => "",
			"param_name" => "acc_content",
			'value' => '',
		),
	)
);
$maps[] = array(
	"name" => __("Blockquote Text", 'lifeline'),
	"base" => "sh_blockquote_text",
	"class" => "",
	"content_element" => true,
	"as_child" => array('only' => 'sh_blockquote_carousel'),
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textarea",
		"class" => "",
		"heading" => __('Content', 'lifeline'),
		"param_name" => "acc_content",
		'value' => '',
		"description" => __("Enter the Content:", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Causes with Thumb", 'lifeline'),
	"base" => "sh_causes_with_thumb", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Columns", 'lifeline'),
		"param_name" => "col",
		"value" => array_flip(array(6 => __('Two Columns', 'lifeline'), 4 => __('Three Columns', 'lifeline'), 3 => __('Four Columns', 'lifeline'))),
		"description" => __("Enter number of columns", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "", "heading" => __("Category", 'lifeline'),
		"param_name" => "cat",
		"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category'), true)),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
)
);

$maps[] = array(
	"name" => __("Causes Listing Fancy Style", 'lifeline'),
	"base" => "sh_causes_listing_fancy_style",
	"class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Columns", 'lifeline'),
			"param_name" => "cols",
			"value" => array_flip(array('6' => '2 Column', '4' => '3 Columns', '3' => '4 Columns')),
			"description" => __("Choose Number of Columns for Galleries.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Number", 'lifeline'),
			"param_name" => "number",
			"value" => '',
			"description" => __("Enter number of causes", 'lifeline')
		),
		array(
			"type" => "checkbox",
			"class" => "", "heading" => __("Category", 'lifeline'),
			"param_name" => "cat",
			"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category'), true)),
			"description" => __("Choose causes category to show causes", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Sort By", 'lifeline'),
			"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
			"description" => __("Choose sorting order for causes listing", 'lifeline')
		),
		array(
			"type" => "dropdown", "class" => "",
			"heading" => __("Sorting Order", 'lifeline'),
			"param_name" => "order",
			"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
			"description" => __("Choose order for causes listing", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Description Limit", 'lifeline'),
			"param_name" => "limit",
			"value" => '',
			"description" => __("Enter the number of characters to show in causes description", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Latest News Carousel", 'lifeline'),
	"base" => "sh_latest_news_carousel", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Category", 'lifeline'),
		"param_name" => "cat",
		"value" => array_flip(sh_get_categories(array('hide_empty' => FALSE), true)),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
)
);
$maps[] = array(
	"name" => __("Fancy Causes", 'lifeline'),
	"base" => "sh_fancy_causes",
	"class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Post", 'lifeline'),
			"param_name" => "post_id",
			"value" => array_flip(sh_get_posts_array('dict_causes')),
			"description" => __("Select Causes Post.", 'lifeline')
		),
	)
);
$maps[] = array(
	"name" => __("Single Cause Fancy Style", 'lifeline'),
	"base" => "sh_fancy_causes_2",
	"class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Post", 'lifeline'),
			"param_name" => "post_id",
			"value" => array_flip(sh_get_posts_array('dict_causes')),
			"description" => __("Select Causes Post.", 'lifeline')
		),
	)
);

$maps[] = array(
	"name" => __("Donation Parallax Box", 'lifeline'),
	"base" => "sh_donation_parallax_box", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'lifeline'),
			"param_name" => "title",
			"value" => "",
			"description" => __("Enter your title.", 'lifeline')
		),
		array("type" => "textarea",
			"class" => "",
			"heading" => __("Description", 'lifeline'),
			"param_name" => "desc",
			"value" => "",
			"description" => __("Enter your description.", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'lifeline'),
			"param_name" => "button_",
			"value" => "",
			"description" => __("Enter Button Text.", 'lifeline')
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Background Image", 'lifeline'),
			"param_name" => "bg",
			"value" => "",
			"description" => __("Select your Background Image. For the best result plese selct 384x438 image size", 'lifeline')
		),
	)
);


$maps[] = array(
	"name" => __("Projects Carousel Full Page", 'lifeline'),
	"base" => "sh_project_carousal_full_page", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Category", 'lifeline'),
		"param_name" => "category",
		"value" => array_flip(sh_get_categories(array('post_type' => 'dict_project', 'taxonomy' => 'project_category', 'hide_empty' => FALSE ), true)),
		
		"description" => __("Choose Category.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
)
);

$maps[] = array(
	"name" => __("Causes New Style", 'lifeline'),
	"base" => "sh_causes_new_style", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Category", 'lifeline'),
		"param_name" => "category",
		"value" => array_flip(sh_get_categories(array('taxonomy' => 'causes_category', 'hide_empty' => FALSE), true)),
		"description" => __("Choose Category.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name" => "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Content Options:", 'lifeline'),
		"param_name" => "c_opt",
		"value" => array(
			__('Content Excerpt', 'lifeline') => 'excerpt',
			__('Full Content', 'lifeline') => 'full',
			__('Custom Limit', 'lifeline') => 'limit',
		),
		"description" => __("Select option of showing post content.", 'lifeline')
	),
	array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Character Limit:", 'lifeline'),
		"param_name" => "c_limit",
		"value" => "",
		"description" => __("Enter the number of showing post characters.", 'lifeline'),
		'dependency' => array(
			'element' => 'c_opt',
			'value' => array('limit')
		),
	),
)
);
$maps[] = array(
	"name" => __("Urgent Cause Parallax", 'lifeline'),
	"base" => "sh_urgent_cause_parallax", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Select Cause:", 'lifeline'),
			"param_name" => "cause",
			"value" => array_flip(vp_get_posts_custom('dict_causes')),
			"description" => __("Select Cause", 'lifeline')
		),
		array("type" => "attach_image",
			"class" => "",
			"heading" => __("Background Image:", 'lifeline'),
			"param_name" => "bg",
			"value" => "",
			"description" => __("Select background image", 'lifeline')
		),
	)
);

$maps[] = array(
	"name" => __("Donation Parallax Full Page", 'lifeline'),
	"base" => "sh_donation_parallax_full_page", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Title:", 'lifeline'),
			"param_name" => "title",
			"value" => "",
			"description" => __("Enter the title of this section", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Section Sub Title:", 'lifeline'),
			"param_name" => "subtitle",
			"value" => "",
			"description" => __("Enter the sub title of this section", 'lifeline')
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Description:", 'lifeline'),
			"param_name" => "desc",
			"value" => "",
			"description" => __("Enter the description", 'lifeline')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text:", 'lifeline'),
			"param_name" => "btn_txt",
			"value" => "",
			"description" => __("Enter the text of the button", 'lifeline')
		),
	)
);

$maps[] = array(
	"name" => __("Modern Event Counter", 'lifeline'),
	"base" => "sh_modern_event_counter", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Select Event:", 'lifeline'),
			"param_name" => "cause",
			"value" => array_flip(vp_get_posts_custom('dict_event')),
			"description" => __("Select Event", 'lifeline')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Overlap", 'lifeline'),
			"param_name" => "lap",
			"value" => array(__('True', 'lifeline') => 'true', __('False', 'lifeline') => 'false'),
		),
	)
);

$maps[] = array(
	"name" => __("Charity Events New", 'lifeline'),
	"base" => "sh_charity_events_new", "class" => "",
	"icon" => "",
	"category" => __('Lifeline', 'lifeline'),
	"params" => array(array(
		"type" => "textfield",
		"class" => "",
		"heading" => __("Number", 'lifeline'),
		"param_name" => "number",
		"value" => '',
		"description" => __("Enter number of posts", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Category", 'lifeline'),
		"param_name" => "category",
		"value" => array_flip(sh_get_categories(array('post_type' => 'dict_event','taxonomy' => 'event_category', 'hide_empty' => FALSE), true)),
		"description" => __("Choose Category.", 'lifeline')
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => __("Sort By", 'lifeline'),
		"param_name" => "sort_by", "value" => array_flip(array('date' => __('Date', 'lifeline'), 'title' => __('Title', 'lifeline'), 'name' => __('Name', 'lifeline'), 'author' => __('Author', 'lifeline'), 'comment_count' => __('Comment Count', 'lifeline'), 'random' => __('Random', 'lifeline'))),
		"description" => __("Choose Sorting by.", 'lifeline')
	),
	array(
		"type" => "dropdown", "class" => "",
		"heading" => __("Sorting Order", 'lifeline'),
		"param_name"
		=> "sorting_order",
		"value" => array(__('Ascending Order', 'lifeline') => 'ASC', __('Descending Order', 'lifeline') => 'DESC'),
		"description" => __("Choose Sorting Order.", 'lifeline')
	),
)
);

foreach ($maps as $vc_arr) {
	if (sh_set($vc_arr, 'params') != '') {
		foreach (sh_set($vc_arr, 'params') as $k => $param) {
			if (sh_set($param, 'type') == 'dropdown') {
				if (array_key_exists('value', $param)) {
					$new_elment = array(__('Please select', 'lifeline') . ' ' . strtolower(sh_set($param, 'heading')) => 0);
					$param['value'] = array_merge($new_elment, $param['value']);
					$vc_arr['params'][$k]['value'] = $param['value'];
				}
			}
		}
	}
	vc_map($vc_arr);
}

class WPBakeryShortCode_sh_blockquote_carousel extends WPBakeryShortCodesContainer {
	
}

class WPBakeryShortCode_sh_blockquote_text extends WPBakeryShortCode {
	
}

function sh_custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
	if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
        $class_string = str_replace('vc_row-fluid', 'my_row-fluid', $class_string); // This will replace "vc_row-fluid" with "my_row-fluid"
    }
    if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
        $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', 'col-md-$1', $class_string); // This will replace "vc_col-sm-%" with "my_col-sm-%"
    }
    return $class_string;
}

// Filter to Replace default css class for vc_row shortcode and vc_column
add_filter('vc_shortcodes_css_class', 'sh_custom_css_classes_for_vc_row_and_vc_column', 10, 2);

function vc_theme_vc_row($atts, $content = null) {
	extract(shortcode_atts(array(
		'el_class' => '',
		'bg_image' => '',
		'bg_color' => '',
		'bg_image_repeat' => '',
		'font_color' => '',
		'posts_grid' => '',
		'padding' => '',
		'margin_bottom' => '',
		'container' => '',
		'gap' => '', 'parallax' => '',
		'parallax_clr' => '',
		'parallax_bg' => '',
		'top_margin' => '',
		'bottom_margin' => '',
		'col_title' => '',
		'heading_style' => '',
		'pattren' => '',
		'sub_title' => '',
		'css_animation'=>'',
		'css' => '',
	), $atts));
	$atts['base'] = '';
	wp_enqueue_style('js_composer_front');
	wp_enqueue_script('wpb_composer_front_js');
	wp_enqueue_style('js_composer_custom_css');
	$vc_row = new WPBakeryShortCode_VC_Row($atts);
	$el_class = $vc_row->getExtraClass($el_class);
	$el_class = $vc_row->getExtraClass($el_class);
	$css_class = $el_class;

	if ($css)
		$css_class .= vc_shortcode_custom_css_class($css, ' ') . ' ';
	$output = '';

	$my_class = ($css_animation) ? ' animated '.$css_animation : '';
	$style = $vc_row->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);

	$output .= '<section class="block ' . $css_class . ' ' . $my_class . '';
	if ($top_margin): $output .= ' remove-top';
	endif;
	if ($bottom_margin): $output .= ' remove-bottom';
	endif;
	$output .= '"><div class="container">';

	if ($parallax == 'true'):
		if ($parallax_bg):
			$img = wp_get_attachment_image_src($parallax_bg, 'full');
		else:
			$img = array('0' => '');
		endif;
		$output .= '<div class="fixed';
		if ($pattren == 1) : $output .= ' pattern ';
		endif;
		$output .= ' ' . $parallax_clr . '" style="background:url(' . $img[0] . ');"></div>';
	endif;
	$output .= '<div class="row">';
	if ($heading_style != '' && $col_title != ''): $output .= '<div class="col-md-12">';
		$con = explode(' ', $col_title, 2);
		if ($heading_style == 'sec-heading') {
			$output .='<div class="' . $heading_style . '">
			<h2><strong>' . sh_set($con, '0') . '</strong> ' . sh_set($con, '1') . '</h2>
			</div>';
		} else if ($heading_style == 'sec-title') {
			$output .='<div class="' . $heading_style . '">
			<h2><span>' . sh_set($con, '0') . '</span> ' . sh_set($con, '1') . '</h2>
			</div>';
		} else if ($heading_style == 'sec-heading2') {
			$output .='<div class="' . $heading_style . '">
			<h2>' . sh_set($con, '0') . ' <strong> ' . sh_set($con, '1') . '</strong></h2>
			</div>';
		} else if ($heading_style == 'sec-heading3') {
			$output .='<div class="' . $heading_style . '">
			<h5>' . $sub_title . ' </h5><h1> ' . $col_title . '</h1>
			</div>';
		} else if ($heading_style == 'parallax') {
			$output .='<div class="sec-heading3">
			<h2> ' . $col_title . '</h2>
			</div>';
		} else if ($heading_style == 'sec-heading4') {
			$output .='<div class="sec-heading4">
			<h2>' . sh_set($con, '0') . ' <span>' . sh_set($con, '1') . '</span></h2>
			<p>' . $sub_title . '</p>
			</div>';
		}
		$output .= '</div>';
	endif;
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div>';

	$output .='</section>';
	return $output;
}

function vc_theme_vc_column($atts, $content = null) {
	extract(shortcode_atts(array('width' => '1/1', 'el_class' => '', 'col_title' => '', 'sub_title' => '', 'heading_style' => ''), $atts));
	$width_col = wpb_translateColumnWidthToSpan($width);
	$width = str_replace('vc_col-sm-', 'col-md-', $width_col . ' column');

	$el_class = ($el_class) ? ' ' . $el_class : '';
	$output = '<div class="' . $width . '">';
	if ($heading_style != '' && $col_title != ''):
		$con = explode(' ', $col_title, 2);
		if ($heading_style == 'sec-heading') {
			$output .='<div class="' . $heading_style . '">
			<h2><strong>' . sh_set($con, '0') . '</strong> ' . sh_set($con, '1') . '</h2>
			</div>';
		} else if ($heading_style == 'sec-title') {
			$output .='<div class="' . $heading_style . '">
			<h2><span>' . sh_set($con, '0') . '</span> ' . sh_set($con, '1') . '</h2>
			</div>';
		} else if ($heading_style == 'sec-heading2') {
			$output .='<div class="' . $heading_style . '">
			<h2>' . sh_set($con, '0') . ' <strong> ' . sh_set($con, '1') . '</strong></h2>
			</div>';
		} else if ($heading_style == 'sec-heading3') {
			$output .='<div class="' . $heading_style . '">
			<h5>' . $sub_title . ' </h5><h1> ' . $col_title . '</h1>
			</div>';
		} else if ($heading_style == 'parallax') {
			$output .='<div class="sec-heading3">
			<h2> ' . $col_title . '</h2>
			</div>';
		} else if ($heading_style == 'sec-heading4') {
			$output .='<div class="sec-heading4">
			<h2>' . sh_set($con, '0') . ' <span>' . sh_set($con, '1') . '</span></h2>
			<p>' . $sub_title . '</p>
			</div>';
		}
	endif;
	$output .= do_shortcode($content);
	$output .= '</div>';
	return $output;
}
//function vc_theme_vc_column($atts, $content = null) {
//	extract(shortcode_atts(array(
//		'width' => '1/1',
//		'el_class' => '',
//		'back_ground' => '',
//		'top_margin' => '',
//		'css'   =>'',
//	), $atts));
//	$atts['base'] = '';
//
//	$width_col = wpb_translateColumnWidthToSpan($width);
//	$width = str_replace('vc_col-sm-', 'col-md-', $width_col);
//	$el_class = ($el_class) ? ' ' . $el_class : '';
//
//	$css_class = $el_class;
//
//	$css_class = ($css) ? apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $atts['base'], $atts) . ' ' . $css_class : $css_class;
//	$output = '<div class="' . $width . '  ' . $css_class . '">';
//	if ($top_margin) {
//		$output .= '<div class="' . $top_margin . '">';
//	}
//	$output .= do_shortcode($content);
//	if ($top_margin) {
//		$output .= '</div>';
//	}
//	$output .= '</div>';
//	return $output;
//}
$title = array(
	"type" => "textfield",
	"class" => "",
	"heading" => __("Enter the Title", 'lifeline'),
	"param_name" => "col_title",
	"description" => __("Enter the title of this section.", 'lifeline')
);
$sub_title = array(
	"type" => "textfield",
	"class" => "",
	"heading" => __("Enter the Sub Title", 'lifeline'),
	"param_name" => "sub_title",
	"description" => __("Enter the Sub title of this section. This is only work for Title with Subtitle heading style.", 'lifeline')
);
$heading_style = array(
	"type" => "dropdown",
	"class" => "",
	"heading" => __("Heading Style", 'lifeline'),
	"param_name" => "heading_style",
	"value" => array('' => '', __('Title for Parallax', 'lifeline') => 'parallax', __('Underline Title', 'lifeline') => 'sec-heading', __('Modern Style', 'lifeline') => 'sec-title', __('Doubble Border Title', 'lifeline') => 'sec-heading2', __('Title with Subtitle', 'lifeline') => 'sec-heading3', __('Innovative 2015', 'lifeline') => 'sec-heading4'),
	"description" => __("Choose the Heading style You want to choose.", 'lifeline')
);
$parallax = array(
	"type" => "dropdown",
	"class" => "",
	"heading" => __("Parallax", 'lifeline'),
	"param_name" => "parallax",
	"value" => array('False' => 'false', 'True' => 'true'),
	"description" => __("Make this section as parallax.", 'lifeline')
);
$parallax_clr = array("type" => "dropdown",
	"class" => "",
	"heading" => __("Parallax Style", 'lifeline'),
	"param_name" => "parallax_clr",
	"value" => array(__('No Layer', 'lifeline') => 'no-layer', __('Whitish', 'lifeline') => 'whitish', __('Blackish', 'lifeline') => 'blackish'),
	"description" => __("Choose Style for Parallax.", 'lifeline')
);
$parallax_img = array(
	"type" => "attach_image",
	"class" => "",
	"heading" => __("Parallax Background", 'lifeline'),
	"param_name" => "parallax_bg",
	"description" => __("Make this section as parallax.", 'lifeline')
);
$top_margin = array(
	"type" => "checkbox", "class" => "",
	"heading" => __("Top Margin", 'lifeline'),
	"param_name" => "top_margin",
	"value" => array('Top Margin' => true),
	"description" => __("Remove top margin of this section.", 'lifeline')
);
$patren = array(
	"type" => "checkbox",
	"class" => "",
	"heading" => __("Pattern", 'lifeline'),
	"param_name" => "pattren",
	"value" => array('Show Pattern' => true), "description" => __("Check this if you are choosing some pattern as background.", 'lifeline')
);
$bottom_margin = array(
	"type" => "checkbox",
	"class" => "",
	"heading" => __("Bottom Margin", 'lifeline'), "param_name" => "bottom_margin",
	"value" => array('Bottom Margin' => true),
	"description" => __("Remove bottom margin of this section.", 'lifeline')
);


vc_add_param('vc_column', $title);
vc_add_param('vc_column', $sub_title);
vc_add_param('vc_column', $heading_style);
vc_add_param('vc_row', $title);
vc_add_param('vc_row', $sub_title);
vc_add_param('vc_row', $heading_style);
vc_add_param('vc_row', $parallax);
vc_add_param('vc_row', $parallax_clr);
vc_add_param('vc_row', $parallax_img);
vc_add_param('vc_row', $patren);
vc_add_param('vc_row', $top_margin);
vc_add_param('vc_row', $bottom_margin);

vc_remove_param("vc_row", "full_width");
vc_remove_param("vc_row", "parallax_image");
vc_remove_param("vc_row", "el_id");
/*vc_remove_param("vc_row", "css");*/
vc_remove_param("vc_row", "disable_element");
vc_remove_param("vc_row", "gap");
vc_remove_param("vc_row", "content_placement");
//vc_remove_param("vc_row", "video_bg");