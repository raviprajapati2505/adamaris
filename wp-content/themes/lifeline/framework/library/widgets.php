<?php
/** function widget($args, $instance) see @ WP_Widget::widget */
/** function update($new_instance, $old_instance) see @ WP_Widget::update */
/** function form($instance) see @ WP_Widget::form */
//People Reviews
class SH_people_reviews extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'PeopleReviews', /* Name */ __('People Reviews', 'lifeline'), array('description' => __('This widgtes is used to add People Reviews to Footer', 'lifeline')));
    }
    function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $number_of_reviews = apply_filters('widget_number_of_reviews', $instance['number_of_reviews']);
        echo wp_kses_post($before_widget);
        echo wp_kses_post($before_title) . '<strong><span>' . substr($title, 0, 1) . '</span>' . substr($title, 1) . '</strong> ' . $sub_title . $after_title;
        $Records = '';
        $args = array('post_type' => 'dict_testimonials', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => $number_of_reviews);
        $loop = new WP_Query($args);
		$args = array(
					'status' => 'all',
					'number' => '5',
				);
		$comments = get_comments($args);
		//foreach($comments as $comment) :
			//echo($comment->comment_author . '<br />' . $comment->comment_content);
		//endforeach;die;
        //if ($loop->have_posts()) : while ($loop->have_posts()): $loop->the_post();
		if(!empty($comments)){
			foreach($comments as $comment) {
			$FirstLetter = substr(strip_tags($comment->comment_content), 0, 1);
			$RemainingReviews = substr(strip_tags($comment->comment_content), 1);
			$Settings = get_post_meta(get_the_ID(), '_dict_testimonials_settings', true);
			
			
			$Records .= '<li>
			<div class="review"> <i>' . $FirstLetter . '</i>
				<p><span>' . substr($FirstLetter, 1) . '</span> ' . $RemainingReviews . '</p>
			</div>
			<div class="from">
				<h6>' . $comment->comment_author . '</h6>
				</div>
			</li>';
			}
		}
		//<span>' . sh_set($Settings, 'designation') . ', ' . sh_set($Settings, 'location') . '</span> 
        //endwhile;
        //endif;
        wp_reset_query();
        ?>
        <div class="footer_carousel">
            <ul class="slides">
                <?php if( function_exists( 'lifeline_print_output' ) ) : ?>
                    <?php echo lifeline_print_output($Records); ?>
                <?php endif; ?>
            </ul>
        </div>
        <script>
            jQuery(document).ready(function ($) {
                if ($('.footer_carousel').length) {
                    $('.footer_carousel').flexslider({
                        animation: "slide",
                        animationLoop: false,
                        slideShow: false,
                        controlNav: true,
                        maxItems: 1,
                        pausePlay: false,
                        mousewheel: false,
                        start: function (slider) {
                            $('body').removeClass('loading');
                        }
                    });
                }
            });
        </script>
        <?php
        echo lifeline_print_output($after_widget);
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['number_of_reviews'] = $new_instance['number_of_reviews'];
        return $instance;
    }
    function form($instance) {
        $title = ($instance) ? esc_attr($instance['title']) : __('People', 'lifeline');
        $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Reviews', 'lifeline');
        $number_of_reviews = ($instance) ? esc_attr($instance['number_of_reviews']) : '';
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title') ); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('number_of_reviews') ); ?>"><?php _e('Number Of Reviews:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('number_of_reviews') ); ?>" name="<?php echo esc_attr( $this->get_field_name('number_of_reviews') ); ?>" type="text" value="<?php echo esc_attr( $number_of_reviews ); ?>" />
        </p>
        <?php
    }
}
// Flicker Gallery
class SH_Flickr extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'SH_Flickr', /* Name */ __('Flickr Feed', 'lifeline'), array('description' => __('Fetch the latest feed from Flickr', 'lifeline')));
    }
    function widget($args, $instance) {
        static $counter = 1;
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $flickr_id = apply_filters('widget_flickr_id', $instance['flickr_id']);
        $number = apply_filters('widget_number', $instance['number']);
        echo wp_kses_post( $before_widget );
        echo wp_kses_post($before_title) . '<strong><span>' . substr($title, 0, 1) . '</span>' . substr($title, 1) . '</strong> ' . $sub_title . $after_title;
        $limit = ( $number ) ? $number : 9;
        //$counter = 1;
        ?>
        <div class="lightbox flickr-images flickr-images<?php echo esc_attr( $counter ); ?>">
            
        </div>

        <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    
                    $('.flickr-images<?php echo esc_attr($counter);?>').jflickrfeed({
                        limit: <?php echo esc_attr($limit); ?>,
                        qstrings: {id: '<?php echo esc_attr($instance['flickr_id']); ?>'},
                        itemTemplate: '<a href="{{image_b}}" ><img  src="{{image_s}}" alt="{{title}}" /></a>'
                    });
                });
                jQuery(window).on('load',function ($) {
                    
                    var foo = jQuery('.flickr-images<?php echo esc_attr($counter); ?>');
                foo.poptrox();
                });
               
            </script><?php
         $counter++;
        echo wp_kses_post($after_widget);
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['flickr_id'] = $new_instance['flickr_id'];
        $instance['number'] = $new_instance['number'];
        return $instance;
    }
    function form($instance) {
        wp_enqueue_script('flickrjs');
        $title = ($instance) ? esc_attr($instance['title']) : __('Flicker', 'lifeline');
        $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Feed', 'lifeline');
        $flickr_id = ($instance) ? esc_attr($instance['flickr_id']) : '';
        $number = ( $instance ) ? esc_attr($instance['number']) : 8;
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr( $sub_title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('flickr_id')); ?>"><?php _e('Flickr ID:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('flickr_id')); ?>" name="<?php echo esc_attr($this->get_field_name('flickr_id')); ?>" type="text" value="<?php echo esc_attr($flickr_id); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number of Tweets:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number') ); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        <?php
    }
}
//Contact Us
class SH_Contact_US extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'ContactUs', /* Name */ __('Contact Us', 'lifeline'), array('description' => __('This widgtes is used to add Contact Us details to Footer', 'lifeline')));
    }
    function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $address = apply_filters('widget_address', $instance['address']);
        $phone_no = apply_filters('widget_phone_no', $instance['phone_no']);
        $email_id = apply_filters('widget_email_id', $instance['email_id']);
        $web_address = apply_filters('widget_web_address', $instance['web_address']);
        echo wp_kses_post($before_widget);
        $TitleFirstLetter = substr($title, 0, 1);
        echo wp_kses_post($before_title ). '<strong><span>' . substr($title, 0, 1) . '</span>' . substr($title, 1) . '</strong> ' . $sub_title . $after_title;
        ?>
        <ul class="contact-details">
            <?php if($address) : ?>
                <li>
                    <span><i class="icon-home"></i><?php _e("ADDRESS", 'lifeline'); ?></span>
                    <p><?php echo wp_kses_post($address); ?></p>
                </li>
            <?php endif; ?>
            <?php if($phone_no) : ?>
                <li>
                    <span><i class="icon-phone-sign"></i><?php _e("PHONE NO", 'lifeline'); ?></span>
                    <p><?php echo esc_html($phone_no); ?></p>
                </li>
            <?php endif; ?>
            <?php if($email_id) : ?>
                <li>
                    <span><i class="icon-envelope-alt"></i><?php _e("EMAIL ID", 'lifeline'); ?></span>
                    <p><a href="mailto:<?php echo esc_attr($email_id); ?>"><?php echo esc_html($email_id ); ?></a></p>
                </li>
            <?php endif; ?>
            <?php if($web_address) : ?>
                <li>
                    <span><i class="icon-link"></i><?php _e("WEB ADDRESS", 'lifeline'); ?></span>
                    <p><a href="<?php echo esc_attr($web_address); ?>"><?php echo esc_attr($web_address); ?></a></p>
                </li>
            <?php endif; ?>
        </ul>
        <?php
        echo wp_kses_post($after_widget);
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['address'] = $new_instance['address'];
        $instance['phone_no'] = $new_instance['phone_no'];
        $instance['email_id'] = $new_instance['email_id'];
        $instance['web_address'] = $new_instance['web_address'];
        return $instance;
    }
    function form($instance) {
        $title = ($instance) ? esc_attr($instance['title']) : __('Contact', 'lifeline');
        $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Us', 'lifeline');
        $address = ($instance) ? esc_attr($instance['address']) : '';
        $phone_no = ($instance) ? esc_attr($instance['phone_no']) : '';
        $email_id = ($instance) ? esc_attr($instance['email_id']) : '';
        $web_address = ($instance) ? esc_attr($instance['web_address']) : '';
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title') ); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php _e('Address:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('address') ); ?>" name="<?php echo esc_attr($this->get_field_name('address') ); ?>" type="text" value="<?php echo esc_attr($address); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('phone_no')); ?>"><?php _e('Phone Number:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone_no')); ?>" name="<?php echo esc_attr( $this->get_field_name('phone_no') ); ?>" type="text" value="<?php echo esc_attr($phone_no); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('email_id')); ?>"><?php _e('Email:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('email_id')); ?>" name="<?php echo esc_attr($this->get_field_name('email_id')); ?>" type="text" value="<?php echo esc_attr($email_id); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('web_address')); ?>"><?php _e('Website URL:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('web_address')); ?>" name="<?php echo esc_attr($this->get_field_name('web_address')); ?>" type="text" value="<?php echo esc_attr($web_address); ?>" />  
        </p>
        <?php
    }
}
//News Letter Subscription
class SH_News_Letter_Subscription extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'NewsLetterSubscription', /* Name */ __('Newsletter Subscription', 'lifeline'), array('description' => __('This widgtes is used to add news letter subscription form to Footer', 'lifeline')));
    }
    function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $text = apply_filters('widget_text', $instance['text']);
        $rss_link = apply_filters('widget_rss_link', $instance['rss_link']);
        $facebook_link = apply_filters('widget_facebook_link', $instance['facebook_link']);
        $gplus_link = apply_filters('widget_gplus_link', $instance['gplus_link']);
        $twitter_link = apply_filters('widget_twitter_link', $instance['twitter_link']);
        $pinteres_link = apply_filters('widget_pinteres_link', $instance['pinteres_link']);

        $youtube_link = sh_set($instance, 'youtube_link') ? apply_filters('widget_youtube_link', $instance['youtube_link']) : '';
	    $instagram_link = sh_set($instance, 'instagram_link') ? apply_filters('widget_instagram_link', $instance['instagram_link']) : '';
	    $linkedin_link = sh_set($instance, 'linkedin_link') ? apply_filters('widget_linkedin_link', $instance['linkedin_link']) : '';
        $ID = apply_filters('widget_ID', $instance['ID']);
        ?>
        <?php echo wp_kses_post($before_widget); ?>
        <?php if (sh_set($instance, 'form_type') == 'mailchimp'): ?>
            <?php wp_enqueue_script('newsletter-script');?>
            <form id="newsletter-email" method="post">
                <div class="newsletter" >
                    <h4><strong><?php echo wp_kses_post($title); ?></strong> <?php echo wp_kses_post($sub_title); ?></h4>
                    <p><?php echo wp_kses_post($text); ?></p>
                    <div class="newsletter-message"></div>
                    <input class="form-control" type="email"  name="email" value="" id="email" placeholder="<?php _e("domain@mail.com", 'lifeline'); ?>" />
                    <input type="hidden" id="uri" name="uri" value="<?php echo esc_attr($ID); ?>">
                    <input type="hidden" value="en_US" name="loc">
                </div>
                <?php
                if (!empty($rss_link) || !empty($facebook_link) || !empty($google_plus_link) || !empty($twitter_link) || !empty($pinteres_link) || !empty($youtube_link) || !empty($instagram_link) || !empty($linkedin_link)) {
                    ?>
                    <ul class="social-bar">
                        <?php
                        echo (!empty($rss_link) ) ? '<li><a href="' . $rss_link . '"    target="_blank" ><img src="' . get_template_directory_uri() . '/images/rss.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($facebook_link) ) ? '<li><a href="' . $facebook_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/facebook.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($gplus_link) ) ? '<li><a href="' . $gplus_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/gplus.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($twitter_link) ) ? '<li><a href="' . $twitter_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/twitter-icon.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($pinteres_link) ) ? '<li><a href="' . $pinteres_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/pinterest.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($youtube_link) ) ? '<li><a href="' . $youtube_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/youtube.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($instagram_link) ) ? '<li><a href="' . $instagram_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/insta.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($linkedin_link) ) ? '<li><a href="' . $linkedin_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/linked-in.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        ?>
                    </ul>
                    <?php
                }
                ?>
                <div class="newsletter-btn">
                    <input id="newsletter-form-submit" name="submit" type="submit" value="<?php _e("Submit", 'lifeline'); ?>" />
                </div>
            </form>
        <?php else: ?>
            <form name="newletter_sub" method="post" action="http://feedburner.google.com/fb/a/mailverify" target="popupwindow">
                <div class="newsletter">
                    <h4><strong><?php echo wp_kses_post($title); ?></strong> <?php echo wp_kses_post($sub_title); ?></h4>
                    <p><?php echo wp_kses_post($text); ?></p>
                    <input class="form-control" type="email"  name="email" value="" id="email" placeholder="<?php _e("domain@mail.com", 'lifeline'); ?>" />
                    <input type="hidden" id="uri" name="uri" value="<?php echo esc_attr($ID); ?>">
                    <input type="hidden" value="en_US" name="loc">
                </div>
                <?php
                if (!empty($rss_link) || !empty($facebook_link) || !empty($google_plus_link) || !empty($twitter_link) || !empty($pinteres_link) || !empty($youtube_link) || !empty($instagram_link) || !empty($linkedin_link)) {
                    ?>
                    <ul class="social-bar">
                        <?php
                        echo (!empty($rss_link) ) ? '<li><a href="' . $rss_link . '"    target="_blank" ><img src="' . get_template_directory_uri() . '/images/rss.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($facebook_link) ) ? '<li><a href="' . $facebook_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/facebook.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($gplus_link) ) ? '<li><a href="' . $gplus_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/gplus.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($twitter_link) ) ? '<li><a href="' . $twitter_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/twitter-icon.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($pinteres_link) ) ? '<li><a href="' . $pinteres_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/pinterest.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($youtube_link) ) ? '<li><a href="' . $youtube_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/youtube.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($instagram_link) ) ? '<li><a href="' . $instagram_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/insta.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        echo (!empty($linkedin_link) ) ? '<li><a href="' . $linkedin_link . '"   target="_blank" ><img src="' . get_template_directory_uri() . '/images/linked-in.jpg" alt="'.esc_attr__( 'Not Found', 'lifeline' ).'" /></a></li>' : '';
                        ?>
                    </ul>
                    <?php
                }
                ?>
                <div class="newsletter-btn">
                    <input name="submit" type="submit" value="<?php _e("Submit", 'lifeline'); ?>" />
                </div>
            </form>
        <?php endif; ?>
        <?php echo wp_kses_post($after_widget); ?>
        <?php
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['text'] = $new_instance['text'];
        $instance['form_type'] = $new_instance['form_type'];
        $instance['rss_link'] = $new_instance['rss_link'];
        $instance['facebook_link'] = $new_instance['facebook_link'];
        $instance['gplus_link'] = $new_instance['gplus_link'];
        $instance['twitter_link'] = $new_instance['twitter_link'];
        $instance['pinteres_link'] = $new_instance['pinteres_link'];
	    $instance['youtube_link'] = $new_instance['youtube_link'];
	    $instance['instagram_link'] = $new_instance['instagram_link'];
	    $instance['linkedin_link'] = $new_instance['linkedin_link'];
        $instance['ID'] = $new_instance['ID'];
        return $instance;
    }
    function form($instance) {
        $title = ($instance) ? esc_attr($instance['title']) : __('SIGNUP', 'lifeline');
        $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('NEWSLETTER', 'lifeline');
        $text = ($instance) ? esc_attr($instance['text']) : '';
        $form_type = ($instance) ? esc_attr($instance['form_type']) : '';
        $rss_link = ($instance) ? esc_attr($instance['rss_link']) : '';
        $facebook_link = ($instance) ? esc_attr($instance['facebook_link']) : '';
        $gplus_link = ($instance) ? esc_attr($instance['gplus_link']) : '';
        $twitter_link = ($instance) ? esc_attr($instance['twitter_link']) : '';
        $pinteres_link = ($instance) ? esc_attr($instance['pinteres_link']) : '';
	    $youtube_link = '';//($instance) ? esc_attr($instance['youtube_link']) : '';
	    $instagram_link = ($instance) ? esc_attr($instance['instagram_link']) : '';
	    $linkedin_link = ($instance) ? esc_attr($instance['linkedin_link']) : '';
        $ID = ($instance) ? esc_attr($instance['ID']) : '';
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title') ); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('text')); ?>"><?php _e('Text:', 'lifeline'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('text')); ?>" name="<?php echo esc_attr($this->get_field_name('text')); ?>" > <?php echo wp_kses_post($text); ?></textarea> 
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('form_type')); ?>"><?php _e('Newsletter Form Type:', 'lifeline'); ?></label>
            <select class="newsletter-frm-type" name="<?php echo esc_attr($this->get_field_name('form_type')); ?>" >
                <option value="">--<?php esc_html_e('--Select Form Type', 'lifeline'); ?>--</option>
                <option<?php echo esc_attr($form_type == 'mailchimp') ? ' selected="selected"' : ''; ?> value="mailchimp"><?php esc_html_e('Mailchimp', 'lifeline') ?></option>
                <option<?php echo esc_attr($form_type == 'feedburner') ? ' selected="selected"' : ''; ?> value="feedburner"><?php esc_html_e('Feedburner', 'lifeline') ?></option>
            </select>
        </p>
        <p class="feedburner-field" <?php echo esc_attr($form_type == 'mailchimp') ? 'style="display:none;"' : '';?>>
            <label for="<?php echo esc_attr($this->get_field_id('ID')); ?>"><?php _e('FeedBurner ID:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('ID')); ?>" name="<?php echo esc_attr($this->get_field_name('ID')); ?>" type="text" value="<?php echo esc_attr($ID); ?>" />
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('rss_link')); ?>"><?php _e('RSS Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('rss_link')); ?>" name="<?php echo esc_attr($this->get_field_name('rss_link')); ?>" type="text" value="<?php echo esc_attr($rss_link); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('facebook_link')); ?>"><?php _e('Facebook Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('facebook_link')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook_link')); ?>" type="text" value="<?php echo esc_attr($facebook_link); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('gplus_link')); ?>"><?php _e('Gogle Plus Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('gplus_link')); ?>" name="<?php echo esc_attr($this->get_field_name('gplus_link')); ?>" type="text" value="<?php echo esc_attr($gplus_link); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('twitter_link') ); ?>"><?php _e('Twitter Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter_link')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter_link')); ?>" type="text" value="<?php echo esc_attr($twitter_link); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('pinteres_link')); ?>"><?php _e('Pinteres Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('pinteres_link')); ?>" name="<?php echo esc_attr($this->get_field_name('pinteres_link') ); ?>" type="text" value="<?php echo esc_attr($pinteres_link); ?>" /> 
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('youtube_link')); ?>"><?php _e('YouTube Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('youtube_link')); ?>" name="<?php echo esc_attr($this->get_field_name('youtube_link')); ?>" type="text" value="<?php echo esc_attr($youtube_link); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('instagram_link')); ?>"><?php _e('Instagram Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('instagram_link')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram_link')); ?>" type="text" value="<?php echo esc_attr($instagram_link ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('linkedin_link')); ?>"><?php _e('Linkedin Link:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('linkedin_link')); ?>" name="<?php echo esc_attr($this->get_field_name('linkedin_link')); ?>" type="text" value="<?php echo esc_attr($linkedin_link); ?>" />
        </p>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                $('.newsletter-frm-type').on('change', function(){
                    if($(this).val() == 'mailchimp'){
                        $('.feedburner-field').css('display','none');
                    }else{
                        $('.feedburner-field').css('display','block');
                    }
                });
            });
        </script>
        <?php
    }
}
//Galleries
class SH_Galleries extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'Galleries', /* Name */ __('Galleries', 'lifeline'), array('description' => __('This widgtes is used to add Galleries.', 'lifeline')));
    }
    function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $number = apply_filters('widget_number', $instance['number']);
        echo wp_kses_post($before_widget);
        echo wp_kses_post($before_title) . $title . ' <span>' . $sub_title . '</span>' . $after_title;
        $Records = '';
        $CoverImage = '';
        $Posts = query_posts('post_type=dict_gallery&posts_per_page=' . $number);
        if (have_posts()): while (have_posts()): the_post();
        $Records .= '<div class="col-md-4">
        <a href="' . get_permalink() . '"  >' . get_the_post_thumbnail(get_the_ID(), '150x150') . '</a>
    </div>';
    endwhile;
    endif;
    wp_reset_query();
    ?>
    <div class="gallery row">
        <?php echo wp_kses_post($Records); ?>
    </div>
    <?php
    echo wp_kses_post($after_widget);
}
function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['sub_title'] = strip_tags($new_instance['sub_title']);
    $instance['number'] = $new_instance['number'];
    return $instance;
}
function form($instance) {
    $title = ($instance) ? esc_attr($instance['title']) : __('Gallery', 'lifeline');
    $sub_title = ($instance) ? esc_attr($instance['sub_title']) : '';
    $number = ($instance) ? esc_attr($instance['number']) : '';
    ?>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
    </p>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
    </p>
    <p>    
        <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number Of Galleries:', 'lifeline'); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
    </p>
    <?php
}
}
//Popular Posts
class SH_Popular_Posts extends WP_Widget {
    function __construct() {
        parent::__construct(/* Base ID */'PopularPosts', /* Name */ __('Popular Posts', 'lifeline'), array('description' => __('This widgtes is used to add Popular Posts in sidebar.', 'lifeline')));
    }
    function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
        $number = apply_filters('widget_number', $instance['number']);
        echo wp_kses_post($before_widget);
        echo wp_kses_post($before_title) . $title . ' <span>' . $sub_title . '</span>' . $after_title;
        $Records = '';
        $CoverImage = '';
        $posts = query_posts('orderby=comment_count&order=DESC&post_type=post&posts_per_page=' . $number);
        if (have_posts()): while (have_posts()): the_post();
        $NumberOFComments = get_comments_number(get_the_ID());
        if ($NumberOFComments == 0)
            $NumberOFComments = '0 comments';
        else if ($NumberOFComments == 1)
            $NumberOFComments = '1 comment';
        else
            $NumberOFComments .= ' comments';
        ?>
        <div class="popular-post"> 
            <?php echo get_the_post_thumbnail(get_the_ID(), '270x155'); ?>
            <div class="popular-post-title">
                <h6><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a></h6>
                <span><?php echo get_the_date('F d, Y') . ' / ' . $NumberOFComments; ?></span> </div>
            </div>
            <?php
            endwhile;
            endif;
            wp_reset_query();
            echo wp_kses_post($after_widget);
        }
        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['sub_title'] = strip_tags($new_instance['sub_title']);
            $instance['number'] = $new_instance['number'];
            return $instance;
        }
        function form($instance) {
            $title = ($instance) ? esc_attr($instance['title']) : __('Popular', 'lifeline');
            $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Posts', 'lifeline');
            $number = ($instance) ? esc_attr($instance['number']) : ''
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title') ); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title') ); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
            </p>
            <p>    
                <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number Of Posts:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
            </p>
            <?php
        }
    }
//Recent Events
    class SH_Recent_Events extends WP_Widget {
        function __construct() {
            parent::__construct(/* Base ID */'RecentEvents', /* Name */ __('Recent Events', 'lifeline'), array('description' => __('This widgtes is used to add Recent Events in sidebar.', 'lifeline')));
        }
        function widget($args, $instance) {
            extract($args);
            $title = apply_filters('widget_title', $instance['title']);
            $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
            $number = apply_filters('widget_number', $instance['number']);
            $orderby = apply_filters('widget_orderby', $instance['orderby']);
            $order = apply_filters('widget_order', $instance['order']);
            echo wp_kses_post($before_widget);
            echo wp_kses_post($before_title) . $title . ' <span>' . $sub_title . '</span>' . $after_title;
            $Records = '';
            $CoverImage = '';
            $Posts = query_posts('orderby=' . $orderby . '&order=' . $order . '&post_type=dict_event&posts_per_page=' . $number);
            if (have_posts()): while (have_posts()): the_post();
            $NumberOFComments = get_comments_number(get_the_ID());
            if ($NumberOFComments == 0)
                $NumberOFComments = '0 comments';
            else if ($NumberOFComments == 1)
                $NumberOFComments = '1 comment';
            else
                $NumberOFComments .= ' comments';
            $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
            $EventdateDetails = '';
            if (!empty($Settings['start_date'])) {
                $Eventdate = new DateTime($Settings['start_date']);
                $EventdateDetails = $Eventdate->format('F d, Y') . ' / ';
            } else if (!empty($Settings['end_date'])) {
                $Eventdate = new DateTime($Settings['end_date']);
                $EventdateDetails = $Eventdate->format('F d, Y') . ' / ';
            }
            ?>
            <div class="popular-post"> 
                <?php echo get_the_post_thumbnail(get_the_ID(), '270x103'); ?>
                <div class="popular-post-title">
                    <h6><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a></h6>
                    <span><?php echo wp_kses_post($EventdateDetails) . $NumberOFComments; ?></span> </div>
                </div>
                <?php
                endwhile;
                endif;
                wp_reset_query();
                echo  wp_kses_post($after_widget);
            }
            function update($new_instance, $old_instance) {
                $instance = $old_instance;
                $instance['title'] = strip_tags($new_instance['title']);
                $instance['sub_title'] = strip_tags($new_instance['sub_title']);
                $instance['number'] = $new_instance['number'];
                $instance['orderby'] = $new_instance['orderby'];
                $instance['order'] = $new_instance['order'];
                return $instance;
            }
            function form($instance) {
                $title = ($instance) ? esc_attr($instance['title']) : __('Recent', 'lifeline');
                $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Events', 'lifeline');
                $number = ($instance) ? esc_attr($instance['number']) : '';
                $orderby = ($instance) ? esc_attr($instance['orderby']) : __('date', 'lifeline');
                $order = ($instance) ? esc_attr($instance['order']) : __('ASC', 'lifeline');
                $OrderByOptions = $OrderOptions = '';
                $OptArray1 = array('date' => 'Date', 'title' => 'Title', 'name' => 'Name', 'author' => 'Author', 'comment_count' => 'Comment Count', 'random' => 'Random');
                foreach ($OptArray1 as $k => $v) {
                    $SelectedOrderByOption = ( $k == $orderby ) ? ' selected="selected"' : '';
                    $OrderByOptions .= '<option value="' . $k . '"' . $SelectedOrderByOption . '>' . $v . '</option>';
                }
                $OptArray2 = array('ASC' => 'Ascending Order', 'DESC' => 'Descending order');
                foreach ($OptArray2 as $k => $v) {
                    $SelectedOrderOption = ( $k == $orderby ) ? ' selected="selected"' : '';
                    $OrderOptions .= '<option value="' . $k . '"' . $SelectedOrderOption . '>' . $v . '</option>';
                }
                ?>
                <p>
                    <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
                </p>
                <p>
                    <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number Of Events:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('orderby')); ?>"><?php _e('Order By:', 'lifeline'); ?></label>
                    <select class="widefat" id="<?php echo esc_attr($this->get_field_id('orderby')); ?>" name="<?php echo esc_attr($this->get_field_id('orderby')); ?>">
                        <?php echo lifeline_print_output($OrderByOptions); ?>
                    </select>
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('order')); ?>"><?php _e('Order:', 'lifeline'); ?></label>
                    <select class="widefat" id="<?php echo esc_attr($this->get_field_id('order')); ?>" name="<?php echo esc_attr($this->get_field_id('order')); ?>">
                        <?php echo lifeline_print_output($OrderOptions); ?>
                    </select>
                </p>
                <?php
            }
        }
//Video Widget
        class SH_Video extends WP_Widget {
            function __construct() {
                parent::__construct(/* Base ID */'Video', /* Name */ __('Video', 'lifeline'), array('description' => __('This widgtes is used to add Video to sidebar.', 'lifeline')));
            }
            function widget($args, $instance) {
                extract($args);
                $title = apply_filters('widget_title', sh_set($instance, 'title'));
                $sub_title = apply_filters('widget_sub_title', sh_set($instance, 'sub_title'));
                $number = apply_filters('widget_number', sh_set($instance, 'number'));
                $posttype = apply_filters('widget_posttype', sh_set($instance, 'posttype'));
                $posttype = (!empty($posttype) ) ? $posttype : 'dict_gallery';
                echo wp_kses_post($before_widget);
                echo wp_kses_post($before_title) . $title . ' <span>' . $sub_title . '</span>' . $after_title;
                $Posts = query_posts('orderby=comment_count&order=DESC&post_type=' . $posttype . '&posts_per_page=' . $number);
                if (have_posts()): while (have_posts()): the_post();
                $PostTitle = get_the_title();
                $Settings = get_post_meta(get_the_ID(), '_dict_gallery_settings', true);
                $Records = '';
                if (!empty($Settings)) {
                    $GalleryAttachments = get_posts(array('post_type' => 'attachment', 'post__in' => explode(',', sh_set($Settings, 'gallery'))));
                    $i = 1;
                    $opt = get_post_meta(get_the_ID(), '_dictate_gal_videos', true);
                    foreach ((array) sh_set($Settings, 'videos') as $new_a) {
                        $video_data = sh_grab_video($new_a, $opt);
                        if ($i == 1) {
                            $Records .= '<div class="sidebar-video"> 
                            <img src="' . sh_set($video_data, 'thumb') . '" style="width:270; height:203px;" alt="' . sh_set($video_data, 'title') . '" /> 
                            <h6>' . sh_character_limit(20, get_the_title()) . '</h6>
                            <span><a class="html5lightbox" href="' . $new_a . '" title="' . sh_set($video_data, 'title') . '"></a></span>
                        </div>';
                        $i++;
                    }
                }
            }
            endwhile;
            endif;
            wp_reset_query();
            ?>
            <?php echo wp_kses_post($Records); ?>
            <?php
            echo wp_kses_post($after_widget);
        }
        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['sub_title'] = strip_tags($new_instance['sub_title']);
            $instance['number'] = $new_instance['number'];
            $instance['posttype'] = $new_instance['posttype'];
            return $instance;
        }
        function form($instance) {
            $title = ($instance) ? esc_attr($instance['title']) : __('Popular', 'lifeline');
            $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Video', 'lifeline');
            $number = ($instance) ? esc_attr($instance['number']) : '';
            $posttype = ($instance) ? esc_attr($instance['posttype']) : __('dict_gallery', 'lifeline');
            $post_types = get_post_types('', 'names');
            $Options = '';
            foreach ($post_types as $post_type) {
                $Value = str_replace('dict', '', $post_type);
                $Value = str_replace('_', ' ', $Value);
                $SelectedType = ( $posttype == $post_type ) ? ' selected="selected"' : '';
                $Options .= '<option value="' . $post_type . '"' . $SelectedType . '>' . ucwords($Value) . '</option>';
            }
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
            </p>
            <p>    
                <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number Of Videos:', 'lifeline'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
            </p>
            <p>    
                <label for="<?php echo esc_attr($this->get_field_id('posttype')); ?>"><?php _e('Video From:', 'lifeline'); ?></label>
                <select class="widefat" id="<?php echo esc_attr($this->get_field_id('posttype')); ?>" name="<?php echo esc_attr($this->get_field_id('posttype')); ?>">
                    <?php echo wp_kses_post($Options); ?>
                </select>
            </p>
            <?php
        }
    }
//Donate Us Widget
    class SH_Donate_Us extends WP_Widget {
        function __construct() {
            parent::__construct(/* Base ID */'DonateUs', /* Name */ __('Donate Us', 'lifeline'), array('description' => __('This widgtes is used to add Donate Us to sidebar.', 'lifeline')));
        }
        function widget($args, $instance) {
            extract($args);
            $title = apply_filters('widget_title', $instance['title']);
            $sub_title = apply_filters('widget_sub_title', $instance['sub_title']);
            $contactno = apply_filters('widget_contactno', $instance['contactno']);
            $currency = apply_filters('widget_currency', $instance['currency']);
            $collecteddonation = apply_filters('widget_collecteddonation', $instance['collecteddonation']);
            echo wp_kses_post($before_widget);
            echo wp_kses_post($before_title) . $title . ' <span>' . $sub_title . '</span>' . $after_title;
            ?>
            <div class="donate-us">
                <h3><?php _e('Give Your Donations', 'lifeline'); ?></h3>
                <span><i class="icon-phone"></i><?php echo esc_html($contactno); ?></span>
                <div class="collected">
                    <p><?php _e('Collected Donations', 'lifeline'); ?></p>
                    <span><strong><?php echo esc_html($currency); ?></strong> <?php echo sh_character_limit(6, $collecteddonation); ?>!</span> </div>
                    <div class="d-now"> <a data-toggle="modal" data-target="#myModal"  data-security="<?php echo wp_create_nonce(LIFELINE_KEY); ?>" data-url="<?php echo get_permalink() ?>" data-type="general" class="btn-don donate-btn" 
                     ><?php _e('Donate Now', 'lifeline'); ?></a> </div>
                </div>
                <?php
                echo wp_kses_post($after_widget);
            }
            function update($new_instance, $old_instance) {
                $instance = $old_instance;
                $instance['title'] = strip_tags($new_instance['title']);
                $instance['sub_title'] = strip_tags($new_instance['sub_title']);
                $instance['contactno'] = $new_instance['contactno'];
                $instance['currency'] = $new_instance['currency'];
                $instance['collecteddonation'] = $new_instance['collecteddonation'];
                return $instance;
            }
            function form($instance) {
                $title = ($instance) ? esc_attr($instance['title']) : __('Donate', 'lifeline');
                $sub_title = ($instance) ? esc_attr($instance['sub_title']) : __('Us', 'lifeline');
                $contactno = ($instance) ? esc_attr($instance['contactno']) : '';
                $currency = ($instance) ? esc_attr($instance['currency']) : '';
                $collecteddonation = ($instance) ? esc_attr($instance['collecteddonation']) : '';
                ?>
                <p>
                    <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
                </p>
                <p>
                    <label for="<?php echo esc_attr($this->get_field_id('sub_title')); ?>"><?php _e('Sub Title:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('sub_title')); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('contactno')); ?>"><?php _e('Contact Number:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('contactno')); ?>" name="<?php echo esc_attr($this->get_field_name('contactno')); ?>" type="text" value="<?php echo esc_attr($contactno); ?>" /> 
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('currency')); ?>"><?php _e('Currency:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('currency')); ?>" name="<?php echo esc_attr($this->get_field_name('currency')); ?>" type="text" value="<?php echo esc_attr($currency); ?>" /> 
                </p>
                <p>    
                    <label for="<?php echo esc_attr($this->get_field_id('collecteddonation')); ?>"><?php _e('Collected Donation:', 'lifeline'); ?></label>
                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('collecteddonation')); ?>" name="<?php echo esc_attr($this->get_field_name('collecteddonation')); ?>" type="text" value="<?php echo esc_attr($collecteddonation); ?>" /> 
                </p>
                <?php
            }
        }
        class sh_categories extends WP_Widget {
            public function __construct() {
                $widget_ops = array('classname' => 'sh_widget_categories', 'description' => __("This widgtes is used in sidebar to show Categoreis of custom post types.", 'lifeline'));
                parent::__construct('sh_categories', __('The Lifeline Categories', 'lifeline'), $widget_ops);
            }
            public function widget($args, $instance) {
                $c = !empty($instance['count']) ? '1' : '0';
                $title = isset($instance['title']) ? $instance['title'] : '';
                $crop = explode(' ', $title, 2);
                echo wp_kses_post($args['before_widget']);
                $cat_args = array(
                    'orderby' => 'name',
                    'show_count' => $c,
                    'order' => 'ASC',
                    'taxonomy' => $instance['cat'],
                    );
                    ?>
                    <div class="sidebar-title">
                        <h4><?php echo sh_set($crop, '0') ?> <span><?php echo sh_set($crop, '1') ?></span></h4>
                    </div>
                    <ul class="sidebar-list">
                        <?php
                        $cat_args['title_li'] = '';
            //printr($cat_args);
                        wp_list_categories(apply_filters('widget_categories_args', $cat_args));
                        ?>
                    </ul>
                    <?php
                    echo wp_kses_post($args['after_widget']);
                }
                public function update($new_instance, $old_instance) {
                    $instance = $old_instance;
                    $instance['title'] = strip_tags($new_instance['title']);
                    $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
                    $instance['cat'] = strip_tags($new_instance['cat']);
                    return $instance;
                }
                public function form($instance) {
                    $instance = wp_parse_args((array) $instance, array('title' => ''));
                    $title = isset($instance['title']) ? $instance['title'] : '';
                    $count = isset($instance['count']) ? (bool) $instance['count'] : false;
                    $cat = isset($instance['cat']) ? $instance['cat'] : '';
                    $opt = array('testimonial_category' => 'Testimonials', 'project_category' => 'Project', 'portfolio_category' => 'portfolio', 'team_category' => 'Team', 'gallery_category' => 'Gallery', 'event_category' => 'Event');
                    ?>
                    <p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'lifeline'); ?></label>
                        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
                        <p>
                            <input type="checkbox" class="checkbox" id="<?php echo esc_attr($this->get_field_id('count')); ?>" name="<?php echo esc_attr($this->get_field_name('count')); ?>"<?php checked($count); ?> />
                            <label for="<?php echo esc_attr($this->get_field_id('count')); ?>"><?php _e('Show post counts', 'lifeline'); ?></label><br />
                            <p>
                                <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php _e('Select Post Type:', 'lifeline'); ?></label>
                                <select class="widefat" id="<?php echo esc_attr($this->get_field_id('cat')); ?>" name="<?php echo esc_attr($this->get_field_name('cat')); ?>" >
                                    <?php
                                    foreach ($opt as $k => $op) :
                                        $selected = ( $cat == $k ) ? 'selected="selected"' : '';
                                    echo '<option value="' . $k . '" ' . $selected . '>' . $op . '</option>';
                                    endforeach;
                                    ?>
                                </select>
                            </p>
                            <?php
                        }
                    }
class sh_instagram_Widget extends WP_Widget {
	function __construct() {
		parent::__construct(/* Base ID */'instagram', /* Name */ esc_html__('Instagram Feeds', 'lifeline'), array('description' => esc_html__('This widgtes is used to show instagram feeds.', 'lifeline')));
	}
	function widget($args, $instance) {
		extract($args);
		echo wp_kses($before_widget, true);
		$title = apply_filters('widget_title', sh_set($instance, 'title'));
		$number = apply_filters('widget_number', sh_set($instance, 'number'));
		if ($title)
			echo wp_kses($before_title, true) . $title . wp_kses($after_title, true);
		?>
		<?php if(function_exists('sh_instagram_feed') && sh_set($instance, 'access_token') && sh_set($instance, 'instagram_id') && class_exists('Instagram')):
			$counter = 0;
			$counter2 = 0;
			/*   exit('asd');*/
			$instagram = new Instagram( sh_set( $instance, 'instagram_id' ) );
			//printr($number);
			$instagram->setAccessToken( sh_set( $instance, 'access_token' ) );
			$result = $instagram->getUserMedia( 'self', sh_set( $instance, 'number' ) );
			if ( false === ( $images = get_transient( 'sh_instagram' ) ) ) {
				$images = $result->data;
				set_transient( 'sh_instagram', $images, 60 * 60 * 24 );
			}
			if ( !empty( $images ) ):
				?>
                <div class="insta-photos">
					<?php foreach($images as $ins_img):?>
						<?php if($counter==$number) break; ?>
						<?php
						$thumbnail = sh_set( sh_set( $ins_img, 'images' ), 'thumbnail' );
						$full_path = sh_set( sh_set( $ins_img, 'images' ), 'standard_resolution' );
						?>
                        <a href="<?php echo esc_url(sh_set($full_path, 'url'));?>"><img width="79" height="79" src="<?php echo esc_url(sh_set($thumbnail, 'url'));?>" alt="feed" /></a>
						<?php $counter++;  ?>
					<?php endforeach;?>
                </div>
				<?php
			endif;
		endif;
		echo wp_kses($after_widget, true);
	}
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = sh_set($new_instance, 'title');
		$instance['number'] = sh_set($new_instance, 'number');
		$instance['access_token'] = sh_set($new_instance, 'access_token');
		$instance['instagram_id'] = sh_set($new_instance, 'instagram_id');
		return $instance;
	}
	function form($instance) {
		$title = ($instance) ? esc_attr(sh_set($instance, 'title')) : '';
		$number = ($instance) ? esc_attr(sh_set($instance, 'number')) : 12;
		$instagram_id = ($instance) ? esc_attr(sh_set($instance, 'instagram_id')) : '';
		$access_token = ($instance) ? esc_attr(sh_set($instance, 'access_token')) : '';
		?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Feeds Number:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('instagram_id')); ?>"><?php esc_html_e('Instagram User ID:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('instagram_id')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram_id')); ?>" type="text" value="<?php echo esc_attr($instagram_id); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('access_token')); ?>"><?php esc_html_e('Instagram Access Token:', 'lifeline'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('access_token')); ?>" name="<?php echo esc_attr($this->get_field_name('access_token')); ?>" type="text" value="<?php echo esc_attr($access_token); ?>" />
        </p>
        <p><strong><?php echo sprintf( esc_html__( 'To get your instagram access token %s', 'lifeline' ), "<a href='http://instagram.pixelunion.net/' target='_blank'>Click Here</a>");?>:</strong></p>
		<?php
	}
}