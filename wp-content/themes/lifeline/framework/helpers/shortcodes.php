<?php

class SH_Shortcodes {

    protected $keys;
    protected $toggle_count = 0;

    function __construct() {
        $GLOBALS['sh_toggle_count'] = 0;
        add_action('init', array($this, 'add'), 1000);
    }

    function add() {
        if ( function_exists( 'vc_map' ) )
            get_template_part( 'framework/vc_map' );

        include(SH_FRW_DIR . 'resource/shortcodes.php');
        $this->keys = array_keys($options);
        foreach ($this->keys as $k) {
            if (method_exists($this, $k) && function_exists('lifeline_custom_shortcode_setup'))
                lifeline_custom_shortcode_setup('sh_' . $k, array($this, $k));
        }
    }

    function recent_news($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => __('Recent News', 'lifeline'),
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'DESC',
            'c_opt' => 'excerpt',
            'c_limit' => '100',
            'title_charcacter_limit'=>'20',
            'heading_style' => 'simple',
                        ), $atts)
        );
        static $counter = 1;
        $News = $Thumb = '';
        wp_reset_query();
        $args = array('post_type' => 'post', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        //if($category != '') $args['category'] = array($category);
//        if ($cat != 'all') {
//            $args = array('post_type' => 'post', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order, 'cat' => (int) $cat);
//        }

	    if ($cat != 'all') {
	    	$cat = get_category_by_slug( $cat );
	    	$args = array('post_type' => 'post', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order, 'cat' => $cat->term_taxonomy_id);
	    }
        //printr($args);
        $Posts = query_posts($args);
        //printr($Posts);
        $query = new WP_Query($args);
        //printr($query);
        $i = 1;
        if ($query->have_posts()): while ($query->have_posts()): $query->the_post();
                $Settings = get_post_meta(get_the_ID(), '_post_settings', true);
                if ($video_link = sh_set($Settings, 'video_link')) {
                    $opt = get_post_meta(get_the_ID(), '_dictate_gal_videos', true);
                    $video_data = sh_grab_video($video_link, $opt);
                    $PostMedia = '<div class="image"> 
                              <img src="' . sh_set($video_data, 'thumb') . '" style="width:261px; height:207px;" alt="' . sh_set($video_data, 'title') . '" /> 
                              <a class="html5lightbox" rel="prettyPhoto" href="' . $video_link . '" title="' . sh_set($video_data, 'title') . '"> 
                                  <span><i class="icon-play"></i></span> 
                              </a> 
                          </div>';
                    $CarouselThumb = '<img src="' . sh_set($video_data, 'thumb') . '" style="width:131px; height:78px;" alt="' . sh_set($video_data, 'title') . '" />';
                } else {
                    $PostAttchment = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large');
                    $PostMedia = '<div class="image">
                              ' . get_the_post_thumbnail(get_the_ID(), '370x252') . '
                              <a   href="' . sh_set($PostAttchment, 0) . '" rel="prettyPhoto" class="html5lightbox"><i class="icon-picture"></i></a>
                          </div>';
                    $CarouselThumb = get_the_post_thumbnail(get_the_ID(), '270x155');
                }
                $News .= '<div id="news' . $i .'-'.$counter.'">
                    <div class="row">
                        <div class="col-md-6 desc">
                          <h3><a href="' . get_permalink() . '"  >' . sh_character_limit(35, get_the_title()) . '</a></h3>';
                $News .= '<p>';
                if ($c_opt == 'excerpt') {
                    $News .= get_the_excerpt();
                } elseif ($c_opt == 'full') {
                    $News .= get_the_content(get_the_ID());
                } elseif ($c_opt == 'limit') {
                    $News .= sh_contents(get_the_content(get_the_ID()), $c_limit);
                }
                $News .="</p>";
                $News .='<a href="' . get_the_permalink() . '"  >' . __('read more', 'lifeline') . '</a>';

                $News .= '</div>
                        <div class="col-md-6">
                            ' . $PostMedia . '
                        </div>
                    </div>
                  </div>';
                $Thumb .= '<a href="#news' . $i.'-'.$counter.'">' . $CarouselThumb . '<span class="carusal-our-news">' . sh_character_limit($title_charcacter_limit, get_the_title()) . '</span></a>';
                $i++;
            endwhile;
        endif;
        wp_reset_query();
        wp_enqueue_script('carofredcsel');
        $output = '';


        $output .= '<div class="carusal-slider">
                      <div id="carousel-wrapper">
                        <div id="carousel-'.$counter.'" class="carousel">
                          ' . $News . ' 
                        </div>
                      </div>
                      <div id="thumbs-wrapper">
                        <div  class="thumbs-'.$counter.'" id="thumbs">' . $Thumb . '</div>
                        <a class="prev" id="prev" href="#"><i class="icon-angle-left"></i></a> <a class="next" id="next" href="#"><i class="icon-angle-right"></i></a> </div>
                    </div>
                    <script>
                    jQuery(document).ready(function($){
                         $(\'.carousel\').carouFredSel({
                          responsive: true,
                          circular: false,
                          auto: false,
                          items: {
                           visible: 1,
                           width: 20,
                          },
                          scroll: {
                           fx: \'directscroll\'
                          }
                         });
                         $(".thumbs-'.$counter.'").carouFredSel({
                          responsive: true,
                          circular: false,
                          infinite: false,
                          auto: false,
                          prev: \'.prev\',
                          next: \'.next\',
                          items: {
                           visible: {
                            min: 1,
                            max: 6
                           },
                           width: 200,
                           height: \'80%\'
                          }
                         });
                         $(".thumbs-'.$counter.' a").click(function() {
                          $("#carousel-'.$counter.'").trigger(\'slideTo\', \'#\' + this.href.split(\'#\').pop() );
                          $(".thumbs-'.$counter.' a").removeClass(\'selected\');
                          $(this).addClass(\'selected\');
                          return false;
                         });
                    });
                    </script>
                    ';
        $counter++;
        return $output;
    }

    function our_causes($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => __('Our Causes', 'lifeline'),
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'donate_sec' => 'true',
            'c_opt' => 'excerpt',
            'c_limit' => '100',
            'heading_style' => 'simple',
            'don_sect_title' => '',
            'needed_label' => '',
            'collected_label' => '',
            'columns_grid'=>'',
                        ), $atts)
        );

        $paypal_res = '';
        $single_page = (sh_set($_SESSION, 'sh_causes_page')) ? sh_set($_SESSION, 'sh_causes_page') : false;
        if ($single_page == true)
            unset($_SESSION['sh_causes_page']);
        if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
            if(defined('CPATH'))
                $paypal_res = require_once(CPATH. 'includes/pp_recurring/order_confirm.php');
        }
        $return_url = (is_home()) ? home_url() : get_permalink();

        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != 'all') {

            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
        //printr($cat);
        $Posts = query_posts($args);

        $chunk_num = ($columns_grid == '4') ? 3 : 4;
        $col_class_outer = ($donate_sec == 'true') ? 'col-md-9' : 'col-md-12';
        $col_class_inner = 'col-md-'.$columns_grid;

        $chunk = array_chunk($Posts, $chunk_num);

        $output = '';

        $output .= '<div class="row"><div class="' . $col_class_outer . '">';
        $output .= '<div class="our-causes">
                        
                          <ul class="slides">';

        foreach ($chunk as $p) {
            $output .= '<li><div class="row">';
            foreach ($p as $pos) {
                $Settings = get_post_meta(sh_set($pos, 'ID'), '_dict_causes_settings', true);
                $output .= '<div class="' . $col_class_inner . '">
                                <div class="causes-image">'
                        . get_the_post_thumbnail(sh_set($pos, 'ID'), '370x491') . '
                                    <div class="cause-heading">
                                        <h3>' . sh_excerpt(sh_set($pos, 'post_title'), 27) . '</h3>
                                        <p>' . __("in", 'lifeline') . ' ' . sh_set($Settings, 'location') . '</p>
                                    </div>
                                    <a href="' . get_permalink(sh_set($pos, 'ID')) . '" title="' . sh_set($pos, 'post_title') . '">
                                        <div class="our-causes-hover">
                                          
                                          <h3>' . sh_excerpt(sh_set($pos, 'post_title'), 27) . '</h3>
                                          <span>' . __('in', 'lifeline') . ' <i>' . sh_set($Settings, 'location') . '</i></span>';
                $output .= '<p>';
                if ($c_opt == 'excerpt') {
                    $output .= sh_excerpt($pos, 127);
                } elseif ($c_opt == 'full') {
                    $output .= $pos->post_content;
                } elseif ($c_opt == 'limit') {
                    $output .= sh_excerpt($pos, $c_limit);
                }
                $output .="</p>";
                $output .='
                                          <span class="help"><strong>' . __('Help us', 'lifeline') . '</strong> ' . __('to collect', 'lifeline') . ':</span> <span class="needed-amount"><span>' . sh_set($Settings, 'currency_symbol') . '</span>' . sh_set($Settings, 'donation_needed') . ' </span> 
                                        </div>
                                    </a>
                                </div>
                            </div>';
            }
            $output .= '</div></li>
            <script>
                jQuery(document).ready(function($){
                    if( $(\'.our-causes\').length ){
                $(\'.our-causes\').flexslider({
                    animation: "slide",
                    animationLoop: false,
                    controlNav: true,   
                    maxItems: 1,
                    pausePlay: false,
                    mousewheel:false,
                    start: function(slider){
                    $(\'body\').removeClass(\'loading\');
                    }
                });
            }
        });
        </script>
                ';
        }
        wp_reset_query();
        $paypal = $GLOBALS['_sh_base']->donation;
        if ($notif = $paypal->_paypal->handleNotification())
            $paypal_res = $paypal->single_pament_result($notif);
        $output .= '</ul></div></div></div>';
        $donation_data = get_option('lifeline');
        $percent = (sh_set($donation_data, 'paypal_target')) ? (int) str_replace(',', '', sh_set($donation_data, 'paypal_raised')) / (int) str_replace(',', '', sh_set($donation_data, 'paypal_target')) : 0;
        $donation_percentage = $percent * 100;
        $symbol = (sh_set($donation_data, 'paypal_currency')) ? sh_set($donation_data, 'paypal_currency') : '$';
        $donation = '<div class="col-md-3">';
        $donation .= '<div class="donate-us-box">
                       <h5>' . $don_sect_title . '</h5>
                       <span>' . $needed_label . '</span> <span class="amount-figures"><strong>' . $symbol . '</strong> ' . sh_set($donation_data, 'paypal_target') . '!</span> <span>' . $collected_label . '</span> <span class="amount-figures coloured"><strong>' . $symbol . '</strong> ' . sh_set($donation_data, 'paypal_raised') . '!</span> 
                       <span class="cell"><i class="icon-phone"></i>' . sh_set($donation_data, 'paypal_contact') . '</span>';
        if (sh_set($donation_data, 'donate_method') == 'true') {
            $donation .= '<a   data-toggle="modal" data-url="' . get_permalink() . '" data-type="general" data-target="#myModal"  class="btn-don donate-btn"  data-security="'.wp_create_nonce(LIFELINE_KEY) .'"  >'.esc_html__('Donate Us', 'lifeline').'</a>';
        }
        $donation .= '</div>
                   </div></div>';
        $output .= ($donate_sec == 'true') ? $donation : '';

        return $output;
    }

    function our_causes_2($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => 'Our Causes',
            'bg' => '',
            'blackish' => '',
            'cat' => '',
            'sort_by' => 'date',
            'heading_style' => 'simple',
            'sorting_order' => 'ASC'), $atts)
        );
        wp_enqueue_script('layersliderscript1');
        wp_enqueue_script('layersliderscript2');

        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != '' && $cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $cat));
            }
        }

        $black_layer = ($blackish) ? 'blackish' : '';
        $Posts = query_posts($args);
        $attach = ($bg) ? wp_get_attachment_image_src($bg, 'large') : '';
        $bgimg = ( $attach ) ? sh_set($attach, 0) : get_template_directory_uri() . '/images/moving-bg.png';
        //$output = '<div class="parallax"><div class="fixed-bg ' . $black_layer . '" style="background:url(' . $bgimg . ') no-repeat scroll 0 0 transparent; background-size:cover;"></div>
        $output = '<div class="row"><div class="col-md-12">';

//      $output .= ($heading_style == 'underline') ? '<div class="sec-heading">
//                  ' . sh_get_title( $title, 'h2', 'strong', TRUE ) . '
//                </div>' : '';
//      $output .= ($heading_style == 'modern') ? '<div class="sec-title">
//                          ' . sh_get_title( $title, 'h1', 'span', FALSE ) . '
//                      </div>' : '';
//      $output .= ($heading_style == 'simple') ? '<div class="sec-heading3">
//                  ' . sh_get_title( $title, 'h2', 'strong', TRUE ) . '
//                </div>' : '';

        $output .='
                    <div class="posts-carousel">
                        <ul class="slides">';
        $chunk = array_chunk($Posts, 4);
        foreach ($chunk as $p) {
            $output .= '<li><div class="row">';
            foreach ($p as $pos) {
                $Settings = get_post_meta(sh_set($pos, 'ID'), '_dict_causes_settings', true);
                if (sh_set($Settings, 'video_link') && sh_set($Settings, 'video_link') != '') {
                    $video_link = sh_set($Settings, 'video_link');
                    $video_data = sh_grab_video($video_link, $Settings);
                    $PostMedia = '<div class="carou-post-img">
                                      <img src="' . sh_set($video_data, 'thumb') . '" style="width:277px; height:190px;" alt="' . sh_set($video_data, 'title') . '" />
                                      <a title="' . sh_set($video_data, 'title') . '" href="' . $video_link . '" class="html5lightbox"><i class="icon-play"></i></a>
                                  </div>';
                } else if (sh_set($Settings, 'gallery')) {
                    $GalleryAttachments = get_posts(array('post_type' => 'attachment', 'post__in' => explode(',', sh_set($Settings, 'gallery'))));
                    $Slides = '';
                    foreach ($GalleryAttachments as $Attachment) {
                        $Thumb = sh_set(wp_get_attachment_image_src($Attachment->ID, '370x252'), '0');
                        $LargeImage = sh_set(wp_get_attachment_image_src($Attachment->ID, 'large'), '0');
                        $Slides .= '<li data-masterspeed="500" data-slotamount="7" data-transition="curtain-1">
                        <img data-bgrepeat="no-repeat" data-bgposition="left top" data-bgfit="cover" alt="slidebg1" src="' . $Thumb . '" draggable="false"> </li>';
                        //$i++;
                    }

                    $PostMedia = '<div class="carou-post-img"><div class="tp-banner-causes">
                                        <ul>
                                        ' . $Slides . '
                                     </ul>
                                    </div></div>';
//                    $PostMedia .= '<script type="text/javascript">
//                                    jQuery(document).ready(function(){
//                                        
//                                        jQuery(".tp-banner-causes ul").revolution(
//                                                {
//                                                    delay: 15000,
//                                                    startwidth: 270,
//                                                    startheight: 184,
//                                                    autoHeight: "on",
//                                                    navigationType: "none",
//                                                    navigation: {
//                                                        arrows:{enable:true}              
//                                                    },            
//                                                    hideThumbs: 10,
//                                                    fullWidth: "off",
//                                                    fullScreen: "off",
//                                                    fullScreenOffsetContainer: ""
//                                                });
//                                       
//                                    });
//                            </script>';
                } else {
                    $PostAttchment = wp_get_attachment_image_src(get_post_thumbnail_id(sh_set($pos, 'ID')), 'large');
                    $PostMedia = '<div class="carou-post-img">
                                      ' . get_the_post_thumbnail(sh_set($pos, 'ID'), '370x252') . '
                                      <a   href="' . sh_set($PostAttchment, 0) . '" class="html5lightbox"><i class="icon-picture"></i></a>
                                  </div>';
                }
                $output .= '<div class="col-md-3">
                                <div class="carou-post">
                                   ' . $PostMedia . '
                                   <h4>' . substr(strip_tags(sh_set($pos, 'post_title')), 0, 30) . '</h4>
                                   <p>' . substr(strip_tags(sh_set($pos, 'post_content')), 0, 200) . '</p>
                                   <a href="' . get_permalink(sh_set($pos, 'ID')) . '"  >' . __('Read More', 'lifeline') . '</a> 
                               </div>
                           </div>';
            }
            $output .= '</div></li>';
        }
        $output .= '    </ul>
                      </div>
                      </div>
                      </div>
                      <script>
                      jQuery(document).ready(function($){
                      if( $(".posts-carousel").length > 0 ){
                            $(".posts-carousel").flexslider({
                                animation: "slide",
                                animationLoop: false,
                                controlNav: false,  
                                maxItems: 1,
                                pausePlay: false,
                                mousewheel:false,
                                start: function(slider){
                                  $("body").removeClass("loading");
                                }
                            });
                        }
                      });
                      </script>
                      ';
        wp_reset_query();
        return $output;
    }

    function our_causes_3($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => 'Our Causes',
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'c_opt' => 'excerpt',
            'c_limit' => '100',
            'heading_style' => 'simple',
                        ), $atts)
        );
        wp_enqueue_script(array('bootstrap'));

        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
        $Posts = query_posts($args);
        $i = 1;
        $Cause = '';
        $PostNav = '';
        if (have_posts()): while (have_posts()): the_post();
                $PostClass = ($i == 1) ? 'tab-pane fade in active' : 'tab-pane fade';
                $NavClass = ($i == 1) ? 'active' : '';
                $Settings = get_post_meta(get_the_ID(), '_dict_causes_settings', true);
                $Cause .= '<div id="tab' . $i . '" class="' . $PostClass . '">

                                <div class="cause-image"> 
                                    ' . get_the_post_thumbnail(get_the_ID(), '1170x312') . '
                                    <div class="meta"> <span>' . __('In', 'lifeline') . ' <i>' . sh_set($Settings, 'location') . '</i></span> </div>
                                    <div class="cause-title">
                                        <h2><a href="' . get_permalink() . '">' . sh_excerpt(get_the_title(), 100) . '</a></h2>
                                    </div>
                                </div>
                                <div class="details">
                                    <div class="needed-amount">
                                        <h5><i>' . sh_set($Settings, 'currency_symbol') . '</i>' . sh_set($Settings, 'donation_needed') . '<span>' . __('Donation Needed', 'lifeline') . '</span></h5>
                                    </div>';
                $Cause .= '<p>';
                if ($c_opt == 'excerpt') {
                    $Cause .= get_the_excerpt();
                } elseif ($c_opt == 'full') {
                    $Cause .= get_the_content(get_the_ID());
                } elseif ($c_opt == 'limit') {
                    $Cause .= sh_contents(get_the_content(get_the_ID()), $c_limit);
                }
                $Cause .="</p>";
                $Cause .=' </div>
                            </div>';
               //$PostNav = '<li class="col-md-4"> <a data-toggle="tab" href="#tab' . $i . '"> ' . get_the_post_thumbnail(get_the_ID(), '1170x455') . ' <span>' . sh_excerpt(get_the_title(), 35) . '</span> </a> </li>';
                $i++;
            endwhile;
        endif;
        wp_reset_query();
        $nav = '';
        if ($PostNav) {
            foreach (array_chunk($PostNav, 3) as $chunk) {
                $nav .= '<li>
                            <ul class="nav nav-tabs cause-tabber">';
                if (is_array($chunk)) {
                    foreach ($chunk as $ch) {
                        $nav .= $ch;
                    }
                }
                $nav .= '</ul>
                        </li>';
            }
        }
        $output = '';

        $output .= '<div class="our-cause-sec">';

        $output .= '<div class="tab-content" id="myTabContent">' . $Cause . '</div>
                        
                            <div class="causes-carousel">
                                <ul class="slides">
                                    ' . $nav . '
                                </ul>
                            </div>
                        
                    </div>
                    <script>
                    jQuery(document).ready(function($){
                    if( $(\'.causes-carousel\').length > 0 )
                        {
                        $(\'.causes-carousel\').flexslider({
                            animation: "slide",
                            animationLoop: false,
                            controlNav: false,  
                            pausePlay: false,
                            mousewheel:false,
                            start: function(slider){
                              $(\'body\').removeClass(\'loading\');
                            }
                        });
                        }
                    });
                    </script>
                    ';

        return $output;
    }

    function our_causes_4($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => 'Our Causes',
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'c_opt' => 'excerpt',
            'c_limit' => '100',
            'heading_style' => 'simple',
                        ), $atts)
        );

        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
        $Posts = query_posts($args);
        //printr($Posts);
        $TotalPosts = count($Posts);
        $PostBatch = 2;
        $EndingTagAppended = FALSE;
        $StrtingTag = '<div class="col-md-4">';
        $CloseTag = '</div>';
        $i = 1;
        $Cause = '';
        $NextBatch = TRUE;
        if (have_posts()): while (have_posts()): the_post();
                $NewSecStrtTag = ($TotalPosts < $PostBatch || $i == 1 || $EndingTagAppended === TRUE) ? $StrtingTag : '';
                if (!empty($NewSecStrtTag) && $NextBatch === TRUE) {
                    $ImageSize = '370x491';
                    $NextBatch = FALSE;
                    $AlterStyle = TRUE;
                } else if (!empty($NewSecStrtTag) && $NextBatch === FALSE) {
                    $ImageSize = '370x252';
                    $NextBatch = TRUE;
                    $AlterStyle = FALSE;
                } else if (empty($NewSecStrtTag) && $AlterStyle === TRUE) {
                    $ImageSize = '370x252';
                    $AlterStyle = FALSE;
                } else if (empty($NewSecStrtTag) && $AlterStyle === FALSE) {
                    $ImageSize = '370x491';
                    $AlterStyle = TRUE;
                }
                $NewSecCloseTag = ($i == $TotalPosts || ($TotalPosts < $PostBatch && $i == $TotalPosts) || ($i % $PostBatch == 0 && $i !== 1)) ? $CloseTag : '';
                $EndingTagAppended = ($i == $TotalPosts || ($TotalPosts < $PostBatch && $i == $TotalPosts) || ($i % $PostBatch == 0 && $i !== 1)) ? TRUE : FALSE;
                $Cause .= $NewSecStrtTag .
                        '<div class="portfolio" data-cat="' . $i . '"> 
                      <a href="' . get_permalink() . '">' . get_the_post_thumbnail(get_the_ID(), $ImageSize) . '</a>
                      <div class="port-desc">
                        <h4><a href="' . get_permalink() . '" title="' . get_the_title() . '">' . sh_excerpt(get_the_title(), 40) . '</a></h4>';
                $Cause .= '<p>';
                if ($c_opt == 'excerpt') {
                    $Cause .= get_the_excerpt();
                } elseif ($c_opt == 'full') {
                    $Cause .= get_the_content(get_the_ID());
                } elseif ($c_opt == 'limit') {
                    $Cause .= sh_contents(get_the_content(get_the_ID()), $c_limit);
                }
                $Cause .="</p>";
                $Cause .= '</div>
                   </div>'
                        . $NewSecCloseTag;
                $i++;
            endwhile;
        endif;
        wp_reset_query();
        $output = '';
        $output .= '
                    <div id="Grid">
                      <div class="row">
                        ' . $Cause . '
                      </div>
                    </div>';

        return $output;
    }

    function donation($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Donate Us',
            'heading_style' => 'simple',
            'don_sect_title' => '',
            'needed_label' => '',
            'collected_label' => '',
                        ), $atts));
        $paypal = $GLOBALS['_sh_base']->donation;

        $donation_data = get_option('lifeline');
        $output = '';
        $output .= '<div class="pull-right">';

        $output .= '<div class="donate-us-box">
                      <h5>' . $don_sect_title . '</h5>
                      <span>' . $needed_label . '</span> <span class="amount-figures"><strong>' . sh_set($donation_data, 'paypal_currency') . '</strong> 
                      ' . sh_set($donation_data, 'paypal_target') . '!</span> <span>' . $collected_label . '</span> 
                      <span class="amount-figures coloured"><strong>' . sh_set($donation_data, 'paypal_currency') . '</strong> 
                      ' . sh_set($donation_data, 'paypal_raised') . '!</span> <span class="cell"><i class="icon-phone"></i>
                      ' . sh_set($donation_data, 'paypal_contact') . '</span> 
                    <a  data-toggle="modal" data-target="#myModal" data-url="' . get_permalink() . '" data-type="general" class="btn-don donate-btn"  data-security="'.wp_create_nonce(LIFELINE_KEY) .'"  >' . __('Donate Us', 'lifeline') . '</a>
                    </div>
              </div>';
        return $output;
    }

    function donation_2($atts, $content = null) {
        extract(shortcode_atts(array(
	        'needed_label' => '',
	        'collected_label' => '',
                        ), $atts));
        $paypal = $GLOBALS['_sh_base']->donation;
        $paypal_res = '';
        $single_page = (sh_set($_SESSION, 'sh_causes_page')) ? sh_set($_SESSION, 'sh_causes_page') : false;
        if ($single_page == true)
            unset($_SESSION['sh_causes_page']);
        if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
            if(defined('CPATH'))
                $paypal_res = require_once(CPATH. 'includes/pp_recurring/order_confirm.php');
        }

        $donation_data = get_option('lifeline');
        if ($notif = $paypal->_paypal->handleNotification())
            $paypal_res = $paypal->single_pament_result($notif);
        $return_url = (is_home()) ? home_url() : get_permalink();
        $percent = (sh_set($donation_data, 'paypal_target')) ? (int) str_replace(',', '', sh_set($donation_data, 'paypal_raised')) / (int) str_replace(',', '', sh_set($donation_data, 'paypal_target')) : 0;
        $donation_percentage = $percent * 100;
        $symbol = (sh_set($donation_data, 'paypal_currency')) ? sh_set($donation_data, 'paypal_currency') : '$';
        $output = '';

        $output .= '<div class="donation-bar">
                        <div class="amount pull-left">
                          <p>' . $needed_label . '</p>
                          <span><strong>' . sh_set($donation_data, 'paypal_currency') . '</strong> ' . sh_set($donation_data, 'paypal_target') . '!</span> </div>
                    <div class="donate-now">';
        if (sh_set($donation_data, 'donate_method') == 'true') {
            $output .= '<a  data-toggle="modal" data-target="#myModal" data-url="' . get_permalink() . '" data-type="general" data-security="'. wp_create_nonce(LIFELINE_KEY) .'"  class="btn-don donate-btn"  >' . __('Donate Us', 'lifeline') . '</a>';
        } else {
            $output .= $paypal->button(array('currency_code' => sh_set($donation_data, 'paypal_currency_code'), 'item_name' => get_bloginfo('name'), 'return' => $return_url));
        }
        $output .= '</div>
                      <div class="amount pull-right">
                        <p>' . $collected_label . '</p>
                        <span><strong>' . sh_set($donation_data, 'paypal_currency') . '</strong> ' . sh_set($donation_data, 'paypal_raised') . '!</span> 
                      </div>
                   </div>';

        $output .= '<div class="modal fade" id="_myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';

        $output .= '<div class="donate-popup">
        <div class="cause-bar">
            <div class="cause-box"><h3><span>' . $symbol . '</span>' . sh_set($donation_data, 'paypal_target') . '</h3><i>' . __('NEEDED DONATION', 'lifeline') . '</i></div>
            <div class="cause-progress">
                    <div class="progress-report">
                    <h6>' . __('PHASES', 'lifeline') . '</h6>
                    <span>' . $donation_percentage . '%</span>
                    <div class="progress pattern">
                        <div class="progress-bar" style="width: ' . $donation_percentage . '%"></div>
                    </div>
                </div>
            </div>
            <div class="cause-box"><h3><span>' . $symbol . '</span>' . sh_set($donation_data, 'paypal_raised') . '</h3><i>' . __('COLLECTED DONATION', 'lifeline') . '</i></div>
            <div class="cause-box donate-drop-btn"><h4>' . __('DONATE NOW', 'lifeline') . '</h4></div>
        </div>
        <div class="donate-drop-down">
            <div class="recursive-periods">';
        $Settings = get_option('lifeline');
        $value = sh_set($Settings, 'transactions_detail');
        if ($value) {
            foreach ($value as $val) {
                $txt = ucwords(str_replace('_', ' ', $val));
                $output .= '<a style="cursor:pointer;">' . $txt . '</a>';
            }
        }
        $output .='</div>
            <div class="amount-btns">';
        if (intval(sh_set($Settings, 'pop_up_1st_value')) != '')
            $output .= '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_1st_value') . '</span></a>';
        if (intval(sh_set($Settings, 'pop_up_2nd_value')) != '')
            $output .= '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_2nd_value') . '</span></a>';
        if (intval(sh_set($Settings, 'pop_up_3rd_value')) != '')
            $output .= '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_3rd_value') . '</span></a>';
        if (intval(sh_set($Settings, 'pop_up_4th_value')) != '')
            $output .= '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_4th_value') . '</span></a>';
        if (intval(sh_set($Settings, 'pop_up_5th_value')) != '')
            $output .= '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_5th_value') . '</span></a>';
        $output .= '</div>';
        $output .= '<div class="other-amount">
                    ' . $paypal->button(array('item_name' => get_bloginfo('name'), 'amount' => 30, 'return' => $return_url)) . '
                </div>';
        $output .= '</div>
                </div>';
        $output .= '</div>';
        return $output;
    }

    function donation_3($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
             'btn_label' =>'',
                        ), $atts));
        $paypal_res = '';
        $single_page = (sh_set($_SESSION, 'sh_causes_page')) ? sh_set($_SESSION, 'sh_causes_page') : false;
        if ($single_page == true)
            unset($_SESSION['sh_causes_page']);
        if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
            if(defined('CPATH'))
                $paypal_res = require_once(CPATH. 'includes/pp_recurring/order_confirm.php');
        }
        $paypal = $GLOBALS['_sh_base']->donation;
        if ($notif = $paypal->_paypal->handleNotification())
            $paypal_res = $paypal->single_pament_result($notif);
        $return_url = (is_home()) ? home_url() : get_permalink();

        $donation_data = get_option('lifeline');
        $percent = (sh_set($donation_data, 'paypal_target')) ? (int) str_replace(',', '', sh_set($donation_data, 'paypal_raised')) / (int) str_replace(',', '', sh_set($donation_data, 'paypal_target')) : 0;
        $donation_percentage = $percent * 100;
        $symbol = (sh_set($donation_data, 'paypal_currency')) ? sh_set($donation_data, 'paypal_currency') : '$';
        $output = '';

        $output .= '<div class="donate-us">
                      <h3>' . sh_character_limit(20, $title) . '</h3>
                      <span><i class="icon-phone"></i>' . sh_set($donation_data, 'paypal_contact') . '</span>
                      <div class="collected">
                        <p>' . __('Collected Dontaions', 'lifeline') . '</p>
                        <span><strong>' . sh_set($donation_data, 'paypal_currency') . '</strong> ' . sh_set($donation_data, 'paypal_raised') . '!</span> 
                      </div>
                      <div class="d-now">';
        if (sh_set($donation_data, 'donate_method') == 'true') {
            $button_label = $btn_label ? $btn_label : __('Donate Us', 'lifeline');
            $output .= '<a  data-toggle="modal" data-target="#myModal" data-url="' . get_permalink() . '" data-type="general" class="btn-don donate-btn"     data-security="'.wp_create_nonce(LIFELINE_KEY) .'">' . $button_label . '</a>';
        } else {
            $output .= $paypal->button(array('currency_code' => sh_set($donation_data, 'paypal_currency_code'), 'item_name' => get_bloginfo('name'), 'return' => $return_url));
        }
        $output .= '</div>';
        $output .= '</div>';
        return $output;
    }

    function start_regular_donation($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'sub_title' => '',
            'image' => '',
            'currency' => '$',
            'donation_needed' => '',
            'link_caption' => '',
            'button_cap_url' => '',
            'text' => '',
            'heading_style' => 'simple',
                        ), $atts));

        $title = sh_character_limit(25, $title);
        $SubTitle = (!empty($sub_title)) ? '<h5>' . sh_character_limit(26, $sub_title) . '</h5>' : '';
        $DonationNeeded = (!empty($donation_needed) || !empty($currency)) ? '<span>' . $currency . $donation_needed . ' </span>' : '';
        $output = '';


        $output .= '
                      <div class="donate-message">
                        ' . wp_get_attachment_image($image, '270x155') . '
                        ' . $SubTitle . '
                        <p>' . $DonationNeeded . sh_character_limit(100, $text) . '</p>
                        <a href="'.$button_cap_url.'"  >' . sh_character_limit(25, $link_caption) . '</a> 
                      </div>
                   ';

        return $output;
    }

    function ceo_message($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'overlap' => 'true',
                        ), $atts)
        );
        $args = array('post_type' => 'dict_testimonials', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'testimonial_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'testimonial_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
        //printr($args);
        $Posts = query_posts($args);
        //printr($Posts);
        $i = 1;
        $Message = $MessageNav = '';
        if (have_posts()): while (have_posts()): the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_testimonials_settings', true);
                $Message .= '<li>
                        <div class="carusal-image-thumb"> 
                            ' . get_the_post_thumbnail(get_the_ID(), '150x150') . ' 
                            <strong>' . sh_set($Settings, 'name') . ',</strong>
                            <span class="carusal-image-thumb-name"> ' . sh_set($Settings, 'designation') . ' </span> 
                        </div>
                        <p>' . get_the_content() . '</p></a>
                     </li>';
                $i++;
            endwhile;
        endif;
        wp_reset_query();
        $output = '';
        $output .= '<div class="testimonial';
        if ($overlap == 'true'): $output .= ' overlap';
        endif;
        $output .= '">
            <div class="slideshow">
                <ul class="slides">
                  ' . $Message . '
                </ul>
              </div>
        </div>
<script>
jQuery(document).ready(function($){
if( $(\'.slideshow\').length ){
        $(\'.slideshow\').flexslider({
            animation: "fade",
            animationLoop: false,
            slideShow:false,
            controlNav: true,   
            maxItems: 1,
            pausePlay: false,
            mousewheel:false,
            start: function(slider){
              $(\'body\').removeClass(\'loading\');
            }
        });
    }
});
</script>';
        return $output;
    }

    /* function recent_news( $atts, $content = null )
      {
      extract( shortcode_atts( array(
      'number' => '',
      'title' => __('Recent News', 'lifeline'),
      'category' => '',
      'sort_by' => 'date',
      'sorting_order' => 'DESC',
      'heading_style' => 'simple' ,
      'margins' =>'' ), $atts )
      );
      $marginsarr = explode(',' , $margins);

      $News = $Thumb = '';
      wp_reset_query();
      $args = array('post_type' => 'post' , 'posts_per_page'=> $number , 'orderby' => $sort_by , 'order' => $sorting_order, 'category' => $category);
      //if($category != '') $args['category'] = array($category);

      $query = new WP_Query($args);
      //printr($query);

      $i = 1;

      if( $query->have_posts()): while( $query->have_posts() ): $query->the_post();

      $Settings = get_post_meta( get_the_ID(), '_post_settings', true );

      if( $video_link = sh_set( $Settings, 'video_link' ) )
      {
      $opt = get_post_meta( get_the_ID(), '_dictate_gal_videos', true );
      $video_data = sh_grab_video( $video_link, $opt );

      $PostMedia = '<div class="image">
      <img src="'.sh_set( $video_data, 'thumb').'" style="width:261px; height:207px;" alt="'.sh_set( $video_data, 'title' ).'" />
      <a class="html5lightbox" rel="prettyPhoto" href="'.$video_link.'" title="'.sh_set( $video_data, 'title' ).'">
      <span><i class="icon-play"></i></span>
      </a>
      </div>';
      $CarouselThumb = '<img src="'.sh_set( $video_data, 'thumb').'" style="width:131px; height:78px;" alt="'.sh_set( $video_data, 'title' ).'" />';
      }
      else
      {
      $PostAttchment = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );
      $PostMedia = '<div class="image">
      '.get_the_post_thumbnail( get_the_ID(), '370x252' ).'
      <a   href="'.sh_set( $PostAttchment, 0 ).'" rel="prettyPhoto" class="html5lightbox"><i class="icon-picture"></i></a>
      </div>';
      $CarouselThumb = get_the_post_thumbnail( get_the_ID(), '270x155' );
      }

      $News .= '<div id="news'.$i.'">
      <div class="row">
      <div class="col-md-6 desc">
      <h3><a href="'.get_permalink().'"  >'.sh_character_limit( 35, get_the_title() ).'</a></h3>
      <p>'.sh_character_limit( 340, get_the_content() ).'</p>
      </div>
      <div class="col-md-6">
      '.$PostMedia.'
      </div>
      </div>
      </div>';
      $Thumb .= '<a href="#news'.$i.'">'.$CarouselThumb.'<span class="carusal-our-news">'.sh_character_limit( 17, get_the_title() ).'</span></a>';
      $i++;
      endwhile;
      endif;
      wp_reset_query();
      wp_enqueue_script('carofredcsel');
      $output = '' ;
      $output.= ( in_array('top' , (array)$marginsarr)) ? '<div class="block"></div>' : '';
      $output .= ($heading_style == 'underline')? '<div class="sec-heading">
      '.sh_get_title( $title, 'h2', 'strong', TRUE ).'
      </div>' : '' ;
      $output .= ($heading_style == 'modern')? '<div class="sec-title">
      '.sh_get_title( $title, 'h1', 'span', FALSE ).'
      </div>' : '' ;
      $output .= ($heading_style == 'simple')? '<div class="sec-heading2">
      '.sh_get_title( $title, 'h2', 'strong', TRUE ).'
      </div>' : '' ;

      $output .= '<div class="carusal-slider">
      <div id="carousel-wrapper">
      <div id="carousel">
      '.$News.'
      </div>
      </div>
      <div id="thumbs-wrapper">
      <div id="thumbs">'.$Thumb.'</div>
      <a id="prev" href="#"><i class="icon-angle-left"></i></a> <a id="next" href="#"><i class="icon-angle-right"></i></a> </div>
      </div>
      <script>
      jQuery(document).ready(function($){
      $(\'#carousel\').carouFredSel({
      responsive: true,
      circular: false,
      auto: false,
      items: {
      visible: 1,
      width: 20,
      },
      scroll: {
      fx: \'directscroll\'
      }
      });
      $(\'#thumbs\').carouFredSel({
      responsive: true,
      circular: false,
      infinite: false,
      auto: false,
      prev: \'#prev\',
      next: \'#next\',
      items: {
      visible: {
      min: 1,
      max: 6
      },
      width: 200,
      height: \'80%\'
      }
      });
      $(\'#thumbs a\').click(function() {
      $(\'#carousel\').trigger(\'slideTo\', \'#\' + this.href.split(\'#\').pop() );
      $(\'#thumbs a\').removeClass(\'selected\');
      $(this).addClass(\'selected\');
      return false;
      });
      });
      </script>
      ';
      $output.= ( in_array('bottom' , (array)$marginsarr)) ? '<div class="block"></div>' : '';
      return $output ;
      }
     */

    function recent_events($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Recent Events',
            't_limit' => '',
            'number' => '',
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'heading_style' => 'simple',
                        ), $atts)
        );

        $args = array('post_type' => 'dict_event', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);

        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'slug', 'terms' => $cat));
            }
        }


        //printr($args);
        $Posts = query_posts($args);

        //printr($Posts);

        query_posts($args);

        $Events = '';
        $i = 1;
        if (have_posts()): while (have_posts()): the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
                $PostThumbSize1 = ($i == 1) ? 'style="width:570px; height:184px;"' : 'style="width:100px; height:100px;"';
                $PostThumbSize2 = ($i == 1) ? '570x220' : '150x150';
                $EventOrganizer = ($i == 1 && sh_set($Settings, 'organizer')) ? '<li><a href="#"  ><i class="icon-user"></i>' . __('by', 'lifeline') . ' ' . sh_set($Settings, 'organizer') . '</a></li>' : '';
                $EventdateDetails = '';
                if (sh_set($Settings, 'video_link')) {
                    $video_link = sh_set($Settings, 'video_link');
                    $video_data = sh_grab_video($video_link, $Settings);
                    $PostMedia = '<div class="carou-post-img">
                              <img src="' . sh_set($video_data, 'thumb') . '" ' . $PostThumbSize1 . '  alt="' . sh_set($video_data, 'title') . '" />
                              <a title="' . sh_set($video_data, 'title') . '" href="' . $video_link . '" class="html5lightbox"><i class="icon-play"></i></a>
                          </div>';
                } else {
                    //$PostMedia = get_the_post_thumbnail( get_the_ID(), $PostThumbSize );
                    $PostAttchment = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large');
                    $PostMedia = '<div class="carou-post-img">
                              ' . get_the_post_thumbnail(get_the_ID(), $PostThumbSize2) . '
                              <a   href="' . sh_set($PostAttchment, 0) . '" class="html5lightbox"><i class="icon-picture"></i></a>
                          </div>';
                }
                if (!empty($Settings['start_date'])) {
                    $Eventdate = new DateTime($Settings['start_date']);
                    $EventdateDetails = '<li><a href="' . get_permalink() . '"  ><i class="icon-calendar-empty"></i><span>' . $Eventdate->format('F') . '</span> ' . $Eventdate->format('d, Y') . '</a></li>';
                } else if (!empty($Settings['end_date'])) {
                    $Eventdate = new DateTime($Settings['end_date']);
                    $EventdateDetails = '<li><a href="' . get_permalink() . '"  ><i class="icon-calendar-empty"></i><span>' . $Eventdate->format('F') . '</span> ' . $Eventdate->format('d, Y') . '</a></li>';
                }
                $Class = ($i == 1) ? 'recent-event' : 'recent-event  previous-event';
                $md_class = ($i == 1) ? 'col-md-12' : 'col-md-6';
                $Location = (sh_set($Settings, 'location')) ? '<li><a href="' . get_permalink() . '"  ><i class="icon-map-marker"></i>in ' . sh_set($Settings, 'location') . '</a></li>' : '';
                $TitleLength = ($i == 1) ? $t_limit : $t_limit;
                //printr($TitleLength);
                $Events .= '<div class="' . $md_class . '">
                        <div class="' . $Class . '">
                          <div class="recent-event-img"> ' . $PostMedia . ' </div>
                          <h4><a href="' . get_permalink() . '"  >' . sh_character_limit($TitleLength, get_the_title()) . '</a></h4>
                          <ul>
                            ' . $EventOrganizer . '
                            ' . $Location . '
                            ' . $EventdateDetails . '
                          </ul>
                       </div>
                   </div>';
                $i++;
            endwhile;
        endif;
        wp_reset_query();
        $output = '';


        $output .= '
                 
                  <div class="row">
                      ' . $Events . '
                  </div>
        ';

        return $output;
    }

    function recent_events_2($atts, $content = null) {
        
        extract(shortcode_atts(array(
            'title' => 'Upcoming Events',
            'number' => 2,
            'cat' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'heading_style' => 'simple',
                        ), $atts)
        );
        
        //wp_enqueue_script(array('jquery-plugins', 'jquery-countdown-min'));
        $args = array('post_type' => 'dict_event', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
        //printr($args);
        $Posts = new WP_Query($args);
        //printr($Posts);
        $Events = '';
        $count = 1;
        if ($Posts->have_posts()): while ($Posts->have_posts()): $Posts->the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
                $EventdateDetails = '';
                if (!empty($Settings['start_date'])) {
                    $Eventdate = new DateTime($Settings['start_date']);
                    $EventdateDetails = '<a href="' . get_permalink() . '"  ><i class="icon-calendar-empty"></i><span>' . $Eventdate->format('F') . '</span> ' . $Eventdate->format('d, Y') . '</a>';
                } else if (!empty($Settings['end_date'])) {
                    $Eventdate = new DateTime($Settings['end_date']);
                    $EventdateDetails = '<a href="' . get_permalink() . '"  ><i class="icon-calendar-empty"></i><span>' . $Eventdate->format('F') . '</span> ' . $Eventdate->format('d, Y') . '</a>';
                }
                $end_date = explode('-', sh_set($Settings, 'end_date'));
                $Location = (sh_set($Settings, 'location')) ? ' <a href="' . get_permalink() . '"  ><i class="icon-map-marker"></i>In ' . sh_set($Settings, 'location') . '</a>' : '';
                $counter_class = "count-down" . $count;
                $variable = 'variable_' . $count;
                $Events .= '<div class="col-md-6">
                        <div class="event">
                          <div class="event-thumb"> ' . get_the_post_thumbnail(get_the_ID(), '270x155') . '
                            <div class="counter">
                              <ul class="countdown time' . get_the_ID() . '">
                                    <li><p class="days_ref">' . __('days', 'lifeline') . '</p><span class="days">00</span></li>
                                    <li><p class="hours_ref">' . __('hours', 'lifeline') . '</p><span class="hours">00</span></li>
                                    <li><p class="minutes_ref">minutes</p><span class="minutes">00</span></li>
                                    <li><p class="seconds_ref">seconds</p><span class="seconds">00</span></li>
                                </ul>
                            </div>
                          </div>
                          <div class="event-intro">
                            <h5><a href="' . get_permalink() . '"  >' . sh_character_limit(35, get_the_title()) . '</a></h5>
                            ' . $EventdateDetails . $Location . ' 
                          </div>
                       </div>
                   </div>';

                $start_time = sh_set($Settings, 'start_time');
                $total_time = sh_set($Settings, 'start_date') . ' ' . sh_set($Settings, 'start_time');
                $e_date = new DateTime($total_time);
                $date = $e_date->format('Y/m/d H:i:s');
                $Events .= '<script>
                      jQuery(document).ready(function($){
                         jQuery(".countdown.time' . get_the_ID() . '").downCount({
                                date: "' . $date . '",
                                offset: ' . get_option('gmt_offset') . '
                            });
                      });
                    </script>
                   ';
                $count++;
            endwhile;
        endif;
        wp_reset_query();
        $output = '';

        $output .= '<div class="recent-events">';

        $output .= '<div class="row">
                            ' . $Events . '
                        </div>
                  </div>';

        return $output;
    }

    function recent_events_3($atts, $content = null) {
        
        extract(shortcode_atts(array(
            'title' => 'Upcoming Event',
            'number' => '',
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'heading_style' => 'simple',
                        ), $atts)
        );
        $args = array('post_type' => 'dict_event', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != '') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'term_id', 'terms' => $category));
            } elseif($category=='all') { 
                $args = array('post_type' => 'dict_event', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order); 
            }else{
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $Posts = query_posts($args);
        $Events = '';
        if (have_posts()): while (have_posts()): the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
                $EventdateDetails = '';
                if (sh_set($Settings, 'start_date')) {
                    $Eventdate = new DateTime($Settings['start_date']);
                    $EventdateDetails = '<li><a href="' . get_permalink() . '"  ><i class="icon-calendar"></i>' . $Eventdate->format('M d Y') . '</a></li>';
                } else if (sh_set($Settings, 'end_date')) {
                    $Eventdate = new DateTime($Settings['end_date']);
                    $EventdateDetails = '<li><a href="' . get_permalink() . '"  ><i class="icon-calendar"></i>' . $Eventdate->format('M d Y') . '</a></li>';
                }
                $TwitterLink = (sh_set($Settings, 'twitter_link')) ? '<li><a href="' . sh_set($Settings, 'twitter_link') . '"  ><i class="icon-twitter"></i>' . __('Twitter Updates', 'lifeline') . '</a></li>' : '';
                $Organizer = (sh_set($Settings, 'organizer')) ? '<li><a href="' . get_permalink() . '"  ><i class="icon-pencil"></i>' . sh_set($Settings, 'organizer') . '</a></li>' : '';
                $Events .= '<div class="upcoming-event"> 
                      <a href="' . get_permalink() . '"  >' . get_the_post_thumbnail(get_the_ID(), '270x155') . '</a>
                      <h5>' . sh_character_limit(25, get_the_title()) . '</h5>
                      <ul>
                        ' . $TwitterLink . $EventdateDetails . $Organizer . '
                      </ul>
                   </div>';
            endwhile;
        endif;
        wp_reset_query();
        $title = sh_character_limit(20, $title);
        $output = '';


        $output .= $Events;

        return $output;
    }

    function successful_stories($atts, $content = null) {
        extract(shortcode_atts(
                        array(
            'title' => __('Successful Stories', 'lifeline'),
            'number' => '',
            'category' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'c_opt' => 'excerpt',
            'c_limit' => '150',
            'heading_style' => 'simple',
                        ), $atts)
        );
        $output = '';

        $args = array(
            'post_type' => 'dict_project',
            'showposts' => $number,
            'orderby' => $sort_by,
            'order' => $sorting_order
        );
        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $query = new WP_Query($args);
        $counter = 1;
        $i = 1;
        $output .= '<div class="stories-carousel"><ul class="slides">';
        $output .= '<li><div class="row">';
        while ($query->have_posts()): $query->the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_project_settings', true);

            $Location = ( sh_set($Settings, 'location') ) ? '<span><i class="icon-map-marker"></i>' . __('In', 'lifeline') . ' ' . sh_set($Settings, 'location') . '</span>' : '';
            $MoneySpent = (sh_set($Settings, 'spent_amount')) ? '<h6><i>' . sh_set($Settings, 'spent_amount_currency') . '</i> ' . sh_set($Settings, 'spent_amount') . '<span>' . __('Money Spent', 'lifeline') . '</span></h6>' : '';
            $output .= '<div class="col-md-6">
                            <div class="story">
                                <div class="story-img">' . get_the_post_thumbnail(get_the_ID(), '370x252') . '
                                    <h5>' . sh_character_limit(25, get_the_title()) . '</h5>
                                    <a href="' . get_the_permalink() . '" title="' . get_the_title() . '"><span></span></a> 
                                </div>
                                <div class="story-meta">
                                    <span><i class="icon-calendar-empty"></i>' . get_the_date("m-d-y", get_the_ID()) . '</span>
                                     ' . $Location . '
                                    <p>' . __("Needed Donation ", 'lifeline') . '<strong>$ ' . sh_set($Settings, 'amount_needed') . '</strong></p>
                                </div>';
            $output .= '<p>';
            if ($c_opt == 'excerpt') {
                $output .= get_the_excerpt();
            } elseif ($c_opt == 'full') {
                $output .= get_the_content(get_the_ID());
            } elseif ($c_opt == 'limit') {
                $output .= sh_contents(get_the_content(get_the_ID()), $c_limit);
            }
            $output .="</p>";
            $output .=' </div>
                        </div>';

            if ($counter % 2 == 0 && $i != $query->post_count) {
                $output .= '</div></li><li><div class="row">';
                $counter = 0;
            }
            $i++;
            $counter++;
        endwhile;
        wp_reset_query();
        $output .= '</div></li></ul></div>';
        return $output;
    }

      function welfare_projects($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Our Welfare Projects',
            'number' => 6,
            'category' => 'all',
            'sort_by' => 'date',
            'sorting_order' => 'DESC',
            'heading_style' => 'simple',
               'donate_now'   => '',
            'needed_amount' => '',
            'needed_text'  => '',
            'title_limit' => '18',
            'limit' => '100',
                        ), $atts)
        );
//printr($atts);
        $paypal = $GLOBALS['_sh_base']->donation;
        $args = array('post_type' => 'dict_project', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $pro = new WP_Query($args);
        $theme_options = get_option( 'lifeline' );
        //print_r(sh_set($Settings, 'paypal_currency') ); exit('aaa');
        $Project = '';
        if ($pro->have_posts()): while ($pro->have_posts()): $pro->the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_project_settings', true);
	        //printr($Settings);
                $symbol = sh_set( $Settings, 'amount_needed_currency', '$' );
                $Project .= '<div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="icon-box"> 
                                    <i style="color:' . sh_set($Settings, 'color', '#A5A4A4') . '" class="' . sh_set($Settings, 'font') . '"></i>';
                                      if( $donate_now =='true' ) : 
                                     $Project .= '<div class="need"><a href="' . get_permalink() . '">' . __('Donate Now', 'lifeline') . '</a></div>';
                                  endif;
                                 $Project .= '</div>
                            </div>
                            <div class="col-md-7">

                                <div class="project-detail"> <a href="' . get_permalink() . '">' . sh_character_limit($title_limit, get_the_title()) . '</a>';
                                        if( $needed_amount =='true' ) : 
                              $Project .= '<span>' . $needed_text . ' '. $symbol . sh_set($Settings, 'amount_needed') . '</span>';
                                  endif;
                                   $Project .= '<p>' . sh_excerpt(get_the_content(), $limit) . '</p>
                             
                                </div>
                            </div>
                        </div>
                     </div>';
            endwhile;
        endif;
        wp_reset_query();
        $output = '';


        $output .= '<div class="our-project-box">
                        <div class="row">
                          ' . $Project . '
                        </div>
                      </div>
                   ';

        return $output;
    }


    function welfare_projects_2($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Our Welfare Projects',
            'number' => '',
            'category' => 'all',
            'sort_by' => 'date',
            'heading_style' => 'simple',
            'sorting_order' => 'ASC',
            'limit'         => 'limit',
                        ), $atts)
        );

        $paypal = $GLOBALS['_sh_base']->donation;
        $args = array('post_type' => 'dict_project', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $Posts = query_posts($args);
        $Project = '';
        if (have_posts()): while (have_posts()): the_post();
                $Settings = get_post_meta(get_the_ID(), '_dict_project_settings', true);
                $symbol = sh_set( $Settings, 'amount_needed_currency', '$' );
                
                $Project .= '<div class="col-md-4">
                        <div class="row">
                            <div class="col-md-5">
                                                            <div class="icon-box"> <i style="color:' . sh_set($Settings, 'color', '#A5A4A4') . '" class="' . sh_set($Settings, 'font') . '"></i>
                                                                <div class="need"><a href="' . get_permalink() . '">' . __('Donate Now', 'lifeline') . '</a></div>
                                                            </div>
                            </div>
                            <div class="col-md-7">
                                                            <div class="project-detail"> <a href="' . get_permalink() . '">' . sh_character_limit(18, get_the_title()) . '</a>
                                                                <span>' . __('NEEDED', 'lifeline') . ' '. $symbol . sh_set($Settings, 'amount_needed') . '</span>
                                                                <p>' . sh_contents(get_the_content(get_the_ID()), $limit) . '</p>
                                                            </div>
                            </div>
                        </div>
                    </div>';
            endwhile;
        endif;
        wp_reset_query();
        $output = '';


        $output .= '<div class="our-project-box">
                      <div class="row">
                        ' . $Project . '
                      </div>
                    </div>';

        return $output;
    }

    function block_quotes($atts, $content = null) {
        extract(shortcode_atts(array('title' => '', 'blockquotes' => ''), $atts));
        $output = '<section class="element" id="blockquotes-style">
                      <h3 class="sub-head">' . $title . '</h3>
                      <blockquote><i class="icon-quote-left"></i>' . $blockquotes . '<i class="icon-quote-right"></i></blockquote>
                   </section>';
        return $output;
    }

    function boxed_block_quotes($atts, $content = null) {
        extract(shortcode_atts(array('title' => '', 'text1' => '', 'text2' => '', 'blockquotes' => ''), $atts));
        $TextBeforeBlockQuote = (!empty($text1)) ? '<p>' . $text1 . '</p><br />' : '';
        $TextAfterBlockQuote = (!empty($text2)) ? '<p>' . $text2 . '</p>' : '';
        $output = '<section class="element" id="blockquotes-style">
                      <h3 class="sub-head">' . $title . '</h3>
                      <blockquote><i class="icon-quote-left"></i>' . $text1 . '<i class="icon-quote-right"></i></blockquote>
                   </section>
                   
                   <section class="element" >
                      <h3 class="sub-head">' . $title . '</h3>
                      ' . $TextBeforeBlockQuote . '
                      <blockquote class="boxed-quote"><i class="icon-quote-left"></i>' . $blockquotes . '<i class="icon-quote-right"></i></blockquote>
                      ' . $TextAfterBlockQuote . '
                   </section>';
        return $output;
    }

    function highlight_text($atts, $content = null) {
        extract(shortcode_atts(array('text' => '', 'rounded' => ''), $atts));
        $Class = ($rounded = 1) ? 'highlight rounded' : 'highlight';
        return '<p class="highlight rounded">' . $text . '</p>';
    }

    function button($atts, $content = null) {

        extract(shortcode_atts(array('text' => '', 'link' => '', 'size' => 'small', 'color' => 'skyblue'), $atts));
        return '<a href="' . $link . '" title="' . $text . '" class="theme-btn ' . strtolower($color) . ' ' . strtolower($size) . '">' . $text . '</a>';
    }

    function list_item($atts, $content = null) {
        extract(shortcode_atts(array('text' => '', 'style' => 'icon-check'), $atts));
        return '<li><i class="' . $style . '"></i>' . $text . '</li>';
    }

    function alert_box($atts, $content = null) {
        extract(shortcode_atts(array('type' => 'done', 'title' => '', 'message' => ''), $atts));
        $title .= (!empty($title)) ? '! ' : '';
        if ($type == 'done')
            $Icon = 'icon-ok';
        else if ($type == 'attention')
            $Icon = 'icon-exclamation-sign';
        else if ($type == 'cancel')
            $Icon = 'icon-remove';
        else if ($type == 'warning')
            $Icon = 'icon-warning-sign';
        return '<div class="alert-box ' . $type . '"> <i class="' . $Icon . '"></i>
                  <h4>' . $title . '<span>' . $message . '</span></h4>
                </div>';
    }

    function social_media_icon($atts, $content = null) {
        extract(shortcode_atts(array('media' => 'social_facebook'), $atts));
        return'<ul class="social-icons multi">
            <li><i class="' . str_replace('_', ' ', $media) . '"></i></li>
        </ul>';
    }

    function progressbar($atts, $content = null) {
        extract(shortcode_atts(array('title' => '', 'percentage' => '0', 'pattren' => '', 'position' => 'within-progress-bar'), $atts));
        $Title = (!empty($title)) ? '<h6>' . $title . '</h6>' : '';
        $Top = ($position == 'top') ? '<span>' . $percentage . '%</span>' : '';
        $Middle = ($position == 'within-progress-bar') ? '<span>' . $percentage . '%</span>' : '';
        return '<div class="progress-report">
                  ' . $Title . $Top . '
                  <div class="progress pattern">
                    <div class="progress-bar" style="width: ' . $percentage . '%"><span>' . $Middle . '</span></div>
                  </div>
                </div>';
    }

    function price_table($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'sub_title' => '',
            'description' => '',
            'short_desc' => '',
            'currency' => '',
            'price' => '',
            'duration' => '',
            'option1' => '',
            'option2' => '',
            'option3' => '',
            'link' => ''
                        ), $atts));
        $ShortDesc = (!empty($short_desc)) ? '<span><p>' . $short_desc . '</span>' : '';
        $Description = (!empty($description)) ? '<li class="table-desc"><p>' . $description . '</p></li>' : '';
        $Options = '';
        $Options .= (!empty($option1)) ? '<li><i class="icon-ok-sign"></i>' . $option1 . '</li>' : '';
        $Options .= (!empty($option2)) ? '<li><i class="icon-ok-sign"></i>' . $option2 . '</li>' : '';
        $Options .= (!empty($option3)) ? '<li><i class="icon-ok-sign"></i>' . $option3 . '</li>' : '';
        $output = '</ul>
                   <ul>
                      <li class="table-head">
                        <h3>' . $title . '</h3>
                        ' . $ShortDesc . '
                      </li>
                      ' . $Description . '
                      ' . $Options . '
                      <li class="price-per-year"><i>' . $currency . '</i> ' . $price . '<span>' . $duration . '</span></li>
                      <li class="table-btn"><a href="' . $link . '"  >' . __('Select Plan', 'lifeline') . '</a></li>
                   </ul>';
        return $output;
    }

    function charity_video($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Charity Video',
            'heading_style' => 'simple',
            'video_title' => '',
            'video_link' => '',
            'description' => '',
            'video_link2'=>'',
            'video_type'=>''
                        ), $atts)
        );

        $title = sh_character_limit(15, $title);
        $opt = get_post_meta(get_the_ID(), '_dictate_gal_videos', true);
        $url = (is_ssl())?'https://vimeo.com/' . $video_link:'http://vimeo.com/' . $video_link;
        $video_data = sh_grab_video($url, $opt);
        $output = '';
        if($video_type == 'youtube') :
         //printr(lif_vd_details($video_link2));

        $video = '<img src="' . sh_set(sh_set(lif_vd_details($video_link2),'thumbnail'),'url') . '" style="width:270px; height:184px;" alt="' . sh_set($video_data, 'title') . '" /> <a class="html5lightbox" href="' . $video_link2 . '" title="' . sh_set(lif_vd_details($video_link2),'title') . '"> <span><i class="icon-play"></i></span> </a>';

    else : 
        $video = '<img src="' . sh_set($video_data, 'thumb') . '" style="width:270px; height:184px;" alt="' . sh_set($video_data, 'title') . '" /> <a class="html5lightbox" href="https://player.vimeo.com/video/' . $video_link . '" title="' . $video_title . '"> <span><i class="icon-play"></i></span> </a>';
endif; 

        $output .= ' <div class="charity-video">
                      <div class="row">
                        <div class="col-md-6 desc">
                          <h3><a href="javascript:void(0)"  >' . sh_character_limit(35, $video_title) . '</a></h3>
                          <p>' . sh_character_limit(310, $description) . '</p>
                        </div>
                        <div class="col-md-6">
                            <div class="image"> '.$video.' </div>
                        </div>
                        </div>
                      </div>';

        return $output;
    }

    function lifeline_video($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'video_link' => '',
            'video_thumb' => '',
                        ), $atts)
        );


        $output = '';


        $output .= '<div class="video">';
        $output .= ($video_thumb) ? wp_get_attachment_image($video_thumb, 'full') : '';
        $output .= ($title) ? '<span>' . $title . '</span>' : '';
        $output .= ($video_link) ? '<span class="play-video"><a title="' . esc_attr($title) . '" href="' . esc_url($video_link) . '?color=ffffff" class="html5lightbox"><i class="icon-play"></i></a></span>' : '';
        $output .= '</div>';

        return $output;
    }

    function charity_video2($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'video_link' => '',
            'duration' => '',
            'projects' => '',
            'members' => '',
            'description' => '',
                        ), $atts)
        );

        $opt = get_post_meta(get_the_ID(), '_dictate_gal_videos', true);
        $url = $video_link;
        $host = explode('.', str_replace('www.', '', strtolower(parse_url($url, PHP_URL_HOST))));
        $host = isset($host[0]) ? $host[0] : $host;
         $video_data = lifeline_vd_details($url);
        if($host=='youtube'):
            $video_thumb=sh_set(sh_set($video_data, 'thumbnail'), 'url');
            else:   
                $video_thumb=sh_set($video_data, 'thumbnail');        
        endif;
       
        $CharityDuration = (!empty($duration)) ? '<li><h6>' . $duration . '</h6><span>' . __("Years In Charity", 'lifeline') . '</span></li>' : '';
        $CharityProjects = (!empty($projects)) ? '<li><h6>' . $projects . '</h6><span>' . __("Project Handled", 'lifeline') . '</span></li>' : '';
        $StafMembers = (!empty($members)) ? '<li><h6>' . $members . '</h6><span>' . __("Staff Members", 'lifeline') . '</span></li>' : '';
        $output = '';

        $output .= '<div class="about-charity">
                      <div class="container">
                        <div class="row">
                          <div class="about-charity-desc col-md-7">
                            <h2>' . sh_character_limit(35, $title) . '</h2>
                            <p>' . sh_character_limit(385, $description) . '</p>
                            <ul>
                              ' . $CharityDuration . $CharityProjects . $StafMembers . '
                            </ul>
                          </div>
                          <div class="col-md-5">
                            <div class="about-charity-video"> <img src="' . $video_thumb . '" style="width:470px; height:278px;" alt="' . sh_set($video_data, 'title') . '" /> <a class="html5lightbox" href="' . esc_attr(sh_set($video_data, 'video')) . '" title="' . sh_set($video_data, 'title') . '"><span><i class="icon-play"></i></span></a> </div>
                          </div>
                        </div>
                      </div>
                   </div>';

        return $output;
    }

    function team($atts, $content = null) {
        extract(shortcode_atts(array(
            'category' => 'all',
            'sort_by' => 'date',
            'number' => 10,
            'cols' => '',
            'sorting_order' => '',
                        ), $atts)
        );

        $args = array('post_type' => 'dict_team', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'team_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'team_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $Posts = query_posts($args);
        $i = 1;
        $Records = '';
	    $cols = $cols ? $cols : 'col-md-3';
        if (have_posts()): while (have_posts()): the_post();
                global $post;
                $Settings = get_post_meta(get_the_ID(), '_dict_team_settings', true);
                $Name = (sh_set($Settings, 'name')) ? '<h3><a href="' . get_permalink() . '">' . sh_character_limit(30, sh_set($Settings, 'name')) . '</a></h3>' : '';
                
                $Designation = (sh_set($Settings, 'designation')) ? '<span>' . sh_character_limit(30, sh_set($Settings, 'designation')) . '</span>' : '';
              
                $FbLink = (sh_set($Settings, 'fb_link')) ? '<li><a target="_blank" href="' . sh_set($Settings, 'fb_link') . '"  ><img src="' . get_template_directory_uri() . '/images/facebook.jpg" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" /></a></li>' : '';
                $GPlusLink = (sh_set($Settings, 'gplus_link')) ? '<li><a target="_blank" href="' . sh_set($Settings, 'gplus_link') . '"  ><img src="' . get_template_directory_uri() . '/images/gplus.jpg" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" /></a></li>' : '';
                $Records .= '<div class=" '. esc_attr( $cols ) .' ">
                      <div class="staff-member">
                        <a href="' . get_the_permalink() . '"  > ' . get_the_post_thumbnail(get_the_ID(), '570x570') . '</a>
                        <div class="member-intro">
                          ' . $Name . '
                          ' . $Designation . '
                        </div>
                        <div class="social-contacts">
                          <ul>
                            ' . $FbLink . '
                            ' . $GPlusLink . '
                          </ul>
                        </div>
                      </div>
                     </div>';
            endwhile;
        endif;
        wp_reset_query();
        $output = '';

        $output .= '<div class="container">
                      <div class="staff">
                        <div class="row">
                          ' . $Records . '
                        </div>
                      </div>
                   </div>';

        return $output;
    }

    function shop_online($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'title' => 'Shop Online For Donation',
            'heading_style' => 'simple',
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
                        ), $atts)
        );

        global $woocommerce, $product;
        //ob_start();
        $query_args = array(
            'posts_per_page' => $number,
            'post_type' => 'product',
        );
        $query_args['meta_query'] = array();
        $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
        $query = new WP_Query($query_args);
        include(get_template_directory() . '/framework/modules/product.php');
        return $output;
    }

    /* function our_blog($atts, $content = null)
      {
      extract(shortcode_atts(array(
      'title' => '',

      ), $atts)
      );

      $args = array('posts_per_page' => 3);
      $output = '';


      $output .= '<div class="main-blog row">';
      $posts = get_posts($args);
      foreach ($posts as $pos):
      $post_meta = get_post_meta(sh_set($pos, 'ID'), '_post_settings', true);
      $output .= '<div class="col-md-6"><div class="blog-post">';
      $image = wp_get_attachment_image_src(get_post_thumbnail_id(sh_set($pos, 'ID')), '1170x455');
      $output .= '<a class="blog-post-img" href="' . get_permalink(sh_set($pos, 'ID')) . '"  ><img src="' . sh_set($image, 0) . '" alt=""></a>';
      $output .= '<h2><a href="' . get_permalink(sh_set($pos, 'ID')) . '"  >' . get_the_title(sh_set($pos, 'ID')) . '</a></h2>';
      $output .= '<div class="blog-post-details">
      <ul class="post-meta">
      <li><a href=""  ><i class="icon-calendar-empty"></i><span>' . get_the_time("M", sh_set($pos, 'ID')) . '</span> ' . get_the_time("d-Y", sh_set($pos, 'ID')) . '</a></li>
      <li><a href=""  ><i class="icon-share-alt"></i>' . sh_set(sh_set(get_the_category(sh_set($pos, 'ID')), 0), 'name') . '</a></li>
      <li><a href=""  ><i class="icon-map-marker"></i>' . __("In", 'lifeline') . ' ' . sh_set($post_meta, 'location') . '</a></li>
      </ul>';
      $output .= '<div class="post-desc"><p>' . substr(strip_tags(sh_set($pos, 'post_content')), 0, 150) . '</p></div>';
      $output .= '</div></div></div>';
      endforeach;
      wp_reset_query();
      $output .= '</div>';

      return $output;
      } */

    function services($atts, $content = null) {
        extract(shortcode_atts(array(
        	'number' => '',
            'col' => '',
            'sort_by' => '',
            'sorting_order' => 'asc',
            'linked' => '',
            'overlap' => '',
                        ), $atts)
        );

        $args = array('post_type' => 'dict_services', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        $services = get_posts($args);
        $output = '';

        $uphalf_class = ($overlap) ? 'up-half' : '';
        $output .= '
        <div class="' . $uphalf_class . '"><div class="services">';
        foreach ($services as $service):
            $meta = get_post_meta(sh_set($service, 'ID'), '_dict_services_settings', true);
            $output .= '<div class="col-md-'.$col.'">
                    <div class="box">
                        <i class="' . sh_set($meta, 'font_awesome') . '"></i>';
            $output .= '<h4>';
            $output .= ($linked) ? '<a href="' . get_permalink(sh_set($service, 'ID')) . '">' : '';
            $output .= get_the_title(sh_set($service, 'ID'));
            $output .= ($linked) ? '</a>' : '';
            $output .= '</h4>';
            $output .= '
                    </div>
                </div>';
        endforeach;
        $output .= '</div></div></div>';

        return $output;
        wp_reset_query();
    }

    function Gallery($atts, $content = null) {
        extract(shortcode_atts(array(
            'cols' => '',
            'num' => 10,
                        ), $atts)
        );
        wp_enqueue_script(array('bootstrap'));

        $args = array('post_type' => 'dict_gallery', 'posts_per_page' => $num);
        $columns = array(1 => 'col-md-12', 2 => 'col-md-6', 3 => 'col-md-4', 4 => 'col-md-3');
        $max_limit_arr = array(1 => 6, 2 => 3, 3 => 2, 4 => 2);
        $col_class = sh_set($columns, $cols);
        if ($cols == 1 || $cols == 2)
            $featured_image_size = "1170x455";
        elseif ($cols == 3)
            $featured_image_size = "370x252";
        elseif ($cols == 4)
            $featured_image_size = "270x155";
        $max_limit = sh_set($max_limit_arr, $cols);
        $output = '';

        $output .= '<ul class="gallery-tabs nav nav-tabs" id="myTab">';
        $taxonomies = sh_get_categories(array('taxonomy' => 'gallery_category', 'hide_empty' => false), false, false);
	    $galleries = array();
        $count = 1;
        foreach ($taxonomies as $id => $tax_name):
            $args['tax_query'] = array(array('taxonomy' => 'gallery_category', 'field' => 'id', 'terms' => $id));
            $galleries[$tax_name] = get_posts($args);
            $active_class = ($count == 1) ? 'active' : '';
            $output .= '<li class="' . $active_class . '"><a data-toggle="tab" href="#cat_' . $tax_name . '">' . $tax_name . '</a></li>';
            $count++;

        endforeach;
        $output .= '</ul>';
        $output .= '<div class="gallery-content tab-content" id="myTabContent">';
        $count_2 = 1;
        foreach ($galleries as $tax => $gal_posts):
            $active_class_ = ($count_2 == 1) ? 'active' : '';
            $output .= '<div id="cat_' . $tax . '" class="tab-pane fade in ' . $active_class_ . '">
                            <div class="row">';
            foreach ($gal_posts as $gal):
                $output .= '<div class="' . $col_class . '">';
                $featured_image = sh_set(wp_get_attachment_image_src(get_post_thumbnail_id(sh_set($gal, 'ID')), $featured_image_size), 0);
                $output .= '<div class="gallery-image"><img src="' . $featured_image . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />';
                $output .= '<span>' . get_the_term_list($gal->ID, 'gallery_category', '', ' / ', '') . '</span>';
                $output .= '<div class="image-lists"><ul>';
                $Settings = get_post_meta(sh_set($gal, 'ID'), '_dict_gallery_settings', true);
                $GalleryAttachments = get_posts(array('post_type' => 'attachment', 'post__in' => explode(',', sh_set($Settings, 'gallery'))));
                $limiter = 1;
                foreach ($GalleryAttachments as $thumb_image):
                    $Thumb = sh_set(wp_get_attachment_image_src(sh_set($thumb_image, 'ID'), '150x150'), '0');
                    $LargeThumb = sh_set(wp_get_attachment_image_src(sh_set($thumb_image, 'ID'), '1170x455'), '0');
                    $output .= '<li>
                                <a class="html5lightbox" href="' . $LargeThumb . '" data-group="group' . sh_set($gal, 'ID') . '"  >
                                    <img src="' . $Thumb . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
                                </a>
                              </li>';
                    if ($limiter < $max_limit)
                        $limiter++;
                    else
                        break;
                endforeach;
                $output .= '</ul></div>';
                $output .= '</div>';
                $output .= '<h3 class="image-title"><a href="' . get_permalink(sh_set($gal, 'ID')) . '"  >' . sh_set($gal, 'post_title') . '</a></h3>';
                $output .= '</div>';
            endforeach;
            $output .= '        </div>
                      </div>';
            $count_2++;
        endforeach;
        $output .= '</div>';

        wp_reset_query();
        return $output;
    }

    function portfolio_without_sidebar($atts, $content = null) {
        extract(shortcode_atts(array(
            'cols' => '',
            'num' => '',
            'show_toggle' => '',
                        ), $atts)
        );
        wp_enqueue_script(array('jquery_isotope'));

        $args = array('post_type' => 'dict_portfolio', 'posts_per_page' => $num);
        $portfolios = get_posts($args);
        $page_meta = get_post_meta(get_the_ID(), '_page_settings', true);
        $columns = array(2 => 'col-md-6', 3 => 'col-md-4', 4 => 'col-md-3');
        $col_class = sh_set($columns, $cols);
        $output = '';
        if ($show_toggle == "true") {
            $output .= '<section id="options1"><div class="option-combo">
            <ul id="filter" class="option-set" data-option-key="filter"><li><a href="#show-all" data-option-value="*" class="selected">' . __("All", 'lifeline') . '</a></li>';
            $taxonomies = sh_get_categories(array('taxonomy' => 'portfolio_category'));
            foreach ($taxonomies as $tax_id => $tax_name):
                if ($tax_id != 'all') :
                    $output .= '<li><a href="#category' . $tax_id . '" data-option-value=".category' . $tax_id . '">' . $tax_name . '</a></li>';
                endif;
            endforeach;
            $output .= '</ul></section>';
        }
        $output .= '<div class="row"><div id="portfolio1" class="variable-sizes bounceinup">';
        $loop_count = 1;
        foreach ($portfolios as $portfolio):
            $thumb_size = ($loop_count == 1) ? '370x491' : '370x252';
            $post_term = get_the_terms($portfolio, 'portfolio_category');
            if (!empty($post_term)) : foreach ($post_term as $t) :
                    $output .= '<div class="category' . sh_set($t, 'term_id') . ' ' . $col_class . '">
                        <div class="portfolio">
                            ' . get_the_post_thumbnail(sh_set($portfolio, 'ID'), $thumb_size) . '
                            <div class="port-desc">
                                <h4><a href="' . get_permalink(sh_set($portfolio, 'ID')) . '">' . get_the_title(sh_set($portfolio, 'ID')) . '</a></h4>
                                <p>' . substr(strip_tags(sh_set($portfolio, 'post_content')), 0, 200) . '</p>
                            </div>
                        </div>
                    </div>';
                endforeach;
            endif;
            $loop_count++;
            if ($loop_count > 2)
                $loop_count = 1;
        endforeach;
        $output .= '</div></div></div>
        <script>
            jQuery(document).ready(function($){
            jQuery(function($){
              var $portfolio = $(\'#portfolio1\');
            
              $portfolio.isotope({
                masonry: {
                  columnWidth: 1
                }
              });
              var $optionSets = $(\'#options1 .option-set\'),
                  $optionLinks = $optionSets.find(\'a\');
              $optionLinks.click(function(){
                var $this = $(this);
                // don\'t proceed if already selected
                if ( $this.hasClass(\'selected\') ) {
                  return false;
                }
                var $optionSet = $this.parents(\'.option-set\');
                $optionSet.find(\'.selected\').removeClass(\'selected\');
                $this.addClass(\'selected\');
                var options = {},
                    key = $optionSet.attr(\'data-option-key\'),
                    value = $this.attr(\'data-option-value\');
                // parse \'false\' as false boolean
                value = value === \'false\' ? false : value;
                options[ key ] = value;
                if ( key === \'layoutMode\' && typeof changeLayoutMode === \'function\' ) {
                  // changes in layout modes need extra logic
                  changeLayoutMode( $this, options )
                } else {
                  // otherwise, apply new options
                  $portfolio.isotope( options );
                }
                return false;
              });
            });
            });
            </script>
        ';

        return $output;
    }

    function portfolio_with_sidebar($atts, $contents = null) {
        extract(shortcode_atts(array(
            'cols' => '',
            'num' => '',
                        ), $atts)
        );
        wp_enqueue_script(array('jquery_isotope'));

        $args = array('post_type' => 'dict_portfolio', 'posts_per_page' => $num);
        $portfolios = get_posts($args);
        $page_meta = get_post_meta(get_the_ID(), '_page_settings', true);
        $sidebar = sh_set($page_meta, 'sidebar') ? sh_set($page_meta, 'sidebar') : 'default-sidebar';
        $columns = array(2 => 'col-md-6', 3 => 'col-md-4');
        $col_class = sh_set($columns, $cols);
        $output = '';

        $output .= '<div class="row"><div class="left-content col-md-9">
        <section id="options"><div class="option-combo">
        <ul id="filter" class="option-set" data-option-key="filter">
        <li><a href="#show-all" data-option-value="*" class="selected">' . __("All", 'lifeline') . '</a></li>';
        $taxonomies = sh_get_categories(array('taxonomy' => 'portfolio_category'));
        foreach ($taxonomies as $tax_id => $tax_name):
            if ($tax_id != 'all') :
                $output .= '<li><a href="#category' . $tax_id . '" data-option-value=".category' . $tax_id . '">' . $tax_name . '</a></li>';
            endif;
        endforeach;
        $output .= '</ul></div></section>';
        $output .= '<div class="row">
        <div id="portfolio" class="variable-sizes bounceinup">';
        $loop_count = 1;

        foreach ($portfolios as $portfolio):
            $thumb_size = ($loop_count == 1) ? '370x491' : '370x252';
            $post_term = get_the_terms($portfolio, 'portfolio_category');
            if (!empty($post_term)) : foreach ($post_term as $t) :
                    $output .= '<div class="category' . sh_set($t, 'term_id') . ' ' . $col_class . '">
                        <div class="portfolio">
                            ' . get_the_post_thumbnail(sh_set($portfolio, 'ID'), $thumb_size) . '
                            <div class="port-desc">
                                <h4><a href="' . get_permalink(sh_set($portfolio, 'ID')) . '">' . get_the_title(sh_set($portfolio, 'ID')) . '</a></h4>
                                <p>' . substr(strip_tags(sh_set($portfolio, 'post_content')), 0, 200) . '</p>
                            </div>
                        </div>
                    </div>';
                endforeach;
            endif;
            $loop_count++;
            if ($loop_count > 2)
                $loop_count = 1;
        endforeach;
        $output .= '</div></div></div>';
        ob_start();
        dynamic_sidebar($sidebar);
        $dynamic_sidebar = ob_get_contents();
        ob_end_clean();
        $output .= '<div class="sidebar col-md-3 ">' . $dynamic_sidebar . '</div>';
        $output .= '</div>
        <script>
            jQuery(document).ready(function($){
            jQuery(function($){
              var $portfolio = $(\'#portfolio\');
            
              $portfolio.isotope({
                masonry: {
                  columnWidth: 1
                }
              });
              var $optionSets = $(\'#options .option-set\'),
                  $optionLinks = $optionSets.find(\'a\');
              $optionLinks.click(function(){
                var $this = $(this);
                // don\'t proceed if already selected
                if ( $this.hasClass(\'selected\') ) {
                  return false;
                }
                var $optionSet = $this.parents(\'.option-set\');
                $optionSet.find(\'.selected\').removeClass(\'selected\');
                $this.addClass(\'selected\');
                var options = {},
                    key = $optionSet.attr(\'data-option-key\'),
                    value = $this.attr(\'data-option-value\');
                // parse \'false\' as false boolean
                value = value === \'false\' ? false : value;
                options[ key ] = value;
                if ( key === \'layoutMode\' && typeof changeLayoutMode === \'function\' ) {
                  // changes in layout modes need extra logic
                  changeLayoutMode( $this, options )
                } else {
                  // otherwise, apply new options
                  $portfolio.isotope( options );
                }
                return false;
              });
            });
            });
            </script>
        ';

        return $output;
    }

    function video_gallery($atts, $contents = null) {
        extract(shortcode_atts(array(
            'cols' => '',
            'play_options' => '',
            'links' => '',
                        ), $atts)
        );

        $vid_links = explode(',', $links);
        
        $output = '';

        $output .= '<div class="gallery-content">
                    <div class="row">';
        foreach ($vid_links as $vid):
            
            $video_data = lifeline_vd_details($vid);
            
            $host = explode('.', str_replace('www.', '', strtolower(parse_url($vid, PHP_URL_HOST))));
            $host = isset($host[0]) ? $host[0] : $host;
            //printr($play_options);
            if ($host=='youtube'):
                $image_url=sh_set(sh_set($video_data, 'thumbnail'),'url');
            else:
                $image_url=sh_set($video_data, 'thumbnail');
            endif;
            $output .= '<div class="col-md-' . $cols . '">';
            $output .= '';
            $output .= ($play_options == 'simple') ?
                    '<div class="gallery-video">' . sh_set($video_data, 'embed_video') . '</div>' : '<div class="gallery-image"><img src="' . $image_url . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
            <span>' . sh_set($video_data, 'title') . '</span>
            <div class="image-lists"><ul><li><a class="video-popup html5lightbox" href="' . esc_attr(sh_set($video_data, 'video')) . '" data-group="group1"  ></a></li></ul></div>
                        </div>';
            $output .= '<h3 class="image-title"><a href="#"  >' . sh_set($video_data, 'title') . '</a></h3>
                    </div>';
        endforeach;
        $output .= '</div></div>';
        return $output;
    }

    function leave_message($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Leave A Message',
            'text' => '',
                        ), $atts));
        $output = '<div class="message-box">
                    <div class="message-box-title">         
                        <span><i class="icon-envelope-alt"></i></span>
                        <p>' . $title . '</p>
                        <i class="icon-angle-up icon-angle-down"></i>
                    </div>
                    <div class="message-form" style="display: none;">
                        <p>' . $text . '</p>
                        <form id="lifeline_contactform_2" name="contactform" action="' . admin_url('admin-ajax.php?action=dictate_ajax_callback&subaction=sh_message_form_submit') . '" method="post">
                            <div class="msgs"></div>
                            <input type="text" placeholder="' . __("Name", 'lifeline') . '" value="" size="30" id="name" class="form-control" name="contact_name">
                            <input type="text" placeholder="' . __("Email", 'lifeline') . '" value="" size="30" id="email" class="form-control" name="contact_email">
                            <textarea placeholder="' . __("Your Message", 'lifeline') . '" class="form-control" id="comments" rows="3" name="contact_message"></textarea>
                            <input type="submit" value="' . __("SEND MESSAGE", 'lifeline') . '" id="submit" class="submit-btn submit">
                            </form>
                    </div>
                </div>';
        $output .= '';
        return $output;
    }

    function qoutes_slider($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => 4,
                        ), $atts));

        $options = get_option('lifeline');
        $output = '';

        $output .= '<section class="block">
                        <div class="client-reviews">
                            <ul class="slides">
                    ';
        for ($i = 0; $i <= $number; $i++) {
            $output .= '<li class="">
                            <div class="reviews effect5">
                                <h3><span>"</span>
                                ' . sh_set(sh_set($options, 'qoutation_text'), $i) . '
                                <span>"</span></h3>
                            </div>
                        </li>';
        }
        $output .= '</ul>
            </div>
        </section>
        <script>
        jQuery(document).ready(function($){
          $(\'.client-reviews\').flexslider({
            animation: "fade",
            animationLoop: false,
            slideShow:true,
            controlNav: false,  
            maxItems: 1,
            pausePlay: false,
            mousewheel:false,
            start: function(slider){
              $(\'body\').removeClass(\'loadings\');
            }
            }); 
        });
        </script>
        ';

        return $output;
    }

    function issues_we_work($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => 4,
            'sort_by' => '',
            'sorting_order' => '',
            'category' => '',
                        ), $atts));

        $args = array( 'post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order );
        if ($category) {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $category));
            }
        }
		$args2 = 'post_type=dict_causes&posts_per_page='.$number.'&orderby='.$sort_by.'&order='.$sorting_order;
        query_posts($args2);
	    $output = '';
	    if ( have_posts() ) :
		    while ( have_posts() ) : the_post();
			    $output .= '<div class="issue">
                        ' . get_the_post_thumbnail(get_the_ID(), "150x150") . '
                        <h4><a href="'.get_the_permalink().'">' . get_the_title(get_the_ID()) . '</a></h4>
                        <p>' . wp_trim_words( get_the_content(), 40, '...' ) . '</p>
                        <a class="btn-don" data-url="'.get_the_permalink().'" data-type="post" data-security="91be1a83e2" data-id="'.get_the_ID().'" data-toggle="modal" data-target="#myModal"  >Donate Now</a>
                  
                  </div>';
		    endwhile;
	    else :
		    echo wpautop( 'Sorry, no posts were found' );
	    endif;

        wp_reset_query();

        $output .= '';
        return $output;
    }

    function latest_news_slider($atts, $content = null) {
        extract(shortcode_atts(array(
            'section_title' => '',
            'heading_style' => 'simple',
            'number' => 4,
            'sort_by' => '',
            'sorting_order' => '',
            'category' => '',
                        ), $atts));
        //print_r($category); exit('aaa');

        $args = array('post_type' => 'post', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $blog_posts = get_posts($args);
        $posts_chunks = array_chunk($blog_posts, 4);

        $output = '';

        $output .= '<section><div class="row"><div class="col-md-12">';

        $output .= '<div class="latest-news">
                        <ul class="slides">';
        foreach ($posts_chunks as $chunk):
            $output .= '<li><div class="row">';
            foreach ($chunk as $c):
	            //print_r(); exit;
                $output .= '<div class="col-md-3">
                    <div class="news">
                        <div class="news-image">
                            ' . get_the_post_thumbnail(sh_set($c, 'ID'), "270x155") . '
                            <a class="html5lightbox" href="'.get_the_post_thumbnail_url(sh_set($c, 'ID')).'"><i class="icon-search"></i></a>
                        </div>
                        <h3>' . get_the_title(sh_set($c, 'ID')) . '</h3>
                        <p>' . substr(strip_tags(sh_set($c, 'post_content')), 0, 50) . '</p>
                    </div>
                </div>';
            endforeach;
            $output .= '</div></li>';
        endforeach;
        $output .= '</ul></div></div></div></section>
        <script>
        jQuery(document).ready(function($){
         $(\'.latest-news\').flexslider({
            animation: "slide",
            animationLoop: false,
            slideShow:false,
            controlNav: true,   
            maxItems: 1,
            pausePlay: false,
            mousewheel:false,
            start: function(slider){
              $(\'body\').removeClass(\'loading\');
            }
            }); 
        });
        </script>
        ';
        return $output;

    }

    function projects_slider($atts, $content = null) {
        extract(shortcode_atts(array(
            'section_title' => '',
            'heading_style' => 'simple',
            'number' => 4,
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'category' => '',
                        ), $atts));

        //printr($category);
        $args = array('post_type' => 'dict_project', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category) {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $posts = get_posts($args);

        $chunks = array_chunk($posts, 2);
        $output = '';

        $output .= '<section>';
        $output .= '<div class="ongoing-projects"><div class="row"><ul class="slides">';
        foreach ($chunks as $chunk):
            $output .= '<li>
                    <div class="row">';
            foreach ($chunk as $c):
                $project_meta = get_post_meta(sh_set($c, 'ID'), '_dict_project_settings', true);
                $video = sh_set((array) sh_set($project_meta, 'videos'), 0);
                $video_data = sh_grab_video($video, '');
                $project_thumb = ($video) ?
                        '<img height="252" width="270" src="' . sh_set($video_data, 'thumb') . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
        <a class="html5lightbox" href="http://player.vimeo.com/video/' . sh_set($video_data, 'id') . '?color=ffffff"  >
            <i class="icon-play"></i>
        </a>' : '<div class="project-image-style"><a href="'.get_permalink(sh_set($c, 'ID')).'">'. get_the_post_thumbnail($c->ID, array(270, 252)).'</a></div>';
                $output .= '
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="ongoing-project-img">
                                        ' . $project_thumb . '
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="ongoing-project-detail">
                                        <h3>' . get_the_title(sh_set($c, 'ID')) . '</h3>
                                        ' . substr(strip_tags(sh_set($c, 'post_content')), 0, 200) . '
                                        <a href="' . get_permalink(sh_set($c, 'ID')) . '"  >' . __("Read More", 'lifeline') . '</a>
                                    </div>
                                </div>
                            </div>
                        </div>';
            endforeach;
            $output .= '
                    </div>
                </li>';
        endforeach;
        $output .= '</ul>
                </div>
            </div>
        </section>
        <script>
        jQuery(document).ready(function($){
         $(\'.ongoing-projects\').flexslider({
            animation: "slide",
            animationLoop: false,
            slideShow:false,
            controlNav: false,  
            maxItems: 1,
            pausePlay: false,
            mousewheel:false,
            start: function(slider){
              $(\'body\').removeClass(\'loading\');
            }
            });
        });
        </script>
        ';

        return $output;
    }

    function welcome_box($atts, $content = null) {
        extract(shortcode_atts(array(
            'h_txt' => '',
            'h_desc' => '',
            'button_txt' => 'PURCHASE NOW',
            'button_url' => '',
                        ), $atts));

        $output = '';
        $output .= '<div class="welcome-box">
                        <h2>' . $h_txt . '</h2><p>' . $h_desc . '</p>                   
                        <a   href="' . $button_url . '" target="_blank"><span>' . $button_txt . '</span></a>                 
                    </div>';
        return $output;
    }

    function parallax_video($atts, $content = null) {
        extract(shortcode_atts(array(
            'video_id' => '',
                        ), $atts));

        $output = '';
        $videolink=(is_ssl())?'https://player.vimeo.com/video/' . $video_id:'http://player.vimeo.com/video/' . $video_id ;
        $output .= '<div id="para_vid" class="parallax-video">
                        <iframe name="video-iframe" src="'.$videolink . '?autoplay=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;loop=1" width="1400" height="425"></iframe>
                    </div>';
        return $output;
    }

    function causes_with_carousel($atts, $content = null) {
        extract(shortcode_atts(array(
            'section_title' => '',
            'section_desc' => '',
            'number' => 4,
            'category' => '',
            'sort_by' => '',
            'sorting_order' => '',
            'use_as' => 'Heading',
                        ), $atts));

        $output = '';
        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category) {
            /*printr($category);*/
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $category));
            } elseif($category=='all') {
                $args['tax_query']= '';
            }  else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $social = get_option('lifeline');
        $post = new WP_Query($args);
        $output .= '<div class="full-section">';
        if ($use_as == 'Heading'):
            $output .= '<div class="full-title">
                        <div class="container">
                            <span class="title-icon"><i class="icon-lightbulb"></i></span>
                            <h2>' . $section_title . '</h2>
                            <span>' . $section_desc . '</span>
                        </div>
                    </div>';
        endif;
        $output .= '<div class="big-carousel">
                        <ul class="slides">';
        while ($post->have_posts()): $post->the_post();
            $img = wp_get_attachment_image_src(get_the_id(), 'full');
            $output .= '<li>
                            <div class="big-picture">
                                <div class="fixed" style="background:url(' . $img[0] . ');"></div>';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_id(), 'full');
            endif;
            $output .= '<div class="short-desc">
                            <h4>' . get_the_title() . '</h4>
                            <p>' . substr(get_the_content(), 0, 100) . '</p>
                            <a href="' . get_the_permalink() . '"  >' . __('Read more', 'lifeline') . '</a>
                            <ul>
                                <li><a href="' . sh_set($social, 'contact_facebook') . '"  ><i class="icon-facebook"></i></a></li>
                                <li><a href="' . sh_set($social, 'contact_linkedin') . '"  ><i class="icon-linkedin"></i></a></li>
                                <li><a href="' . sh_set($social, 'contact_gplus') . '"  ><i class="icon-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </li>';
        endwhile;
        wp_reset_query();
        wp_reset_postdata();
        $output .='</ul>
                    </div>
                </div>';
        $output .='
            <script>
                jQuery(window).load(function(){
                  jQuery(".big-carousel").flexslider({
                    animation: "fade",
                    animationLoop: false,
                    slideShow:false,
                    controlNav: false,  
                    maxItems: 1,
                    pausePlay: false,
                    mousewheel:false,
                    start: function(slider){
                      jQuery("body").removeClass("loading");
                    }
                    });
                });
            </script>';
        return $output;
    }

    function services_with_pictures($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => 4,
            'layout' => 'title_only',
            'display' => 'icon',
            'btn_text' => 'Read More',
            'sort_by' => 'date',
            'sorting_order' => 'asc',
            'limit' => '100',
                        ), $atts));

        $output = '';
        //printr($atts);
        global $post;
        //$args = array('post_type' => 'dict_services', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        query_posts('post_type=dict_services&posts_per_page=' . $number . '&orderby=' . $sort_by . '&order=' . $sorting_order . '&limit=' . $limit);
        $output .= '<div class="row">';
        while (have_posts()): the_post();
            $settings = get_post_meta(get_the_ID(), '_dict_services_settings', true);
            //printr($settings);
            $con = explode(' ', get_the_title(), 2);
            $output .= '<div class="col-md-3">
                            <div class="service">';
            if ($layout == "title_only") {
                if ($display == "icon") {
                    $output .= '<i class="' . sh_set($settings, 'font_awesome') . '"></i>';
                } else {
                    if (has_post_thumbnail()): $output .= get_the_post_thumbnail($post->ID, array(270, 155));
                    endif;
                }

                $output .= '<h4>' . sh_set($con, '0') . ' <span>' . sh_set($con, '1') . '</span></h4>
                                            <a href="' . get_the_permalink() . '"  >' . $btn_text . '</a>';
            }else {

                if ($display == "icon") {
                    $output .= '<i class="' . sh_set($settings, 'font_awesome') . '"></i>';
                } else {
                    if (has_post_thumbnail()): $output .= get_the_post_thumbnail($post->ID, array(270, 155));
                    endif;
                }
//printr($limit);
                $output .= '<h4>' . sh_set($con, '0') . ' <span>' . sh_set($con, '1') . '</span></h4>
                                            <p>' . sh_excerpt(get_the_content(), $limit) . '</p>
                                            <a href="' . get_the_permalink() . '"  >' . $btn_text . '</a>';
            }

            $output .= '</div>
                        </div>';
        endwhile;
        wp_reset_query();
        wp_reset_postdata();
        $output .= '</div>';

        return $output;
    }

    function charity_statics($atts, $content = null) {
        extract(shortcode_atts(array(
            'heading' => '',
            'desc' => '',
            'box1' => '',
            'box1_txt' => '',
            'box1_bg' => '',
            'box2' => '',
            'box2_txt' => '',
            'box2_bg' => '',
            'box3' => '',
            'box3_txt' => '',
            'box3_bg' => '',
            'box4' => '',
            'box4_txt' => '',
            'box4_bg' => '',
                        ), $atts));

        $output = '';

        $output .= '<div class="row">
                        <div class="col-md-4">
                            <div class="mission">
                                <h4>' . $heading . '</h4>
                                <p>' . $desc . '</p>
                            </div>
                        </div>
                    <div class="col-md-8">
                        <div class="row">
                        <div class="remove-ext">';
        if ($box1 != "") {
            $img = wp_get_attachment_image_src($box1_bg, 'thumbnail');
            $output .= '<div class="col-md-3">
                                            <div class="counting">
                                                <div style="background:url(' . $img[0] . ')"></div>
                                                <h3 class="count">' . $box1 . '</h3>
                                                <span>' . $box1_txt . '</span>
                                            </div>
                                        </div>';
        }

        if ($box2 != "") {
            $img = wp_get_attachment_image_src($box2_bg, 'thumbnail');
            $output .= '<div class="col-md-3">
                                            <div class="counting">
                                                <div style="background:url(' . $img[0] . ')"></div>
                                                <h3 class="count">' . $box2 . '</h3>
                                                <span>' . $box2_txt . '</span>
                                            </div>
                                        </div>';
        }

        if ($box3 != "") {
            $img = wp_get_attachment_image_src($box3_bg, 'thumbnail');
            $output .= '<div class="col-md-3">
                                            <div class="counting">
                                                <div style="background:url(' . $img[0] . ')"></div>
                                                <h3 class="count">' . $box3 . '</h3>
                                                <span>' . $box3_txt . '</span>
                                            </div>
                                        </div>';
        }

        if ($box4 != "") {
            $img = wp_get_attachment_image_src($box4_bg, 'thumbnail');
            $output .= '<div class="col-md-3">
                                            <div class="counting">
                                                <div style="background:url(' . $img[0] . ')"></div>
                                                <h3 class="count">' . $box4 . '</h3>
                                                <span>' . $box4_txt . '</span>
                                            </div>
                                        </div>';
        }

        $output .= '</div></div></div></div>';

        return $output;
    }

    function our_mission_carousel($atts, $content = null) {
        extract(shortcode_atts(array(
            'post_type' => '',
            'number' => 4,
            'sort_by' => '',
            'sorting_order' => '',
            'category_post'=>'',
            'category2'=>'',
            'category7'=>'',
            'category3'=>'',
            'category4'=>'',
            'category5'=>'',
            'category6'=>''
            ), 
        $atts));
        
        if($post_type=='post'):
            $cat=$category_post;
            $tax='category';
        elseif($post_type == 'dict_testimonials') :
             $cat=$category2;
             $tax='testimonial_category';
        elseif($post_type == 'dict_causes') :
            $cat=$category7;
            $tax='cause_category';
        elseif($post_type == 'dict_project') :
             $cat=$category3;
             $tax='project_category';
        elseif($post_type == 'dict_event') :
            $cat=$category4;
            $tax='event_category';
        elseif($post_type == 'dict_portfolio') :
            $cat=$category5;
            $tax='portfolio_category';
        elseif($post_type == 'dict_team') :
            $cat=$category6;
            $tax='team_category';
        
         else:
            $cat='';
            $tax='';

        endif;
       
        $output = '';
        $counter = 1;
        
        $args = array('post_type' => $post_type, 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);

        if ($cat) {
            if ($cat != 'all') {
                if (is_numeric($cat)) {
                    $args['tax_query'] = array(array('taxonomy' => $tax, 'field' => 'term_id', 'terms' => $cat));
                } else {
                    $args['tax_query'] = array(array('taxonomy' =>$tax, 'field' => 'slug', 'terms' => $cat));
                }
            }
        }
       // printr( $args);
        $posts = new WP_Query($args);
        //print_r($posts); exit('aa');
        $output .= '<div class="mission-carousel">
                        <ul class="slides"><li><div class="row">';
        if ($posts->have_posts()): while ($posts->have_posts()): $posts->the_post();
                $output .= '';
                $output .='<div class="col-md-4">
                                            <div class="single-mission">
                                                <div class="mission-img">
                                                    <a href="' . get_the_permalink() . '"  >';
                if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_id(), array(270, 155));
                endif;
                $output .='</div>
                                                <h3><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h3>
                                                <p>' . substr(strip_tags(get_the_content()), 0, 100) . '</p>
                                            </div>
                                        </div>';
                if ($counter % 3 == 0 && $counter != $number): $output.='</div></li><li><div class="row">';
                endif;
                $counter++;
            endwhile;
        endif;
        wp_reset_query();
        $output .= '</li></ul>
                    </div>';
        $output .= '<script>
                    jQuery(window).load(function(){
                      jQuery(".mission-carousel").flexslider({
                        animation: "slide",
                        animationLoop: false,
                        slideShow:false,
                        controlNav: false,  
                        maxItems: 1,
                        pausePlay: false,
                        mousewheel:false,
                        start: function(slider){
                          jQuery("body").removeClass("loading");
                        }
                        });
                    });
                    </script>';
        return $output;
    }

    function sponsor($atts, $content = null) {
        extract(shortcode_atts(array(
            'desc' => '',
            'image' => '',
            'btn_text' => '',
                        ), $atts));
        $img = wp_get_attachment_image_src($image, array(270, 155));

        $output = '<div class="sponsor">
                    <img src="' . $img[0] . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
                    <div class="sponsor-desc">';
                        $output .= (function_exists('lifeline_decrypt') && $desc) ? '<p>' . rawurldecode(lifeline_decrypt($desc)) . '</p>' : '';
                    $output .= '<a data-toggle="modal" data-target="#myModal" data-url="' . get_permalink() . '" data-type="general" class="btn-don donate-btn"    data-security="'. wp_create_nonce(LIFELINE_KEY) .'">' . $btn_text . '</a>
                    </div>
                </div>';
        return $output;
    }

    function creative_recent_news($atts, $content = null) {
        extract(shortcode_atts(array(
            'cols'   => '',
            'number' => '',
            'title' => __('Recent News', 'lifeline'),
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'DESC',
            'heading_style' => 'simple',
            'limit' => 50,
                        ), $atts)
        );
//printr($atts);
        $output = '';
        //$term = get_category_by_slug(strtolower(str_replace(' ', '-', $category)));
        wp_reset_query();
        $args = array('post_type' => 'post', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);

        if ($category != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => $category));
            }
        }
        $posts = new WP_Query($args);

        $output .= '<div class="remove-ext">
                        <div class="row">';
        $cols = $cols ? $cols : 'col-md-6';
        if ($posts->have_posts()): while ($posts->have_posts()): $posts->the_post();
                $output .='<div class=" '. esc_attr( $cols ) .' ">
                    <div class="recent-news">
                        <div class="row">
                            <div class="col-md-5">
                                <a class="news-img" href="' . get_the_permalink() . '"  >';
                if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_id(), array(370, 252));
                endif;
                $output .='</a></div>
                    <div class="col-md-7">
                        <h4><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h4>
                        <p>' . substr(get_the_content(), 0, $limit) . '</p>
                    </div>
                </div>
            </div>
        </div>';
            endwhile;
        endif;
        wp_reset_query();
        $output .= '</div>
                    </div>';

        return $output;
    }

    function featured_posts($atts, $content = null) {
        extract(shortcode_atts(array(
            'post_type' => '',
            'number' => '',
            'sort_by' => '',
            'post_ids' => '',
            'sorting_order' => '',
            'btn_text' => 'Read More',
            'limit' => '100',

                         ), $atts));
//printr($atts);
        $post_ids = explode(',', $post_ids);
        $output = '';
        $output .= '<div class="featured-posts">
                        <div class="row">';
        if (!empty($post_ids)) : foreach ($post_ids as $p) :
                $args = array('post_type' => $post_type, 'p' => $p, 'orderby' => $sort_by, 'order' => $sorting_order, 'posts_per_page' => $number,);
                $posts = new WP_Query($args);
                if ($posts->have_posts()): while ($posts->have_posts()): $posts->the_post();
	                //$con = explode(' ', get_the_title(), 2);
                        $output .= '<div class="col-md-4">
                                <div class="featured">
                                    <div class="featured-img">';
                        if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_id(), array(370, 252));
                        endif;
                        $output .= '<a href="' . get_the_permalink() . '"  ><i class="icon-link"></i></a>
                                    </div>
                                    <div class="featured-details">
                                        <h3><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h3>
                                        <p>' . sh_excerpt(get_the_content(), $limit) . '</p>
                                         <a href="' . get_the_permalink() . '"  >' . $btn_text . '</a>
                                    </div>
                                </div>
                            </div>';
                    endwhile;
                endif;
            endforeach;
        endif;
        wp_reset_query();

        $output .= '</div>
                    </div>';
        return $output;
    }

    function post_carousel($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'category' => '',
            'sort_by' => '',
            'sorting_order' => '',
            'limit'  => 50,
                        ), $atts));
        $output = '';
        
        if($category!='all') {
               $args = array('post_type' => 'post', 'posts_per_page' => $number, 'category_name' => $category, 'orderby' => $sort_by, 'order' => $sorting_order);
            }else{
                $args = array('post_type' => 'post', 'posts_per_page' => $number,'orderby' => $sort_by, 'order' => $sorting_order);
            } //if ($cat != '') $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'id', 'terms' => $cat));
        query_posts($args);
        $output .= '<div class="ongoing-projects">
                        <div class="row">
                            <ul class="slides">';
        while (have_posts()): the_post();
            $meta = get_post_meta(get_the_ID(), '_post_settings', true);
            $output .= '<li>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="ongoing-project-img">';
            if (sh_set($meta, 'videos')) {


                if (sh_set($meta['videos'], '0') != "") {
                    $video_link = sh_set($meta['videos'], '0');
                    $url = 'http://vimeo.com/' . $video_link;
                    $video_data = sh_grab_video($url, '');
                    $output .= '<img src="' . sh_set($video_data, 'thumb') . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />';
                    $output .= '<a class="html5lightbox" href="http://player.vimeo.com/video/' . sh_set($video_data, 'id') . '?color=ffffff"  ><i class="icon-play"></i></a>';
                }
            }

            $output .= '</div>
                            </div>
                            <div class="col-md-7">
                                <div class="ongoing-project-detail">
                                    <h3>' . get_the_title() . '</h3>
                                    <p>' . sh_excerpt(get_the_content(), $limit) . '</p>
                                    <a href="' . get_the_permalink() . '"  >' . __('Read More', 'lifeline') . '</a>
                                </div>
                            </div>
                        </div>
                    </li>';
        endwhile;
        wp_reset_query();
        $output .= '</ul>
                        </div>
                    </div>';
        $output .= '<script>
                                jQuery(window).load(function(){
                                  jQuery(".ongoing-projects").flexslider({
                                    animation: "slide",
                                    animationLoop: false,
                                    slideShow:false,
                                    controlNav: false,  
                                    maxItems: 1,
                                    pausePlay: false,
                                    mousewheel:false,
                                    start: function(slider){
                                     jQuery("body").removeClass("loading");
                                    }
                                });
                            });
                            </script>';
        return $output;
    }

    function blockquote_carousel($atts, $content = null) {
        extract(shortcode_atts(array(
            'desc' => '',
                        ), $atts)
        );

        $output = '';
        $output .=' <div class="client-reviews">
                        <ul class="slides">
                            ' . do_shortcode($content) . '
                        </ul>
                    </div>';
        $output .= "<script>
                        jQuery(window).load(function(){
                            jQuery('.client-reviews').flexslider({
                            animation: 'fade',
                            animationLoop: true,
                            slideShow:true,
                            controlNav: false,  
                            maxItems: 1,
                            pausePlay: false,
                            mousewheel:false,
                            start: function(slider){
                              jQuery('body').removeClass('loading');
                            }
                            });
                        });
                    </script>";

        return $output;
    }

    function blockquote_text($atts, $content = null) {
        extract(shortcode_atts(array(
            'acc_content' => '',
                        ), $atts)
        );

        $output = '';
        $output .= '<li>
                        <div class="reviews effect5">
                            <h3><span>"</span>
                                ' . $acc_content . '
                            <span>"</span></h3>
                        </li>';

        return $output;
    }

    function causes_with_thumb($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'col' => '',
            'cat' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
                        ), $atts)
        );

        $output = '';
        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($cat != '') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $cat));
            }
        }
	    $args2 = 'post_type=dict_causes&posts_per_page='.$number.'&orderby='.$sort_by.'&order='.$sorting_order;
        $query = new WP_Query($args2);
        while ($query->have_posts()): $query->the_post();
            $output .= '<div class="col-md-'.$col.'"><div class="issue">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_ID(), array(80, 80));
            endif;
            $output .= '<h4><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h4>
                        </div></div>';
        endwhile;
        wp_reset_query();

        return $output;
    }

    function latest_news_carousel($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'cat' => '',
            'sort_by' => '',
            'sorting_order' => '',
                        ), $atts));
        $output = '';
        $counter = 1;
        $args = array('post_type' => 'post', 'posts_per_page' => $number,  'orderby' => $sort_by, 'order' => $sorting_order);

        if ($cat != 'all') {
            if (is_numeric($cat)) {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'term_id', 'terms' => $cat));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => $cat));
            }
        }

        query_posts($args);
        $output .= '<div class="latest-news">
                        <ul class="slides"><li><div class="row">';
        if (have_posts()): while (have_posts()): the_post();
                $output .= '';
                $output .='<div class="col-md-4">
                                            <div class="news">
                                                <div class="news-image">';
                if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_id(), array(270, 155));
                endif;
                $output .='<a href="' . get_the_permalink() . '"  ><i class="icon-link"></i></a></div>
                                                <h3>' . get_the_title() . '</h3>
                                                <p>' . substr(get_the_content(), 0, 100) . '</p>
                                            </div>
                                        </div>';
                if ($counter % 3 == 0 && $counter != $number): $output.='</div></li><li>';
                endif;
                $counter++;
            endwhile;
        endif;
        wp_reset_query();
        $output .= '</li></ul>';
        $output .= "<script>
                        jQuery(window).load(function(){
                            jQuery('.latest-news').flexslider({
                                animation: 'slide',
                                animationLoop: true,
                                slideShow:false,
                                controlNav: true,   
                                maxItems: 1,
                                pausePlay: false,
                                mousewheel:false,
                                start: function(slider){
                                  jQuery('body').removeClass('loading');
                                }
                                }); 
                        });
                    </script>";
        return $output;
    }

    function fancy_causes($atts, $content = null) {
        extract(shortcode_atts(array(
            'post_id' => '',
                        ), $atts));
        $output = '';
        $args = array(
            'p' => $post_id,
            'post_type' => 'dict_causes',
            'posts_per_page' => 1,
        );
        query_posts($args);
        while (have_posts()): the_post();
            $Settings = get_post_meta($post_id, '_dict_causes_settings', true);
            $output .= '<div class="fancy-cause">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail($post_id, array(370, 252));
            endif;
            $output .= '<div class="fancy-cause-intro">
                                <i>' . __('in', 'lifeline') . '&nbsp;<a href="' . get_the_permalink() . '"  >' . sh_set($Settings, 'location') . '</a></i>
                                <h3>' . get_the_title() . '</h3>
                                <span><strong>' . sh_set($Settings, 'currency_symbol') . '</strong>&nbsp;' . sh_set($Settings, 'donation_needed') . '&nbsp;<i>' . __('Donation Needed', 'lifeline') . '</i></span>
                            </div>
                            <div class="fancy-cause-hover">
                                <p>' . sh_excerpt(get_the_content(), 150) . '</p>
                                <span>' . __('Help Us:', 'lifeline') . ' <strong>' . sh_set($Settings, 'currency_symbol') . '</strong> <i>' . sh_set($Settings, 'donation_needed') . '</i></span>
                                <span><a class="btn-don" data-url="' . get_permalink() . '" data-type="post" data-id="' . get_the_ID() . '"  data-toggle="modal" data-target="#myModal"  >' . __('Donate Now', 'lifeline') . '</a><a href="' . get_the_permalink() . '"  >' . __('Read More', 'lifeline') . '</a></span>                                            
                            </div>
                        </div>';
        endwhile;
        wp_reset_query();
        return $output;
    }

    function fancy_causes_2($atts, $content = null) {
        extract(shortcode_atts(array(
            'post_id' => '',
                        ), $atts));
        $output = '';
        $args = array(
            'p' => $post_id,
            'post_type' => 'dict_causes',
            'posts_per_page' => 1,
        );
        query_posts($args);
        while (have_posts()): the_post();
            $Settings = get_post_meta($post_id, '_dict_causes_settings', true);
            $output .= '<div class="our-cause">
                            <div class="our-cause-img">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail($post_id, array(370, 252));
            endif;
            $output .= '<a href="' . get_the_permalink() . '" title="' . get_the_title() . '"><i class="icon-link"></i></a></div>
                            <div class="our-cause-detail">
                                <h3>' . get_the_title() . '</h3>
                                <span>' . __('in', 'lifeline') . '&nbsp;<a href="' . get_the_permalink() . '"  >' . sh_set($Settings, 'location') . '</a></span>
                                <p>' . sh_excerpt(get_the_content(), 150) . '</p>
                                <i>' . __('Help Us:', 'lifeline') . ' <span>' . sh_set($Settings, 'currency_symbol') . '</span> <strong>&nbsp;' . sh_set($Settings, 'donation_needed') . '</strong></i>                            
                                <a class="btn-don" data-url="' . get_permalink() . '"  data-security="'. wp_create_nonce(LIFELINE_KEY) .'" data-type="post" data-id="' . get_the_ID() . '" data-toggle="modal" data-target="#myModal"  >' . __('Donate Now', 'lifeline') . '</a>
                            </div>
                        </div>';
        endwhile;
        wp_reset_query();
        return $output;
    }

    function causes_listing_fancy_style($atts, $content = null) {
        extract(shortcode_atts(array(
            'cols' => '',
            'number' => '',
            'cat' => '',
            'sort_by' => 'date',
            'order' => '',
            'limit' => 150,
                        ), $atts));
        $output = '';
        $args = array(
            'post_type' => 'dict_causes',
            'orderby' => $sort_by,
            'order' => $order,
            'showposts' => $number,
        );

        $category = array();
        if ($cat != '') {
            $category = explode(',', $cat);
        }
        if (!empty($category) && sh_set($category, 0) != 'all') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $category));
            } else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $category));
            }
        }

        query_posts($args);
        $size = ($cols == '2') ? '570x353' : '370x252';
        $output .= '<div class="remove-ext"><div class="row">';
        $col_class = ($cols) ? 'col-md-' . $cols : 'col-md-4';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_causes_settings', true);


            $output .= '<div class="' . $col_class . '"><div class="our-cause">
                            <div class="our-cause-img">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail( get_the_ID(), $size );
            endif;
            $output .= '<a href="' . get_the_permalink() . '" title="' . get_the_title() . '"><i class="icon-link"></i></a></div>
                            <div class="our-cause-detail">
                                <h3>' . get_the_title() . '</h3>
                                <span>' . __('in', 'lifeline') . '&nbsp;<a href="' . get_the_permalink() . '"  >' . sh_set($Settings, 'location') . '</a></span>
                                <p>' . sh_excerpt(get_the_content(), $limit) . '</p>
                                <i>' . __('Help Us:', 'lifeline') . ' <span>' . sh_set($Settings, 'currency_symbol') . '</span> <strong>&nbsp;' . sh_set($Settings, 'donation_needed') . '</strong></i>                            
                                <a class="btn-don" data-url="' . get_permalink() . '" data-type="post" data-security="'. wp_create_nonce(LIFELINE_KEY) .'" data-id="' . get_the_ID() . '" data-toggle="modal" data-target="#myModal"  >' . __('Donate Now', 'lifeline') . '</a>
                            </div>
                        </div></div>';
        endwhile;
        $output .= '</div></div>';
        wp_reset_query();
        return $output;
    }

    function donation_parallax_box($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'desc' => '',
            'button_' => __('Take Action', 'lifeline'),
            'bg' => '',
                        ), $atts));
        $output = '';
        $img = wp_get_attachment_image_src($bg, 'full');
        $output .= '<div class="fancy-donation">
                        <img src="' . $img['0'] . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
                        <div class="donation-appeal">
                            ' . $title . '
                            <p>' . $desc . '</p>
                            <a class="btn-don" href="#" data-security="'. wp_create_nonce(LIFELINE_KEY) .'" data-url="' . get_permalink() . '" data-type="general" data-toggle="modal" data-target="#myModal"  >' . $button_ . '</a>
                        </div>
                    </div>';
        return $output;
    }

    function project_carousal_full_page($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
                        ), $atts));
        $output = '';
        $args = array('post_type' => 'dict_project', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != '') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'term_id', 'terms' => $category));
            }elseif($category == 'all') {
                $args .= '';
            } 
            else {
                $args['tax_query'] = array(array('taxonomy' => 'project_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        query_posts($args);
        $output .= '<div class="wide-project-carousel">
                        <ul class="slides">';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_project_settings', true);
            $output .= '<li class="wide-project">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="wide-project-detail">
                                                        <h2><a href="' . get_the_permalink() . '" title="' . get_the_title() . '">' . get_the_title() . '</a></h2>
                                                        <h3>' . sh_set($Settings, 'location') . '</h3>
                                                        <p>' . substr(strip_tags(get_the_content()), 0, 500) . '</p>
                                                        <div class="money-spent"><h5><i>' . sh_set($Settings, 'spent_amount_currency') . '</i>' . sh_set($Settings, 'spent_amount') . '</h5><span>' . __('Money Spent', 'lifeline') . '</span></div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="wide-project-img">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_ID(), '570x570');
            endif;
            $output .= '</div>
                                                </div>
                                            </div>
                                        </li>';
        endwhile;
        wp_reset_query();
        $output .= '</ul></div>';
        $output .= '<script>
                        jQuery(window).load(function(){
                            jQuery(".wide-project-carousel").flexslider({
                                animation: "fade",
                                animationLoop: false,
                                slideShow:false,
                                controlNav: false,  
                                maxItems: 1,
                                pausePlay: false,
                                mousewheel:false,
                                start: function(slider){
                                  jQuery("body").removeClass("loading");
                                }
                                });
                        });
                    </script>';
        return $output;
    }

      function causes_new_style($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
            'c_opt' => 'excerpt',
            'c_limit' => '100',
                        ), $atts));
        $output = '';
        $args = array('post_type' => 'dict_causes', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != '') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'term_id', 'terms' => $category));
            } elseif($category == 'all') {
               
            }else {
                $args['tax_query'] = array(array('taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => $category));
            }
        }
        query_posts($args);

        $output .= '<div class="charity-causes"><div class="row">';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_causes_settings', true);
            if (sh_set($Settings, 'donation_collected') && sh_set($Settings, 'donation_needed')) {
                $percent = ((int) sh_set($Settings, 'donation_collected') / (int) sh_set($Settings, 'donation_needed') ) * 100;
            } else {
                $percent = 0;
            }

            $output .='<div class="col-md-4">
                            <div class="charity-cause">
                                <div class="charity-cause-img">';
            if (has_post_thumbnail()): $output .= '<a href="' . get_the_permalink() . '"  >' . get_the_post_thumbnail(get_the_ID(), '470x318') . '</a>';
            endif;
            $output .='</div>
                       <div class="charity-cause-detail">
                           <h3><a href="' . get_the_permalink() . '" title="' . get_the_title() . '">' . get_the_title() . '</a></h3>';
                                            $output .= '<p>';
                if ($c_opt == 'excerpt') {
                    $output .= get_the_excerpt();
                } elseif ($c_opt == 'full') {
                    $output .= get_the_content(get_the_ID());
                } elseif ($c_opt == 'limit') {
                    $output .= sh_contents(get_the_content(get_the_ID()), $c_limit);
                }
                $output .="</p>";
                                            $output .='<div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: ' . $percent . '%">
                                            </div>
                                            </div>
                                            <span>' . __('Needed Donation', 'lifeline') . ' <i>' . sh_set($Settings, 'currency_symbol') . '' . sh_set($Settings, 'donation_needed') . '</i></span>
                                        </div>
                                        <div class="cause-location">
                                            <p>' . __('In', 'lifeline') . ' <span>' . sh_set($Settings, 'location') . '</span></p>
                                        </div>
                                    </div><!-- Charity Cause -->
                                </div>';
        endwhile;
        wp_reset_query();
        $output .= '</div></div>';

        return $output;
    }

    function urgent_cause_parallax($atts, $content = null) {
        extract(shortcode_atts(array(
            'cause' => '',
            'bg' => '',
                        ), $atts));
        $output = '';
        $args = array('post_type' => 'dict_causes', 'posts_per_page' => 1, 'p' => $cause);
        query_posts($args);
        $img = wp_get_attachment_image_src($bg, 'full');
        $output .= '<div class="charity-causes">';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_causes_settings', true);
            if (sh_set($Settings, 'donation_collected') && sh_set($Settings, 'donation_needed')) {
                $percent = ((int) sh_set($Settings, 'donation_collected') / (int) sh_set($Settings, 'donation_needed') ) * 100;
            } else {
                $percent = 0;
            }

            $output .='<div class="urgent-cause">
                                    <img src="' . $img['0'] . '" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" />
                                    <span><img src="' . get_template_directory_uri() . '/images/speaker.png" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" /></span>
                                    <h3>' . __('Our Urgent', 'lifeline') . ' <span>' . __('Causes', 'lifeline') . '</span></h3>
                                    <h5><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h5>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: ' . $percent . '%">
                                        </div>
                                    </div>
                                    <div class="urgent-progress">
                                        <span>' . __('Needed Donation', 'lifeline') . '</span>
                                        <i>' . round($percent) . '%</i>
                                    </div>
                                                                        <h6>' . esc_html__('Collected Donation', 'lifeline') . '</h6>
                                    <strong><i>' . sh_set($Settings, 'currency_symbol') . '</i>' . sh_set($Settings, 'donation_collected') . ' / <i>' . sh_set($Settings, 'currency_symbol') . '</i>' . sh_set($Settings, 'donation_needed') . '</strong>
                                    <a class="btn-don" href="#" data-id="' . get_the_ID() . '" data-url="' . get_permalink() . '" data-type="post" data-toggle="modal" data-security="'. wp_create_nonce(LIFELINE_KEY) .'" data-target="#myModal"  >' . __('DONATE NOW', 'lifeline') . '</a>
                                </div>';
        endwhile;
        wp_reset_query();
        $output .= '</div>';

        return $output;
    }

    function donation_parallax_full_page($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'subtitle' => '',
            'desc' => '',
            'btn_txt' => __('SPONSOR NOW', 'lifeline'),
                        ), $atts));
        $ext = explode(' ', $title, 2);
        $output = '<div class="simple-parallax">
                    <span>' . $subtitle . '</span>
                    <h3>' . sh_set($ext, '0') . ' <span>' . sh_set($ext, '1') . '</span></h3>
                    <p>' . $desc . '</p>
                    <a href="#" class="btn-don transparent-btn" data-url="' . get_permalink() . '" data-type="general" data-toggle="modal" data-target="#myModal"  >' . $btn_txt . '</a>
                </div>';
        return $output;
    }

    function modern_event_counter($atts, $content = null) {
        extract(shortcode_atts(array(
            'cause' => '',
            'lap' => 'true',
                        ), $atts));
        $args = array('post_type' => 'dict_event', 'posts_per_page' => 1, 'p' => $cause);
        query_posts($args);
        $lap = ( $lap == 'true' ) ? 'overlap' : '';
        $output = '';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
            $start_time = sh_set($Settings, 'start_time');
            $total_time = sh_set($Settings, 'start_date') . ' ' . sh_set($Settings, 'start_time');
            $e_date = new DateTime($total_time);
            $date = $e_date->format('Y/m/d H:i:s');
            //printr($date);
            $output .= '<div class="upcoming-bar ' . $lap . '">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="abt-upcoming-event">
                                <span><img src="' . get_template_directory_uri() . '/images/icon.png" alt="'.esc_attr__( 'Image Not Found', 'lifeline' ).'" /></span>
                                <h3><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h3>
                                <ul>
                                    <li><a href="' . get_author_posts_url(get_the_author_meta('ID')) . '"  ><i class="icon-user"></i> ' . __('By', 'lifeline') . ' ' . get_the_author_meta('display_name') . '</a></li>
                                    <li><a href="'.esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d'))).'"  ><i class="icon-calendar"></i> ' . $e_date->format('M d, Y') . '</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="upcoming-counter">
                                <ul class="countdown time' . get_the_ID() . '">
                                    <li><p class="days_ref">days</p><span class="days">00</span></li>
                                    <li><p class="hours_ref">hours</p><span class="hours">00</span></li>
                                    <li><p class="minutes_ref">minutes</p><span class="minutes">00</span></li>
                                    <li><p class="seconds_ref">seconds</p><span class="seconds">00</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>';
            $output .= '<script>
                        jQuery(document).ready(function(){
                            jQuery(".countdown.time' . get_the_ID() . '").downCount({
                                date: "' . $date . '",
                                offset: ' . get_option('gmt_offset') . '
                            }); 
                        });
                    </script>';
        endwhile;
        wp_reset_query();

        return $output;
    }

    function charity_events_new($atts, $content = null) {
        extract(shortcode_atts(array(
            'number' => '',
            'category' => '',
            'sort_by' => 'date',
            'sorting_order' => 'ASC',
                        ), $atts));
        $output = '';
        $args = array('post_type' => 'dict_event', 'posts_per_page' => $number, 'orderby' => $sort_by, 'order' => $sorting_order);
        if ($category != '') {
            if (is_numeric($category)) {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'term_id', 'terms' => $category));
            } elseif($category=='all') {
                
            }else {
                $args['tax_query'] = array(array('taxonomy' => 'event_category', 'field' => 'slug', 'terms' => $category));
            }
        }
     
        query_posts($args);

        $output .= '<div class="charity-events"><div class="row">';
        while (have_posts()): the_post();
            $Settings = get_post_meta(get_the_ID(), '_dict_event_settings', true);
            $date_obj = new DateTime(sh_set($Settings, 'start_date'));
            $event_date = $date_obj->format('M d, Y');
            $output .='<div class="col-md-4">
                                    <div class="charity-event">
                                        <div class="charity-event-img">';
            if (has_post_thumbnail()): $output .= get_the_post_thumbnail(get_the_ID(), '570x570');
            endif;
            $output .='<a href="' . get_the_permalink() . '"  >' . __('Read More', 'lifeline') . '</a>
                                        </div>
                                        <div class="charity-event-detail">
                                            <i>' . $event_date . '</i>
                                            <h3><a href="' . get_the_permalink() . '"  >' . get_the_title() . '</a></h3>
                                            <span><i class="icon-map-marker"></i> ' . sh_set($Settings, 'address') . '</span>
                                            <p>' . substr(strip_tags(get_the_content()), 0, 80) . '</p>
                                        </div>
                                    </div>
                                </div>';
        endwhile;
        wp_reset_query();
        $output .= '</div></div>';

        return $output;
    }

}
