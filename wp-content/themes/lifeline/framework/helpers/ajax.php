<?php

class SH_Ajax {

    function __construct() {
        add_action('wp_ajax_dictate_ajax_callback', array($this, 'ajax_handler'));
        add_action('wp_ajax_nopriv_dictate_ajax_callback', array($this, 'ajax_handler'));
        add_action('wp_ajax_nopriv_lifeline_newsletter_module', array(__CLASS__, 'lifeline_newsletter_module'));
        add_action('wp_ajax_lifeline_newsletter_module', array(__CLASS__, 'lifeline_newsletter_module'));
    }

    function ajax_handler() {
        $method = sh_set($_REQUEST, 'subaction');

        if (method_exists($this, $method))
            $this->$method();

        exit;
    }

    static public function lifeline_newsletter_module() {

        if (class_exists('SH_Newsletter')) {
            if (isset($_POST['action']) && $_POST['action'] == 'lifeline_newsletter_module') {
                SH_Newsletter::lifeline_web_subscribepopup_submit($_POST);
                exit;
            }
        }
    }

    function sh_contact_form_submit() {
        
        check_ajax_referer( LIFELINE_KEY, 'lifeline_securitykey' );
        
        if (!count($_POST))
            return;

        _load_class('validation', 'helpers', true);
        $t = &$GLOBALS['_sh_base'];
        $settings = get_option('lifeline');

        $t->validation->set_rules('contact_name', '<strong>' . __('Name', 'lifeline') . '</strong>', 'required|min_length[4]|max_lenth[30]');
        $t->validation->set_rules('contact_email', '<strong>' . __('Email', 'lifeline') . '</strong>', 'required|valid_email');
        $t->validation->set_rules('contact_message', '<strong>' . __('Message', 'lifeline') . '</strong>', 'required|min_length[5]');
        if (sh_set($settings, 'captcha_status') == 'true' && function_exists( 'lifeline_return_server_info' )) {
            $privatekey = sh_set($settings, 'captcha_secret_key');
            $resp = recaptcha_check_answer($privatekey, lifeline_return_server_info("REMOTE_ADDR"), $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        }

        $messages = '';

        if ($t->validation->run() !== FALSE && empty($t->validation->_error_array)) {

            $name = $t->validation->post('contact_name');
            $email = $t->validation->post('contact_email');
            $message = $t->validation->post('contact_message');
            $contact_to = ( sh_set($settings, 'contact_email') ) ? sh_set($settings, 'contact_email') : get_option('admin_email');

            $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
            if( function_exists( 'lifeline_mail_function' ) ) {
                lifeline_mail_function($contact_to, __('Contact Us Message', 'lifeline'), $message, $headers);
            }
            $message = sh_set($settings, 'success_message') ? $settings['success_message'] : sprintf(__('Thank you <strong>%s</strong> for using our contact form! Your email was successfully sent and we will be in touch with you soon.', 'lifeline'), $name);

            $messages = '<div class="alert alert-success">
							<p class="title">' . __('SUCCESS! ', 'lifeline') . $message . '</p>
						</div>';
        } else {
            if (is_array($t->validation->_error_array)) {
                foreach ($t->validation->_error_array as $msg) {
                    $messages .= '<div class="alert alert-error">
										<p class="title">' . __('Error! ', 'lifeline') . $msg . '</p>
									</div>';
                }
            }
        }
        echo wp_kses_post( $messages );
        exit;
    }

    function sh_message_form_submit() {
        if (!count($_POST))
            return;

        _load_class('validation', 'helpers', true);
        $t = &$GLOBALS['_sh_base'];
        $settings = get_option('lifeline');

        $t->validation->set_rules('contact_name', '<strong>' . __('Name', 'lifeline') . '</strong>', 'required|min_length[4]|max_lenth[30]');
        $t->validation->set_rules('contact_email', '<strong>' . __('Email', 'lifeline') . '</strong>', 'required|valid_email');
        $t->validation->set_rules('contact_message', '<strong>' . __('Message', 'lifeline') . '</strong>', 'required|min_length[5]');

        $messages = '';

        if ($t->validation->run() !== FALSE && empty($t->validation->_error_array)) {

            $name = $t->validation->post('contact_name');
            $email = $t->validation->post('contact_email');
            $message = $t->validation->post('contact_message');
            $contact_to = ( sh_set($settings, 'contact_email') ) ? sh_set($settings, 'contact_email') : get_option('admin_email');

            $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
            if( function_exists( 'lifeline_mail_function' ) ) {
                lifeline_mail_function($contact_to, __('Contact Us Message', 'lifeline'), $message, $headers);
            }
            $message = sh_set($settings, 'success_message') ? $settings['success_message'] : sprintf(__('Thank you <strong>%s</strong> for using our contact form! Your email was successfully sent and we will be in touch with you soon.', 'lifeline'), $name);

            $messages = '<div class="alert alert-success">
							<p class="title">' . __('SUCCESS! ', 'lifeline') . $message . '</p>
						</div>';
        } else {
            if (is_array($t->validation->_error_array)) {
                foreach ($t->validation->_error_array as $msg) {
                    $messages .= '<div class="alert alert-error">
										<p class="title">' . __('Error! ', 'lifeline') . $msg . '</p>
									</div>';
                }
            }
        }
        echo wp_kses_post( $messages );
        exit;
    }

}
