<?php
/* Template Name: Container */
sh_custom_header();

global $post_type, $wp_query;
$settings = get_post_meta(get_the_ID(), '_page_settings', true);

?>

<?php if (sh_set($settings, 'header') == 'true') : ?> 
    <?php if (sh_set($settings, 'top_image')): ?>
        <div class="top-image"> <img src="<?php echo sh_set($settings, 'top_image'); ?>" /></div>
    <?php elseif (sh_set($settings, 'show_page_title') == 1): ?>
        <div class="no-top_img"></div>
    <?php endif; ?>
    <?php
else:
    if (sh_set($settings, 'is_home') != 1 && sh_set($settings, 'show_page_title') == 1):
        ?>
        <div class="no-top-img"></div>
        <?php
    endif;
endif;
?>



<section class="inner-page">


    <?php if (sh_set($settings, 'header') == 'true') : ?>
        <div class="container">
            <?php
            if (sh_set($settings, 'show_page_title') == 1) :
                $title = sh_set($settings, 'page_title');
                ?>

                <div class="page-title">
                    <?php
                    if ($title) :
                        $title = $title;
                    else:
                        $title = get_the_title();
                    endif;
                    echo sh_get_title($title, 'h1', 'span', FALSE);
                    ?>

                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="default2">
    <?php 
    while ( have_posts() ): the_post();
    echo do_shortcode( the_content() );
endwhile; ?>
</div>
</div>
	
</section>
<?php get_footer(); ?>


