<?php
/* Template Name: Projects */

sh_custom_header();
$settings = get_post_meta( get_the_ID(), '_page_settings', true );
$paged = get_query_var( 'paged' );
$sidebar = sh_set( $settings, 'sidebar' ) ? sh_set( $settings, 'sidebar' ) : '';
$sidebar_position = sh_set( $settings, 'sidebar_pos' );
$col_class = (sh_set( $settings, 'sidebar' )) ? 'col-md-9' : 'col-md-12 left-content';
wp_enqueue_script('isotope-initialize');
?>
<?php if(sh_set( $settings, 'header' )=='true' && sh_set( $settings, 'top_image' )) : ?>
	<div class="top-image"> <img src="<?php echo sh_set( $settings, 'top_image' ); ?>" alt="<?php esc_attr_e( 'Not Found', 'lifeline' ); ?>" /> </div>
<!-- Page Top Image -->
<?php endif; ?>
<section class="inner-page <?php echo ( sh_set( $settings, 'sidebar_pos' ) == 'left' ) ? ' switch' : ''; ?>">
	<div class="container">
		<?php if(sh_set( $settings, 'show_page_title' ) == 1  ) :

			$title = sh_set( $settings, 'page_title' ); 
			if($title) :
					$title = $title;
			else: 
					$title = get_the_title();
			endif;  ?>

	        <div class="page-title">

				<?php echo sh_get_title( $title, 'h1', 'span', FALSE ); ?>

	        </div>
        <!-- Page Title -->
    <?php endif; ?>
		<div class="row" >
			<?php if ($sidebar_position == 'left' && is_active_sidebar($sidebar)) : ?>
                <div class="sidebar col-md-3 pull-left">
					<?php dynamic_sidebar( $sidebar ); ?>
                </div>
			<?php endif; ?>
			<div class=" <?php echo esc_attr( $col_class ); ?>"> 
				<!-- Page Title -->
				<?php $query = new WP_Query( 'post_type=dict_project&posts_per_pag=' . $paged ); ?>
				<div class="remove-space">
					<div class="row masonary">
						<?php while ( $query->have_posts() ): $query->the_post(); ?>
							<?php $ProjectSettings = get_post_meta( get_the_ID(), '_dict_project_settings', true ); ?>
							<div class="col-md-3">
                                <div class="story">
									<div class="story-img"> <?php the_post_thumbnail( '370x252' ) ?>
										<h5><?php echo substr( get_the_title(), 0, 32 ); ?></h5>
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span></span></a> </div>
									<div class="story-meta">
                                        <?php if(sh_set( $ProjectSettings, 'date' ) != ''): ?>
                                        <span><i class="icon-calendar-empty"></i><?php echo sh_set($ProjectSettings, 'date'); ?></span>
                                        <?php endif; ?>
                                        <?php if(sh_set( $ProjectSettings, 'location' ) != ''): ?>
                                        <span><i class="icon-map-marker"></i><?php _e( "In ", 'lifeline' ); ?><?php echo sh_set( $ProjectSettings, 'location' ); ?></span>
                                        <?php endif; ?>
                                        <?php if(sh_set( $ProjectSettings, 'amount_needed' ) != ''): ?>
                                        <p>
											<?php _e( "Needed Donation ", 'lifeline' ); ?>
											<strong>$ <?php echo sh_set( $ProjectSettings, 'amount_needed' ); ?></strong>
                                        </p>
                                        <?php endif; ?>
									</div>
									<p><?php echo substr( get_the_content(), 0, 158 ); ?></p>
								</div>
							</div>
							<?php
						endwhile;
						wp_reset_query();
						?>
					</div>
				</div>
				<?php _the_pagination(array( 'total' => $query->max_num_pages )); ?>
			</div>
			<?php if ($sidebar_position == 'right' && is_active_sidebar($sidebar)) : ?>
                <div class="sidebar col-md-3 pull-right">
					<?php dynamic_sidebar( $sidebar ); ?>
                </div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
