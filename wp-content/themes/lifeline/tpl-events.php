<?php
/* Template Name: Events */

sh_custom_header();
$Settings = get_option( 'lifeline' );
$PageSettings = get_post_meta( get_the_ID(), '_page_settings', true );
$paged = get_query_var( 'paged' );
$sidebar = sh_set( $PageSettings, 'sidebar' ) ? sh_set( $PageSettings, 'sidebar' ) : '';
$sidebar_position = (sh_set( $PageSettings, 'sidebar_pos' ) == 'left') ? 'switch' : '';
$col_class = ($sidebar) ? 'col-md-9' : 'col-md-12 left-content ';
?>
<?php if(sh_set( $PageSettings, 'top_image' ) && sh_set( $PageSettings, 'header' )=='true') : ?>
	<div class="top-image"><img src="<?php echo sh_set( $PageSettings, 'top_image' ); ?>" alt="<?php esc_attr_e( 'Not Found', 'lifeline' ); ?>" /> </div>
<!-- Page Top Image -->
<?php endif; ?>
<section class="inner-page <?php echo esc_attr($sidebar_position ); ?>">

    <div class="container">

		 <?php if(sh_set( $PageSettings, 'show_page_title' ) == 1  ) :

			$title = sh_set( $PageSettings, 'page_title' ); 
			if($title) :
					$title = $title;
			else: 
					$title = get_the_title();
			endif;  ?>

	        <div class="page-title">

				<?php echo sh_get_title( $title, 'h1', 'span', FALSE ); ?>

	        </div>
    <?php endif; ?>
		<!-- Page Title -->
		<div class="row" >
                    <?php if ( $sidebar && sh_set( $PageSettings, 'sidebar_pos' )=='left' ): ?> <div class="sidebar col-md-3"><?php dynamic_sidebar( $sidebar ); ?></div><?php endif; ?>
			<div class="<?php echo esc_attr($col_class); ?>">

				<?php $query = new WP_Query( 'post_type=dict_event&order='. sh_set($Settings, 'event_order') .'&orderby='. sh_set($Settings, 'event_order_by') .'&paged=' . $paged ); ?>
				<div class="remove-space">
					<?php while ( $query->have_posts() ): $query->the_post(); ?>

						<?php $EventSettings = get_post_meta( get_the_ID(), '_dict_event_settings', true ); ?>

						<div class="row">

							<?php if ( has_post_thumbnail() ): ?>
								<div class="col-md-5">
									<div class="event-post-image"> 

										<?php the_post_thumbnail( '370x252' ); ?> <img class="map" src="<?php echo get_template_directory_uri(); ?>/images/map.jpg" alt="<?php esc_attr_e( 'Not Found', 'lifeline' ); ?>" /> 

										<i class="icon-map-marker"></i> 

										<a href="<?php echo the_permalink(); ?>"  ><span></span></a> 

									</div>
								</div>
							<?php endif; ?>

							<div class="col-md-7  event-post-detail">

								<h2><a href="<?php echo the_permalink(); ?>"  ><?php the_title(); ?></a></h2>

								<ul class="post-meta">

									<li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )); ?>"  ><i class="icon-user"></i><?php _e( 'by', 'lifeline' ); ?> <?php echo sh_set( $EventSettings, 'organizer' ); ?></a></li>
									<li><a href="<?php echo esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d'))); ?>"  ><i class="icon-calendar-empty"></i><span><?php echo date( 'F', strtotime( sh_set( $EventSettings, 'start_date' ) ) ); ?></span> <?php echo date( 'd, Y', strtotime( sh_set( $EventSettings, 'start_date' ) ) ); ?></a></li>
									<li><i class="icon-map-marker"></i><?php _e( 'In', 'lifeline' ); ?> <?php echo sh_set( $EventSettings, 'location' ); ?></li>
								</ul>
								<p><?php echo substr( get_the_content(), 0, 155 ); ?></p>
							</div>
						</div>
						<?php
					endwhile;
					wp_reset_query();
					?>
				</div>
				<?php _the_pagination( array( 'total' => $query->max_num_pages ) ); ?>

			</div>

			<?php if ( $sidebar && sh_set( $PageSettings, 'sidebar_pos' )=='right' ): ?> <div class="sidebar col-md-3"><?php dynamic_sidebar( $sidebar ); ?></div><?php endif; ?>
		</div>
    </div>

</section>

<?php get_footer(); ?>
