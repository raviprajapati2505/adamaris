<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'adamaris' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Wh9p=gNJU-B25c|Q1TEj;]Bv~]&tM1{sfUZ@/%P]]elbL0,P5 N`umg(?0E<s{L*' );
define( 'SECURE_AUTH_KEY',  'GV;DKx*h-r/>!>C66S7@mqZ4F b{acU3l5vAx0x6nhc??c[Y6pjW){6)$dzqQvu(' );
define( 'LOGGED_IN_KEY',    'rJsF6MKQ3@P6vBD:b9t7Ju>TA.zKo1GnrgMmZlj7gRlG~:Sm_OE}1(x-Hx-<8uoo' );
define( 'NONCE_KEY',        'j*aBi?FJya9_B-W(9bc5v-I8PI8|&HTYF[{K #vI}6,.IkHth-TVS&~VVmjV5BCc' );
define( 'AUTH_SALT',        '1v38R1T2{|BC]V%v`l^?alabd -d7TiL%m{qZfcDbksm]!<g1{-?Co;?lUEokaQ0' );
define( 'SECURE_AUTH_SALT', '<(pa[gsB]}]. :s&dN&jXdm7Uw]?; qnJ`c?Ly@Cn)}6rAzyF 5d5)j!Ks1[)?*v' );
define( 'LOGGED_IN_SALT',   ',4,8=]d8PNQg8QO@dE8HySf,jh3{]Ho/g;/MUK^Ad+Lt>PGDe7@2wiZjm,3zE+vB' );
define( 'NONCE_SALT',       'b?Y_~y5j`!z*UJ#^%*n8f=p1Lj!<+o63F}jWKatahYc:EyGI;F.oV.h>Y}zY+_yH' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
